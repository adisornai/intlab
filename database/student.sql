-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2018 at 06:51 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intlab`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `s_id` int(2) NOT NULL,
  `s_sid` varchar(11) NOT NULL,
  `s_user` varchar(30) NOT NULL,
  `s_pass` varchar(64) NOT NULL,
  `s_prename` int(2) NOT NULL DEFAULT '1',
  `s_fnameEN` varchar(100) NOT NULL,
  `s_lnameEN` varchar(100) NOT NULL,
  `s_fnameTH` varchar(100) NOT NULL,
  `s_lnameTH` varchar(100) NOT NULL,
  `s_major` int(2) NOT NULL DEFAULT '1',
  `s_faculty` int(2) NOT NULL DEFAULT '1',
  `s_university` int(2) NOT NULL DEFAULT '1',
  `s_research` varchar(100) DEFAULT NULL,
  `s_status` char(1) NOT NULL DEFAULT '1',
  `s_education` int(2) NOT NULL DEFAULT '1',
  `s_begin` date NOT NULL,
  `s_end` date NOT NULL,
  `s_adviser` int(5) NOT NULL,
  `s_coadviser` int(5) NOT NULL,
  `s_phone` varchar(10) NOT NULL,
  `s_email` varchar(30) NOT NULL,
  `s_house_no` varchar(10) DEFAULT NULL,
  `s_village_no` varchar(5) DEFAULT NULL,
  `s_lane` varchar(20) DEFAULT NULL,
  `s_road` varchar(20) DEFAULT NULL,
  `s_province` int(2) NOT NULL DEFAULT '1',
  `s_district` int(3) NOT NULL DEFAULT '1',
  `s_amphur` int(3) NOT NULL DEFAULT '1',
  `s_zip` varchar(5) NOT NULL,
  `s_detail` text NOT NULL,
  `s_vdo` char(1) NOT NULL DEFAULT '0',
  `s_disable` char(1) NOT NULL DEFAULT '0',
  `s_img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`s_id`, `s_sid`, `s_user`, `s_pass`, `s_prename`, `s_fnameEN`, `s_lnameEN`, `s_fnameTH`, `s_lnameTH`, `s_major`, `s_faculty`, `s_university`, `s_research`, `s_status`, `s_education`, `s_begin`, `s_end`, `s_adviser`, `s_coadviser`, `s_phone`, `s_email`, `s_house_no`, `s_village_no`, `s_lane`, `s_road`, `s_province`, `s_district`, `s_amphur`, `s_zip`, `s_detail`, `s_vdo`, `s_disable`, `s_img`) VALUES
(4, '58011212285', '58011212285', 'e334cdc84592841130ebaf754e8726190e0e9ebaaa8f10197d44f1850b2c4c05', 1, 'Adisorna', 'Kansawai', 'อดิศร', 'ฆารไสว', 2, 4, 1, NULL, '1', 1, '2015-02-01', '2018-08-31', 1, 1, '0973077318', 'thebassbsorn@gmail.com', '37/4', '4', '-', 'Teenanon', 34, 4763, 523, '46150', '', '1', '0', NULL),
(5, '58011212031', '58011212031', '123456789', 1, 'Kittitorn', 'Sermthaisong', 'กิตติธร ', 'เสริมไธสง', 2, 4, 1, NULL, '1', 1, '2018-09-08', '2018-09-29', 1, 1, '0986304174', 'kittitorn.ser@msu.ac.th', '-', '-', '-', '-', 32, 4338, 484, '44110', '', '0', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`s_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `s_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
