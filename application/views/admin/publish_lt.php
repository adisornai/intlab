<div class="container">
	<div class="admin-col-1 col-md-3 zero-margin-padding">
		<div class="admin-home">
			<a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a>
		</div>
		<div class="admin-box-1">
			<button type="" id="manage-student"><label class="admin-label1">Manage Student</label><label class="admin-label2"><i class="fas fa-list"></i></label></button>
			<div class="admin-student-menu">
				<a href="<?php echo base_url(); ?>admin/student"><label class="admin-label1-menu"><i class="fas fa-address-card"></i></label><label class="">Manage Personal</label></a>
				<a href="<?php echo base_url(); ?>admin/research"><label class="admin-label1-menu"><i class="fab fa-researchgate"></i></label><label class="">Manage Research</label></a>
				<a href="<?php echo base_url(); ?>admin/publish"><label class="admin-label1-menu"><i class="fab fa-leanpub"></i></label><label class="">Manage Publish</label></a>
			</div>
		</div>
		<div class="admin-box-2">
			<button type="" id="manage-lecturer"><label class="admin-label1">Manage Lecturer</label><label class="admin-label2"><i class="fas fa-list"></i></label></button>
			<div class="admin-lecturer-menu">
				<a href="<?php echo base_url(); ?>admin/lecturer"><label class="admin-label1-menu"><i class="fas fa-address-card"></i></label><label class="">Manage Personal</label></a>
				<a href="<?php echo base_url(); ?>admin/research_lt"><label class="admin-label1-menu"><i class="fab fa-researchgate"></i></label><label class="">Manage Research</label></a>
				<a href="<?php echo base_url(); ?>admin/publish_lt"><label class="admin-label1-menu"><i class="fab fa-leanpub"></i></label><label class="">Manage Publish</label></a>
			</div>
		</div>
	</div>
	<div class="admin-col-2 col-md-9">
		<div class="row">	
			<div class="admin-content">
				<div class="admin-add-border margin-bottom-20">
					<a title="" class="publish-show-add-admin a2"><label><i class="fab fa-leanpub"></i></label><label>Add Publish</label></a>
				</div>
				<div class="publish-container-admin">
					<div class="publish-content-admin">
						<div class="publish-head-admin">
							<label>Publish</label>
						</div>
						<div>
			            	<label>ID Lecturer</label>
				        </div>
				        <div class=""> 
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
			           	 	<label class="publish-la1">Standards </label>
				        </div>
				        <div class=""> 
				            <select name="p_ps_id" class="publish-sele1">
				            	<?php 
				            	foreach ($publish_st as $row) {
				            		echo "<option value='".$row->ps_id."'>".$row->ps_name."</option>";
				            	}
				            	 ?>
				            </select>
				        </div>
				        <div>
				        	<label class="publish-la1">Type </label>
				        </div>
				        <div>
				        	<select name="p_status_type" class="publish-sele1">
				            	<?php 
				            	foreach ($publish_type as $row) {
				            		echo "<option value='".$row->pt_id."'>".$row->pt_name."</option>";
				            	}
				            	 ?>
				            </select>
				        </div>
						<div>
			            	<label>Name</label>
				        </div>
				        <div class=""> 
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Author</label>
				        </div>
				        <div>
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Sources</label>
				        </div>
				        <div>
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Year</label>
				        </div>
				        <div>
				            <input type="number" min="1900" max="2099" step="1" value="">
				        </div>
				        <div>
				            <label>Volume</label>
				        </div>
				        <div>
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Issue</label>
				        </div>
				        <div>
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Page</label>
				        </div>
				        <div>
				            <input type="text" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Links</label>
				        </div>
				        <div>
				            <input type="file" name="" value="" placeholder="">
				        </div>
				        <div>
				            <label>Status</label>
				        </div>
				        <div class="margin-bottom-20">
				            <select name="" class="">
				                <option value="0">Student</option>
			                	<option value="1">Lecturer</option>
				            </select>
				        </div>
				        <div class="publish-btn-admin">
				        	<input type="submit" class="publish-submit-admin admin-btn-add" name="" value="Add Publish">
				        	<button type="button" class="publish-close-admin admin-btn-add">Close</button>
				        </div>
					</div>
				</div>
				<table class="table admin-table">
					<thead>
						<tr>
							<th>รหัสงานตีพิมพ์</th>
							<th>ชื่อของงานวิจัยที่ตีพิมพ์</th>
							<th>ผู้เขียน</th>
							<th>ปีที่ตีพิมพ์</th>
							<th>แก้ไข</th>
							<th>ลบ</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						if($result->num_rows() > 0){
							foreach ($result->result() as $row) {
								?>
								<tr>
									<td><?php echo $row->p_id; ?></td>
									<td><?php echo $row->p_name; ?></td>
									<td><?php echo $row->p_author; ?></td>
									<td><?php echo $row->p_year; ?></td>
									<td><button class="ad-pu-edit-lt"><i class="fa fa-cog"></i></button></td>
									<td><button class="ad-pu-del-lt"><i class="fas fa-trash-alt"></i></button></td>
								</tr>
								<?php		
							}
						}else{
							?>
							<tr>
								<td colspan="5" style="text-align:center;">ไม่มีข้อมูล</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>

