<div class="container">
	<div class="admin-col-1 col-md-3 zero-margin-padding">
		<div class="admin-home">
			<a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a>
		</div>
		<div class="admin-box-1">
			<button type="" id="manage-student"><label class="admin-label1">Student Information Management</label><label class="admin-label2"><i class="fas fa-list"></i></label></button>
			<div class="admin-student-menu">
				<a href="<?php echo base_url(); ?>admin/student"><label class="admin-label1-menu"><i class="fas fa-address-card"></i></label><label class="">Personal</label></a>
				<a href="<?php echo base_url(); ?>admin/research"><label class="admin-label1-menu"><i class="fab fa-researchgate"></i></label><label class="">Research</label></a>
				<a href="<?php echo base_url(); ?>admin/publish"><label class="admin-label1-menu"><i class="fab fa-leanpub"></i></label><label class="">Publish</label></a>
			</div>
		</div>
		<div class="admin-box-2">
			<button type="" id="manage-lecturer"><label class="admin-label1">Lecturer Information Management</label><label class="admin-label2"><i class="fas fa-list"></i></label></button>
			<div class="admin-lecturer-menu">
				<a href="<?php echo base_url(); ?>admin/lecturer"><label class="admin-label1-menu"><i class="fas fa-address-card"></i></label><label class="">Personal</label></a>
				<a href="<?php echo base_url(); ?>admin/research_lt"><label class="admin-label1-menu"><i class="fab fa-researchgate"></i></label><label class="">Research</label></a>
				<a href="<?php echo base_url(); ?>admin/publish_lt"><label class="admin-label1-menu"><i class="fab fa-leanpub"></i></label><label class="">Publish</label></a>
			</div>
		</div>
	</div>
	<div class="admin-col-2 col-md-9">
		<div class="row">	
			<div class="admin-content">
				<div class="admin-add-border margin-bottom-20">
					<a title="" class="reseach-show-add a2"><label><i class="fab fa-researchgate"></i></label><label>Add Reseach</label></a>
				</div>
				<div class="reseach-container">
					<div class="reseach-content">
						<form method="<?php base_url(); ?>admin/" action="post">
							<div class="reseach-head">
								<label>Reseach</label>
							</div>
							<div>
								<label>ID Lecturer</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Name Thai</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Name Eng</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Researcher</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Co-Researcher 1</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Co-Researcher 2</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Co-Researcher 3</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div>
								<label>Finances</label>
							</div>
							<div>
								<input type="text" name="" value="" placeholder="">
							</div>
							<div class="reseach-time">
								<label class="reseach-begin">Begin</label>
								<label class="reseach-eng">End</label>
							</div>
							<div class="reseach-time margin-bottom-20">
								<input type="number" class="reseach-begin" name="" value="" placeholder="">
								<input type="number" class="reseach-eng" name="" value="" placeholder="">
							</div>

							<div class="reseach-content-btn">
								<input type="submit" class="reseach-submit admin-btn-add" name="" value="Add Reseach">
								<button type="button" class="reseach-close admin-btn-add">Close</button>
							</div>
						</form>
					</div>
				</div>

				<div class="ad-con">
					<table class="table admin-table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Thai Name</th>
								<th>Eng Name</th>
								<th>Researcher</th>
								<th>Year</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							if($result->num_rows() > 0){
								foreach ($result->result() as $row) {
									?>
									<tr>
										<td><?php echo $row->r_id; ?></td>
										<td><?php echo $row->r_nameTH; ?></td>
										<td><?php echo $row->r_nameEN; ?></td>
										<td><?php echo $row->r_head; ?></td>
										<td><?php echo $row->r_begin; ?></td>
										<td><button class="ad-re-edit-lt"><i class="fa fa-cog"></i></button></td>
										<td><button class="ad-re-del-lt"><i class="fas fa-trash-alt"></i></button></td>
									</tr>
									<?php		
								}
							}else{
								?>
								<tr>
									<td colspan="5" style="text-align:center;">ไม่มีข้อมูล</td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

