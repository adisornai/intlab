<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Intellect Laboratory</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<!--For Plugins external css-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!--Theme custom css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-a.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/admin/style.css'); ?>">

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css" />

    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.11.2.min.js"></script>

    <!-- font thai -->
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">

    <!-- Sweetalert2 -->
    <script src="<?php echo base_url('assets/sweetalert2/package/dist/sweetalert2.all.min.js'); ?>"></script>

</head>
<body>
    
