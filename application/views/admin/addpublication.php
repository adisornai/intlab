<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-bottom-20">
			<div class="page-header">
				<h2>Add Publication</h2>
			</div>
			<form method="post" action="<?php echo base_url(); ?>admin/insert_publication" class="input-pad-16">
				<div class="col-lg-2 p-0"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="">
						<span class="max-width">User</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0 input-pad-16">
						<select id="p_user" name="p_user" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
							<option value="<?php echo $this->session->U_id; ?>"><?php echo $this->session->U_name; ?></option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_sid max-width"><?php echo form_error('p_user'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Published Standards</span>
						</div>
						<div class="col-md-12 p-0">
							<select id="p_ps_id" name="p_ps_id" class="p-5 max-width form-control">
								<option value="">Choose ...</option>
								<?php 
								foreach ($load_ps as $row) {
									if(set_value('p_ps_id') == $row->ps_id)
									{
								?>	
										<option value="<?php echo $row->ps_id; ?>" selected><?php echo $row->ps_name; ?></option>
								<?php
									}
									else
									{
								?>
										<option value="<?php echo $row->ps_id; ?>"><?php echo $row->ps_name; ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
						<div>
							<span class="error error_s_user max-width"><?php echo form_error('p_ps_id'); ?></span>
						</div>					
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Publish Type</span>
						</div>
						<div class="col-md-12 p-0">
							<select id="p_status_type" name="p_status_type" class="p-5 max-width form-control">
								<option value="">Choose ...</option>
								<?php 
								foreach ($load_pt as $row) {
									if(set_value('p_status_type') == $row->pt_id)
									{
								?>	
										<option value="<?php echo $row->pt_id; ?>" selected><?php echo $row->pt_name; ?></option>
								<?php
									}
									else
									{
								?>
										<option value="<?php echo $row->pt_id; ?>"><?php echo $row->pt_name; ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_s_pass max-width "><?php echo form_error('p_status_type'); ?></span>
						</div>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Name TH</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="p_nameTH" name="p_nameTH" value="<?php echo set_value('p_nameTH'); ?>" placeholder="Name TH" class="p-5 max-width form-control">
						</div>
						<div>
							<span class="error error_s_user max-width"><?php echo form_error('p_nameTH'); ?></span>
						</div>					
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Name ENG</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="p_nameEN" name="p_nameEN" value="<?php echo set_value('p_nameEN'); ?>" placeholder="Name ENG" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_s_pass max-width "><?php echo form_error('p_nameEN'); ?></span>
						</div>
					</div>
				</div>
				
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Detail</span>
					</div>
					<div class="col-md-11 col-lg-9 p-0">
						<textarea id="p_detail" name="p_detail" rows="5" class="p-5 max-width form-control"><?php if(set_value('p_detail')){$str = trim(set_value('p_detail'));echo $str;}?></textarea>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('p_detail'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Year</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<input type="date" id="p_year" name="p_year" value="<?php echo set_value('p_year'); ?>" placeholder="" class="max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('p_year'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">First Author</span>
						</div>
						<div class="col-md-12 p-0">
							<select id="p_first_author" name="p_first_author" class="p-5 max-width form-control">
								<option value="">Choose ...</option>
								<?php 
								foreach ($load_student as $row) 
								{
									if(set_value('p_first_author') == $row->s_id){	
								?>
										<option value="<?php echo $row->s_id; ?>" selected><?php echo $row->s_fnameEN." ".$row->s_lnameEN; ?></option>
								<?php
									}
									else{
								?>
										<option value="<?php echo $row->s_id; ?>"><?php echo $row->s_fnameEN." ".$row->s_lnameEN; ?></option>
								<?php
									}
								} 
								?>	
							</select>
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('p_first_author'); ?></span>
						</div>	
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Second Author</span>
						</div>
						<div class="col-md-12 p-0">
							<select id="p_second_author" name="p_second_author" class="p-5 max-width form-control">
								<option value="">Choose ...</option>
								<?php 
								foreach ($load_student as $row) 
								{
									if(set_value('p_second_author') == $row->s_id){	
								?>
										<option value="<?php echo $row->s_id; ?>" selected><?php echo $row->s_fnameEN." ".$row->s_lnameEN; ?></option>
								<?php
									}
									else{
								?>
										<option value="<?php echo $row->s_id; ?>"><?php echo $row->s_fnameEN." ".$row->s_lnameEN; ?></option>
								<?php
									}
								} 
								?>	
							</select>
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('p_second_author'); ?></span>
						</div>
					</div>
				</div>
				
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Language</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0 form-group">
						<?php 
						if(set_value('p_language'))
						{
							if(set_value('p_language') == 1)
							{
						?>
								<span class="radio-inline">
									<input type="radio" id="p_language" name="p_language" value="1" class="" checked>
									ภาษาไทย
								</span>
								<span class="radio-inline">
									<input type="radio" id="p_language" name="p_language" value="0" class="">
									English
								</span>

						<?php
							}
							else
							{
						?>
								<span class="radio-inline">
									<input type="radio" id="p_language" name="p_language" value="1" class="">
									ภาษาไทย
								</span>
								<span class="radio-inline">
									<input type="radio" id="p_language" name="p_language" value="0" class="" checked>
									English
								</span>
						<?php
							}
						}
						else
						{
						?>
								<span class="radio-inline">
									<input type="radio" id="p_language" name="p_language" value="1" class="">
									ภาษาไทย
								</span>
								<span class="radio-inline">
									<input type="radio" id="p_language" name="p_language" value="0" class="">
									English
								</span>
						<?php
						}
						?>

						
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('p_language'); ?></span>
					</div>
				</div>
			
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<span class="max-width">File</span>
					<input type="file" id="p_links" name="p_links" value="<?php echo set_value('p_links'); ?>" placeholder="" class="p-5 max-width ">
					<span class="error max-width"><?php echo form_error('p_links'); ?></span>
				</div>
				
				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>				
			</form>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});

	});
		

	
</script>