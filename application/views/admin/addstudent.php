<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-bottom-20">
			<div class="page-header">
				<h2>Add Student</h2>
			</div>
			<form method="post" action="<?php echo base_url(); ?>admin/insert_student" class="input-pad-16">

				<div class="col-lg-2 p-0"></div>
				<div class="col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-12 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Student ID</span>
						</div>
						<div class="col-md-5 col-lg-12 p-0 input-pad-16">
							<input type="text" id="s_sid" name="s_sid" value="<?php echo set_value('s_sid'); ?>" placeholder="Student ID" maxlength="11" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_s_sid max-width"><?php echo form_error('s_sid'); ?></span>
						</div>
					</div>
					<div class="col-lg-1"></div>
					<div class="margin-bottom-15 col-md-12 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Example ID</span>
						</div>
						<div class="col-md-5 col-lg-12 p-0 input-pad-16">
							<small>Ex.ID 58000000000</small>
						</div>
						<div class="col-md-12 p-0">
							
						</div>
					</div>
				</div>

				<div class="col-lg-2 p-0"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_status" name="s_status" class="p-5 max-width form-control">
							<?php 
								if(set_value('s_status') == 1)
								{
									echo "<option value='1' selected>Studying</option>";
								}
								else
								{
									echo "<option value='1'>Studying</option>";
								}
								if(set_value('s_status') == 2)
								{
									echo "<option value='2' selected>Graduate</option>";
								}
								else
								{
									echo "<option value='2'>Graduate</option>";
								}
								if(set_value('s_status') == 3)
								{
									echo "<option value='3' selected>Retire</option>";
								}
								else
								{
									echo "<option value='3'>Retire</option>";
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('s_status'); ?></span>
					</div>			
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status VDO</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_vdo" name="s_vdo" class="p-5 max-width form-control">
							<?php 
								if(set_value('s_vdo') == 0)
								{
									echo "<option value='0' selected>Allow</option>";
								}
								else
								{
									echo "<option value='0'>Allow</option>";
								}
								if(set_value('s_vdo') == 1)
								{
									echo "<option value='1' selected>Disallow</option>";
								}
								else
								{
									echo "<option value='1'>Disallow</option>";
								}
							?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('s_vdo'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status Login</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_disable" name="s_disable" class="p-5 max-width form-control">
							<?php 
								if(set_value('s_disable') == 0)
								{
									echo "<option value='0' selected>Enable</option>";
								}
								else
								{
									echo "<option value='0'>Enable</option>";
								}
								if(set_value('s_disable') == 1)
								{
									echo "<option value='1' selected>Disable</option>";
								}
								else
								{
									echo "<option value='1'>Disable</option>";
								}
								
							?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('s_disable'); ?></span>
					</div>
				</div>	
				
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Education</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_education" name="s_education" class="p-5 max-width form-control">
							<?php 
								foreach ($education as $row) {
									if(set_value('s_education') == $row->e_id)
									{
										echo "<option value='".$row->e_id."' selected>".$row->e_nameENG."</option>";
									}
									else
									{
										echo "<option value='".$row->e_id."'>".$row->e_nameENG."</option>";
									}
									
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('s_education'); ?></span>
					</div>
				</div>		

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Username</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_user" name="s_user" value="<?php echo set_value('s_user'); ?>" placeholder="Username" class="p-5 max-width form-control">
						</div>
						<div>
							<span class="error error_s_user max-width"><?php echo form_error('s_user'); ?></span>
						</div>					
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Password</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="password" id="s_pass" name="s_pass" value="" placeholder="Password" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_s_pass max-width "><?php echo form_error('s_pass'); ?></span>
						</div>
					</div>
				</div>
				
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Pre Name Thai</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_prename" name="s_prename" class="p-5 max-width form-control">
							<option value="0">Choose ...</option>
							<?php 
								foreach ($prename as $row) {
									if (set_value('s_prename') == $row->n_id)
									{
										echo "<option value='".$row->n_id."' data-eng='".$row->n_engname."' selected>".$row->n_thainame."</option>";
									}
									else
									{
										echo "<option value='".$row->n_id."' data-eng='".$row->n_engname."'>".$row->n_thainame."</option>";
									}
									
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('s_prename'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">First Name TH</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_fnameTH" name="s_fnameTH" value="<?php echo set_value('s_fnameTH'); ?>" placeholder="First Name" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_fnameTH'); ?></span>
						</div>
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Last Name TH</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_lnameTH" name="s_lnameTH" value="<?php echo set_value('s_lnameTH'); ?>" placeholder="Last Name" class="p-5 max-width form-control">
						</div>	
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_lnameTH'); ?></span>
						</div>		
					</div>	
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Pre Name English</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_prenameeng" name="s_prenameeng" class="p-5 max-width form-control" disabled>
							<option value="0"></option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('s_prenameeng'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">First Name ENG</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_fnameEN" name="s_fnameEN" value="<?php echo set_value('s_fnameEN'); ?>" placeholder="First Name" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_fnameEN'); ?></span>
						</div>
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Last Name ENG</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_lnameEN" name="s_lnameEN" value="<?php echo set_value('s_lnameEN'); ?>" placeholder="Last Name" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_lnameEN'); ?></span>
						</div>	
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Email</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_email" name="s_email" value="<?php echo set_value('s_email'); ?>" placeholder="Email" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_email'); ?></span>
						</div>	
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Phone</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="s_phone" name="s_phone" value="<?php echo set_value('s_phone'); ?>" placeholder="Phone" maxlength="10" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_phone'); ?></span>
						</div>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Begin</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="date" id="s_begin" name="s_begin" value="<?php echo set_value('s_begin'); ?>" placeholder="" class="max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_begin'); ?></span>
						</div>	
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">End</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="date" id="s_end" name="s_end" value="<?php echo set_value('s_end'); ?>" placeholder="" class="max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_end'); ?></span>
						</div>			
					</div>				
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">University</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_university" name="s_university" class="p-5 max-width form-control">
							<option value="0">Choose ...</option>
							<?php 
								foreach ($university as $row) {
									if(set_value('u_id') == $row->u_id){
							?>
										<option value="<?=$row->u_id?>" selected><?=$row->u_nameEN?></option>
							<?php
									}
									else{
							?>
										<option value="<?=$row->u_id?>"><?=$row->u_nameEN?></option>
							<?php
									}
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('s_university'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Faculty</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_faculty" name="s_faculty" class="p-5 max-width form-control" disabled>
							<option value="0">Choose ...</option>
						</select>		
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('s_faculty'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Major</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_major" name="s_major" class="p-5 max-width form-control" disabled>
							<option value="0">Choose ...</option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('s_major'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Adviser</span>
						<select id="s_adviser" name="s_adviser" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
							<?php 
								foreach ($load_lecturer as $row) {
									if(set_value('s_adviser') == $row->t_id)
									{
										echo "<option value='".$row->t_id."' selected>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
									}
									else
									{
										echo "<option value='".$row->t_id."'>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
									}
								}
							 ?>
						</select>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_adviser'); ?></span>
						</div>
					</div>
					<div class="col-md-1 p-0"></div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Co-Adviser</span>
						<select id="s_coadviser" name="s_coadviser" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
							<?php 
								foreach ($load_lecturer as $row) {
									if(set_value('s_adviser') == $row->t_id)
									{
										echo "<option value='".$row->t_id."' selected>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
									}
									else
									{
										echo "<option value='".$row->t_id."'>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
									}
									
								}
							 ?>
						</select>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('s_coadviser'); ?></span>
						</div>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">House No.</span>
						<input type="text" id="s_house_no" name="s_house_no" value="<?php echo set_value('s_house_no'); ?>" placeholder="House No" class="p-5 max-width form-control">
						<span class="error max-width"><?php echo form_error('s_house_no'); ?></span>
					</div>
					<div class="col-md-1 p-0"></div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Village No.</span>
						<input type="text" id="s_village_no" name="s_village_no" value="<?php echo set_value('s_village_no'); ?>" placeholder="Village No" class="p-5 max-width form-control">
						<span class="error max-width"><?php echo form_error('s_village_no'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Lane</span>
						<input type="text" id="s_lane" name="s_lane" value="<?php echo set_value('s_lane'); ?>" placeholder="Lane" class="p-5 max-width form-control">
						<span class="error max-width"><?php echo form_error('s_lane'); ?></span>
					</div>
					<div class="col-md-1 p-0"></div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Road</span>
						<input type="text" id="s_road" name="s_road" value="<?php echo set_value('s_road'); ?>" placeholder="Road" class="p-5 max-width form-control">
						<span class="error max-width"><?php echo form_error('s_road'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Province</span>
						<select id="s_province" name="s_province" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
						<?php 
							foreach ($province as $row) {
								if(set_value('s_province') == $row->PROVINCE_ID)
								{
									echo "<option value='".$row->PROVINCE_ID."' selected>".$row->PROVINCE_NAME."</option>";
								}
								else
								{
									echo "<option value='".$row->PROVINCE_ID."'>".$row->PROVINCE_NAME."</option>";
								}
								
							}
						 ?>
						 </select>
						<span class="error max-width"><?php echo form_error('s_province'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Amphur</span>
						<select id="s_amphur" name="s_amphur" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
						 </select>
						<span class="error max-width"><?php echo form_error('s_amphur'); ?></span>
					</div>
					<div class="col-md-1 p-0"></div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Postcode</span>
						<input type="text" id="s_zip" name="s_zip" value="" placeholder="Postcode" class="p-5 max-width form-control">
						<span class="error max-width"><?php echo form_error('s_zip'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0 ิ">
					<div class="col-md-5 col-lg-4 p-0">
						<span class="max-width">District</span>
						<select id="s_district" name="s_district" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
						 </select>
						<span class="error max-width"><?php echo form_error('s_district'); ?></span>
					</div>
				</div>
				
				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>
				
									
			</form>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});

		if($('#s_province').val() != "")
		{
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_amphur',
				type: 'post',
				dataType: 'json',
				data: {id_p: $('#s_province').val()},
			})
			.done(function(res) {
				$('#s_amphur').empty();
				$('#s_amphur').append('<option value="">Choose ...</option>');
				var id = <?php echo set_value('s_amphur'); ?> 
				$.each(res.amphur,function(index, el) {
					if(id == el.AMPHUR_ID)
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'" selected>'+el.AMPHUR_NAME+'</option>');
					}
					else
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'">'+el.AMPHUR_NAME+'</option>');
					}	
					
					
				});

				$.ajax({
				url: '<?php echo base_url(); ?>admin/get_district',
				type: 'post',
				dataType: 'json',
				data: {id_a: $('#s_amphur').val()},
				})
				.done(function(res) {
					$('#s_district').empty();
					$('#s_district').append('<option value="">Choose ...</option>');
					var did = <?php echo set_value('s_district'); ?> 
					$.each(res.district,function(index, el) {
						if(did == el.DISTRICT_ID)
						{
							$('#s_district').append('<option value="'+el.DISTRICT_ID+'" selected>'+el.DISTRICT_NAME+'</option>');
						}
						else
						{
							$('#s_district').append('<option value="'+el.DISTRICT_ID+'">'+el.DISTRICT_NAME+'</option>');
						}
						
						
					});

				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});

				$.ajax({
					url: '<?php echo base_url(); ?>admin/get_postcode',
					type: 'post',
					dataType: 'json',
					data: {param1: $('#s_amphur').val()},
				})
				.done(function(res) {
					$('#s_zip').val(res.postcode[0].POSTCODE);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

		$('#s_province').change(function(event) {
			
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_amphur',
				type: 'post',
				dataType: 'json',
				data: {id_p: $(this).val()},
			})
			.done(function(res) {
				$('#s_amphur').empty();
				$('#s_amphur').append('<option value="">Choose ...</option>');
				console.log('test');
				$.each(res.amphur,function(index, el) {

					console.log(<?php echo set_value('s_amphur'); ?>);
					if("" == el.AMPHUR_ID)
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'" selected>'+el.AMPHUR_NAME+'</option>');
					}
					else
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'">'+el.AMPHUR_NAME+'</option>');
					}	
					
					
				});
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});

		$('#s_amphur').change(function(event) {
			
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_district',
				type: 'post',
				dataType: 'json',
				data: {id_a: $(this).val()},
			})
			.done(function(res) {
				$('#s_district').empty();
				$('#s_district').append('<option value="">Choose ...</option>');
				$.each(res.district,function(index, el) {
					$('#s_district').append('<option value="'+el.DISTRICT_ID+'">'+el.DISTRICT_NAME+'</option>');

				});

			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});


			
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_postcode',
				type: 'post',
				dataType: 'json',
				data: {param1: $(this).val()},
			})
			.done(function(res) {
				$('#s_zip').val(res.postcode[0].POSTCODE);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});

		$('#s_prename').change(function(event) {
			if ($('#s_prename').val() == 0) {
				$('#s_prenameeng').empty();
			}
			else {
				$('#s_prenameeng').empty();
				var tmp = document.createElement("option");
				tmp.setAttribute("value", $(this).val());
				var sel = document.getElementById('s_prename');
				var selected = sel.options[sel.selectedIndex];
				var extra = selected.getAttribute('data-eng');
				tmp.innerHTML = extra;
				$('#s_prenameeng').append(tmp);
			}
			
		
		});
		
		$('#s_university').change(function(event) {
			if ($('#s_university').val() == 0) {
				$('#s_faculty').prop('disabled', 'disabled');
			}
			else {
				loadFaculty($(this).val());
			 	$('#s_faculty').prop('disabled', false);
			}
		
		});

		function loadFaculty(u_id){
			var sel = document.getElementById("s_faculty");
			var path = "<?php echo base_url('admin/loadFaculty'); ?>";
			console.log('loadFaculty');
			// get post
			$.post(path, {id: u_id}, function(res) {
				var result = JSON.parse(res);
				if(result.status == 1){ // success
					renderFaculty(result.mydata,sel);
					
				}else{
					// error
				}
			});
		}

		function renderFaculty(obj,root){
			for (var i = 0; i < obj.length; i++) {
				var tmp = document.createElement("option");
				tmp.setAttribute("value", obj[i]["f_id"]);
				tmp.innerHTML = obj[i]["f_nameEN"];
				root.append(tmp);
			}
		}
		
		$('#s_faculty').change(function(event) {
			if ($('#s_faculty').val() == 0) {
				$('#s_major').prop('disabled', 'disabled');
			}
			else {
				loadMajor($(this).val(), $('#s_university').val());
			 	$('#s_major').prop('disabled', false);
			}		
		});

		function loadMajor(f_id,u_id){
			var sel = document.getElementById("s_major");
			var path = "<?php echo base_url('admin/loadMajor'); ?>";
			console.log('loadMajor');
			// get post
			$.post(path, {u_id: u_id,f_id: f_id}, function(res) {
				var result = JSON.parse(res);
				if(result.status == 1){ // success
					renderMajor(result.mydata,sel);
					console.log(f_id+" "+u_id);
				}else{
					// error
					console.log('error');
				}
			});
		}

		function renderMajor(obj,root){
			for (var i = 0; i < obj.length; i++) {
				var tmp = document.createElement("option");
				tmp.setAttribute("value", obj[i]["m_id"]);
				tmp.innerHTML = obj[i]["m_nameEN"];
				root.append(tmp);
			}
		}
	});

	
</script>