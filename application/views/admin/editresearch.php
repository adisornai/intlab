<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-top-20 padding-bottom-20 ">
			<div class="page-header">
				<h2>Edit Research</h2>
			</div>
			<form method="post" action="<?php echo base_url("admin/update_research/$mydata->r_id"); ?>">
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Research</span>
				 	</div>
					<div class="col-md-5 col-lg-4 p-0">
						<input type="text" id="r_research" name="r_research" value="<?php if(set_value('r_research')){echo set_value('r_research');}else{echo $mydata->r_research;} ?>" placeholder="Research" maxlength="11" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_r_research max-width"><?php echo form_error('r_research'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0 margin-bottom-15">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div class="col-md-12 p-0">
							<span class="max-width">Name(TH)</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="r_nameTH" name="r_nameTH" value="<?php if(set_value('r_nameTH')){echo set_value('r_nameTH');}else{echo $mydata->r_nameTH;} ?>" placeholder="Name Thai" maxlength="11" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_r_nameTH max-width"><?php echo form_error('r_nameTH'); ?></span>
						</div>
					</div>
					<div class="col-md-1 p-0"></div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div class="col-md-12 p-0">
							<span class="max-width">Name(ENG)</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="r_nameEN" name="r_nameEN" value="<?php if(set_value('r_nameEN')){echo set_value('r_nameEN');}else{echo $mydata->r_nameEN;} ?>" placeholder="Name Eng" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_r_nameEN max-width"><?php echo form_error('r_nameEN'); ?></span>
						</div>
					</div>
				</div>	

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Head of Project</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="r_head" name="r_head" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_r_head max-width"><?php echo form_error('r_head'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Source of Investment Funds</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<input type="text" id="r_source" name="r_source" value="<?php if(set_value('r_source')){echo set_value('r_source');}else{echo $mydata->r_source;} ?>" placeholder="Source of Investment Funds" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('r_source'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Budget</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<input type="text" id="r_budget" name="r_budget" value="<?php if(set_value('r_budget')){echo set_value('r_budget');}else{echo $mydata->r_budget;} ?>" placeholder="" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('r_budget'); ?></span>
					</div>
					
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">Begin</span>
						<select id="r_begin" name="r_begin" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
							<?php 
							if(set_value('r_begin')){
								$year = date("Y");
								for ($i=1900; $i < $year; $i++) { 
									if(set_value('r_begin') == $i)
									{
								?>
										<option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
								<?php 
									}
									else
									{
								?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								<?php
									}
								}
							}
							else
							{	
								$year = date("Y");
								for ($i=1900; $i < $year; $i++) { 
									if($mydata->r_begin == $i)
									{
								?>
										<option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
								<?php 
									}
									else
									{
								?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								<?php
									}
								}

							}

							 ?>
						</select>
						<span class="error max-width"><?php echo form_error('r_begin'); ?></span>
					</div>
					<div class="col-md-1 p-0"></div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<span class="max-width">End</span>
						<select id="r_end" name="r_end" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
							<?php 
							if(set_value('r_end')){
								$year = date("Y");
								for ($i=1900; $i < $year; $i++) { 
									if(set_value('r_end') == $i)
									{
								?>
										<option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
								<?php
									}
									else
									{
								?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								<?php
									}
								}
							}
							else
							{
								$year = date("Y");
								for ($i=1900; $i < $year; $i++) { 
									if($mydata->r_end == $i)
									{
								?>
										<option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
								<?php
									}
									else
									{
								?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								<?php
									}
								}
							}

							 ?>
						</select>
						<span class="error max-width"><?php echo form_error('r_end'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Status</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="r_status" name="r_status" class="p-5 max-width form-control">
						<option value="">Choose ...</option>
						<?php 
						if(set_value('r_status')){
							if(set_value('r_status') == 1)
							{
							 ?>
							 	<option value="1" selected>Complete</option>
							<?php 
							}
							else
							{
							 ?>
							 	<option value="1">Complete</option>
							<?php 
							}
							?>
							<?php 
							if(set_value('r_status') == 0)
							{
							 ?>
							 	<option value="0" selected>InComplete</option>
							<?php 
							}
							else
							{
							 ?>
							 	<option value="0">InComplete</option>
							<?php 
							}
						}
						else
						{
							if($mydata->r_status == 1)
							{
							 ?>
							 	<option value="1" selected>Complete</option>
							<?php 
							}
							else
							{
							 ?>
							 	<option value="1">Complete</option>
							<?php 
							}
							?>
							<?php 
							if($mydata->r_status == 0)
							{
							 ?>
							 	<option value="0" selected>InComplete</option>
							<?php 
							}
							else
							{
							 ?>
							 	<option value="0">InComplete</option>
							<?php 
							}
						}
						?>
						
						
					</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('r_status'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<span class="max-width">File</span>
					<input type="file" id="r_file" name="r_file" value="<?php echo set_value('r_file'); ?>" placeholder="" class="p-5 max-width">
					<span class="error max-width"><?php echo form_error('r_file'); ?></span>
				</div>

				
				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>				
			</form>
			
		</div>
	</div>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});
	});
</script>
