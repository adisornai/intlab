<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-bottom-20 max-height">
			<div class="page-header">
				<h2>Add Activity</h2>
			</div>
			<form method="post" action="<?php echo base_url(); ?>admin/insert_activity">
				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Activity Thai</span>
				 	</div>
					<div class="col-md-5 col-lg-5 p-0">
						<input type="text" id="a_nameTH" name="a_nameTH" value="<?php echo set_value('a_nameTH'); ?>" placeholder="Activity Thai" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error  max-width"><?php echo form_error('a_nameTH'); ?></span>
					</div>
				</div>

				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Activity English</span>
				 	</div>
					<div class="col-md-5 col-lg-5 p-0">
						<input type="text" id="a_nameENG" name="a_nameENG" value="<?php echo set_value('a_nameENG'); ?>" placeholder="Activity English" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error  max-width"><?php echo form_error('a_nameENG'); ?></span>
					</div>
				</div>

				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<div>
						<span class="max-width">Detail</span>
					</div>
					<div class="col-md-5 col-lg-5 p-0">
						<textarea id="a_detail" name="a_detail" cols="50" rows="5" class="max-width form-control">
						<?php if(set_value('a_detail')){$str = trim(set_value('a_detail'));echo $str;}?></textarea>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('a_detail'); ?></span>
					</div>
				</div>

				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<div>
						<span class="max-width">Date</span>
					</div>
					<div class="col-md-5 col-lg-5 p-0">
						<input type="date" id="a_date" name="a_date" value="<?php echo set_value('a_date'); ?>" placeholder="" class="max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('a_date'); ?></span>
					</div>
				</div>
				
				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<span class="max-width">Image</span>
					<input type="file" id="a_img" name="a_img" value="<?php echo set_value('a_img'); ?>" placeholder="" class="p-5 max-width ">
					<span class="error max-width"><?php echo form_error('a_img'); ?></span>
				</div>

				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>				
			</form>
			
		</div>
	</div>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});
	});
</script>