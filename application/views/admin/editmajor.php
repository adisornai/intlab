<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-bottom-20 max-height">
			<div class="jumbotron bg-w">
				<h2>Edit Major</h2>
			</div>
			<form method="post" action="<?php echo base_url("admin/update_major/$mydata->m_id"); ?>">

				<div class="col-md-3 col-lg-4 p-0"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="">
						<span class="max-width">University</span>
					</div>
					<div class="col-md-7 col-lg-5 p-0 input-pad-16">
						<select id="u_id" name="u_id" class="p-5 max-width form-control">
							<option value="0">Choose ...</option>
							<?php 
								
								foreach ($data_university as $row) {
									if(set_value('u_id')){
						
										if(set_value('u_id') == $row->u_id){
									
							?>
											<option value="<?=$row->u_id?>" selected><?=$row->u_nameEN?></option>
							<?php
										}
										else{
							?>
											<option value="<?=$row->u_id?>"><?=$row->u_nameEN?></option>
							<?php
										}
									}
									else{
										
										if($mydata->u_id == $row->u_id){
							?>
											<option value="<?=$row->u_id?>" selected><?=$row->u_nameEN?></option>
							<?php
										}
										else
										{
							?>
											<option value="<?=$row->u_id?>"><?=$row->u_nameEN?></option>
							<?php
										}
									}
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_sid max-width"><?php echo form_error('u_id'); ?></span>
					</div>
				</div>

				<div class="col-md-3 col-lg-4 p-0"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="">
						<span class="max-width">Faculty</span>
					</div>
					<div class="col-md-7 col-lg-5 p-0 input-pad-16">
						<select id="f_id" name="f_id" class="p-5 max-width form-control">
							<option value="">Choose ...</option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_sid max-width"><?php echo form_error('f_id'); ?></span>
					</div>
				</div>

				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Major Thai</span>
				 	</div>
					<div class="col-md-7 col-lg-5 p-0">
						<input type="text" id="m_nameTH" name="m_nameTH" value="<?php if(set_value('m_nameTH')){echo set_value('m_nameTH');}else{echo $mydata->m_nameTH;} ?>" placeholder="Faculty Thai" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error  max-width"><?php echo form_error('m_nameTH'); ?></span>
					</div>
				</div>

				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">Major Eng</span>
				 	</div>
					<div class="col-md-7 col-lg-5 p-0">
						<input type="text" id="m_nameEN" name="m_nameEN" value="<?php if(set_value('m_nameEN')){echo set_value('m_nameEN');}else{echo $mydata->m_nameEN;} ?>" placeholder="Faculty Eng" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error  max-width"><?php echo form_error('m_nameEN'); ?></span>
					</div>
				</div>
			

				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>				
			</form>
			
		</div>
	</div>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});
	
		$('#u_id').change(function(event) {
			if ($('#u_id').val() == 0) {
				$('#f_id').prop('disabled', 'disabled');
			}
			else {
				loadFaculty($(this).val());
			 	$('#f_id').prop('disabled', false);
			}
		
		});
		<?php 
			if(set_value('u_id')){
		?>
				loadFaculty("<?php echo set_value('u_id'); ?>");
				//console.log("<?php echo set_value('u_id'); ?>");
		<?php
			}
			else
			{
		?>
				loadFaculty("<?php echo $mydata->u_id; ?>");
				//console.log("<?php echo $mydata->u_id; ?>");
		<?php
			}
		 ?>
		

		function loadFaculty(u_id){
			var sel = document.getElementById("f_id");
			var path = "<?php echo base_url('admin/loadFaculty'); ?>";
			
			// get post
			$.post(path, {id: u_id}, function(res) {
				var result = JSON.parse(res);
				if(result.status == 1){ // success
					//console.log(result);
					renderFaculty(result.mydata,sel);
					
				}else{
					// error
				}
			});
		}

		function renderFaculty(obj,root){
			var f_id;
			for (var i = 0; i < obj.length; i++) {
				var tmp = document.createElement("option");
				tmp.setAttribute("value", obj[i]["f_id"]);
				<?php 
					if(set_value('f_id'))
					{
				?>
						f_id = "<?php echo set_value('f_id'); ?>";
						if(f_id == obj[i]["f_id"]){
							tmp.selected = true;
						}
						
				<?php
					}
					else
					{
				?>
						f_id = "<?php echo $mydata->f_id; ?>";
						if(f_id == obj[i]["f_id"]){
							tmp.selected = true;
						}
				
				<?php
					}
				 ?>
				
				
				tmp.innerHTML = obj[i]["f_nameEN"];
				root.append(tmp);
			}
		}
		
	});
</script>