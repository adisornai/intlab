<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div class="col-sm-8 col-lg-10 bg-w max-height admin-display-center">
			<div class="text-center col-xs-8">
				<h1>
					Welcome To BACKEND
				</h1>
				<div class="progress t-process">
					<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
					aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
					50% Complete (success)
					</div>
				</div>
				<h2><small>Websile : IntLab</small></h2>
				<h4><small class="text-success">Student Information Management (Success) <i class="fas fa-check"></i></small></h4>
				<h4><small class="text-success">General Information Management (Success) <i class="fas fa-check"></i></small></h4>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});
	});
</script>