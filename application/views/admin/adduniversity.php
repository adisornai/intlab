<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-bottom-20 max-height">
			<div class="page-header">
				<h2>Add University</h2>
			</div>
			<form method="post" action="<?php echo base_url(); ?>admin/insert_university">
				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">University Thai</span>
				 	</div>
					<div class="col-md-5 col-lg-5 p-0">
						<input type="text" id="u_nameTH" name="u_nameTH" value="<?php echo set_value('u_nameTH'); ?>" placeholder="University Thai" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error  max-width"><?php echo form_error('u_nameTH'); ?></span>
					</div>
				</div>

				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-8 p-0">
					<div class="col-md-12 p-0">
						<span class="max-width">University English</span>
				 	</div>
					<div class="col-md-5 col-lg-5 p-0">
						<input type="text" id="u_nameEN" name="u_nameEN" value="<?php echo set_value('u_nameEN'); ?>" placeholder="University English" class="p-5 max-width form-control">
					</div>
					<div class="col-md-12 p-0">
						<span class="error  max-width"><?php echo form_error('u_nameEN'); ?></span>
					</div>
				</div>
			

				<div class="col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>				
			</form>
			
		</div>
	</div>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});
	});
</script>