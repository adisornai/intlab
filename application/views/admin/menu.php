<div>
	<div class="admin-menu-home">
		<a href="<?php echo base_url(); ?>" class="p-10 color-white admin-menu-bg" style="display:block;"><span class="h3"><i class="fas fa-home"></i> IntLab</span></a>
	</div>
	<a href="<?php echo base_url(); ?>admin"><button class="admin-menu-btn t-l p-10 color-white admin-menu-bg max-width">
			Home
		</button></a>
	<button id="admin_btn_student" class="admin-menu-btn t-l p-10 color-white admin-menu-bg max-width">
		<span class="col-xs-10 p-0">Student Information Management</span>
		<i class="fas fa-chevron-right col-xs-2 text-right"></i>
	</button>
	<div id="admin_show_student" class="" style="display:none;">
		<a href="<?php echo base_url(); ?>admin/student"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Personal
		</button></a>
	</div>

	<button id="admin_btn_lecturer" class="admin-menu-btn t-l p-10 color-white admin-menu-bg max-width">
		<span class="col-xs-10 p-0">Lecturer Information Management</span>
		<i class="fas fa-chevron-right col-xs-2 text-right"></i>
	</button>
	<div id="admin_show_lecturer" class="" style="display:none;">
		<a href="<?php echo base_url('admin/lecturer'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Personal
		</button></a>
	</div>
	<a href="<?php echo base_url(); ?>admin/research"><button class="admin-menu-btn t-l p-10 color-white admin-menu-bg max-width">
		<span class="col-xs-10 p-0">Research Information Management</span>
		<i class="fab fa-pied-piper-pp col-xs-2 text-right" style="font-size:1.2rem;"></i>
		
	</button></a>
	<a href="<?php echo base_url(); ?>admin/publication"><button class="admin-menu-btn t-l p-10 color-white admin-menu-bg max-width">
		<span class="col-xs-10 p-0">Publication Information Management</span>
		<i class="fab fa-creative-commons-share col-xs-2 text-right" style="font-size:1.2rem;"></i>
	
	</button></a>
	<button id="admin_btn_general" class="admin-menu-btn t-l p-10 color-white admin-menu-bg max-width">
		<span class="col-xs-10 p-0">General Information Management</span>
		<i class="fas fa-chevron-right col-xs-2 text-right"></i>
	</button>
	<div id="admin_show_general" class="" style="display:none;">
		<a href="<?php echo base_url('admin/university'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			University
		</button></a>
		<a href="<?php echo base_url('admin/faculty'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Faculty
		</button></a>
		<a href="<?php echo base_url('admin/major'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Major
		</button></a>
		<a href="<?php echo base_url('admin/prename'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Pre Name
		</button></a>
		<a href="<?php echo base_url('admin/academic_ranks'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Academic Position
		</button></a>
		<a href="<?php echo base_url('admin/published_standards'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Published Standards
		</button></a>
		<a href="<?php echo base_url('admin/publish_type'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Publication Type
		</button></a>
		<a href="<?php echo base_url('admin/education'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Education
		</button></a>
		<a href="<?php echo base_url('admin/activity'); ?>"><button class="admin-menu-btn t-l p-10 p-l-40 color-white admin-menu-bg max-width">
			Activity
		</button></a>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#admin_btn_student').click(function(event) {
			$('#admin_show_student').toggle('fast', function() {
				
			});
		});

		$('#admin_btn_lecturer').click(function(event) {
			$('#admin_show_lecturer').toggle('fast', function() {
				
			});
		});

		$('#admin_btn_general').click(function(event) {
			$('#admin_show_general').toggle('fast', function() {
				
			});
		});
	});
</script>