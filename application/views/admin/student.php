<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div class="col-sm-8 col-lg-10 bg-w max-height">
			<div class="row">
				<small class="p-5" style="display:block;">Student Information Management / Personal</small>
				<div class="margin-top-10 margin-bottom-10">
					<a href="<?php echo base_url(); ?>admin/addstudent" class="b-n color-white b-r admin-btn-sub p-5 margin-left-5"><i class="fas fa-external-link-alt"></i> Add Student</a>
				</div>
			</div>
			<div class="row">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<th>ID</th>
							<th>Student ID</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Edit</th>
							<th>Delete</th>
						</thead>
						<tbody>
							<?php 
								if($result)
								{
									foreach ($result as $row) {
										echo "<tr>";
										echo "<td>".$row->s_id."</td>";
										echo "<td>".$row->s_sid."</td>";
										echo "<td>".$row->s_fnameEN."</td>";
										echo "<td>".$row->s_lnameEN."</td>";
										echo "<td><a href='".base_url()."admin/edit_student/".$row->s_id."'><i class='fas fa-edit'></i></a></td>";
										echo "<td><button class='delstudent b-n bg-w' data-id='".$row->s_id."'><i class='fas fa-trash-alt color-link'></i></button></td>";
										echo "</tr>";
									}
								}
								else
								{
									echo "<tr>";
									echo "<td colspan='6' style='text-align: center;'><h3>Not Data</h3></td>";
									echo "</tr>";
								}
								
							 ?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">


	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
			});
		});

		$('.delstudent').click(function(event) {
			swal({
			  position: 'top-end',
			  type: 'success',
			  title: 'Delete Success.',
			  showConfirmButton: false,
			  timer: 1500
			});
			sid = $(this).attr('data-id');
			path = '<?php echo base_url(); ?>admin/delete_student';

			$.ajax({
				url: path,
				type: 'post',
				dataType: 'json',
				data: {id: sid},
				success : function(res){
					console.log(res);
					setTimeout(function(){ window.location.reload(); }, 1500);
				}
			});			
		});
	});
</script>