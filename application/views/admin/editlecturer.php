<div class="container-fluid admin-top-bar">
	<div class="row">
		<div class="admin-menu-bg">
			<button id="slimenu" class="b-n color-white admin-menu-bg" style="outline: none;padding: 5px 10px 5px 10px;margin:5px;"><i class="fas fa-bars max-width p-0" style="font-size:1.5em;"></i></button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div id="menu" class="col-sm-4 col-lg-2 bg-w p-0 admin-menu">
			<?php $this->load->view('admin/menu'); ?>
		</div>
		<div id="colshow" class="col-sm-8 col-lg-10 bg-w padding-bottom-20">
			<div class="page-header">
				<h2>Edit Lecturer</h2>
			</div>
			<form method="post" action="<?php echo base_url("admin/update_lecturer/$mydata->t_id"); ?>" class="input-pad-16">

				<div class="col-lg-2 p-0"></div>
				<div class="col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-12 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Lecturer ID</span>
						</div>
						<div class="col-md-5 col-lg-12 p-0 input-pad-16">
							<input type="text" id="t_lid" name="t_lid" value="<?php if(set_value('t_lid')){echo set_value('t_lid');}else{echo $mydata->t_lid;} ?>" placeholder="Lecturer ID" maxlength="11" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error error_s_sid max-width"><?php echo form_error('t_lid'); ?></span>
						</div>
					</div>
					<div class="col-lg-1"></div>
					<div class="margin-bottom-15 col-md-12 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Example ID</span>
						</div>
						<div class="col-md-5 col-lg-12 p-0 input-pad-16">
							<small>Ex.ID 000</small>
						</div>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div class="">
							<span class="max-width">Username</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_user" name="t_user" value="<?php if(set_value('t_user')){echo set_value('t_user');}else{echo $mydata->t_user;} ?>" placeholder="Username" class="p-5 max-width form-control">
						</div>
						<div>
							<span class="error max-width"><?php echo form_error('t_user'); ?></span>
						</div>					
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Password</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="password" id="t_pass" name="t_pass" value="" placeholder="Password" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width "><?php echo form_error('t_pass'); ?></span>
						</div>
					</div>
				</div>


				<div class="col-lg-2 p-0"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status Admin</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_status_admin" name="t_status_admin" class="p-5 max-width form-control">
							<?php 
								if(set_value('t_status_admin')){
									if(set_value('t_status_admin') == 2)
									{
										echo "<option value='2' selected>General</option>";
									}
									else
									{
										echo "<option value='2'>General</option>";
									}
									if(set_value('t_status_admin') == 1)
									{
										echo "<option value='1' selected>Admin</option>";
									}
									else
									{
										echo "<option value='1'>Admin</option>";
									}
								}
								else{
									if($mydata->t_status_admin == 2)
									{
										echo "<option value='2' selected>General</option>";
									}
									else
									{
										echo "<option value='2'>General</option>";
									}
									if($mydata->t_status_admin == 1)
									{
										echo "<option value='1' selected>Admin</option>";
									}
									else
									{
										echo "<option value='1'>Admin</option>";
									}
								}
							?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_status_admin'); ?></span>
					</div>			
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status IntLab</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_status_it" name="t_status_it" class="p-5 max-width form-control">
							<?php 
								if(set_value('t_status_it')){
									if(set_value('t_status_it') == 1)
									{
										echo "<option value='1' selected>Yes</option>";
									}
									else
									{
										echo "<option value='1'>Yes</option>";
									}
									if(set_value('t_status_it') == 2)
									{
										echo "<option value='2' selected>No</option>";
									}
									else
									{
										echo "<option value='2'>No</option>";
									}
								}
								else{
									if($mydata->t_status_it == 1)
									{
										echo "<option value='1' selected>Yes</option>";
									}
									else
									{
										echo "<option value='1'>Yes</option>";
									}
									if($mydata->t_status_it == 2)
									{
										echo "<option value='2' selected>No</option>";
									}
									else
									{
										echo "<option value='2'>No</option>";
									}
								}
								
							?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_status_it'); ?></span>
					</div>
				</div>		

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status Login</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_disable" name="t_disable" class="p-5 max-width form-control">
							<?php 
								if(set_value('t_disable')){
									if(set_value('t_disable') == 0)
									{
										echo "<option value='0' selected>Enable</option>";
									}
									else
									{
										echo "<option value='0'>Enable</option>";
									}
									if(set_value('t_disable') == 1)
									{
										echo "<option value='1' selected>Disable</option>";
									}
									else
									{
										echo "<option value='1'>Disable</option>";
									}
								}
								else{
									if($mydata->t_disable == 0)
									{
										echo "<option value='0' selected>Enable</option>";
									}
									else
									{
										echo "<option value='0'>Enable</option>";
									}
									if($mydata->t_disable == 1)
									{
										echo "<option value='1' selected>Disable</option>";
									}
									else
									{
										echo "<option value='1'>Disable</option>";
									}
								}
								
							?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_disable'); ?></span>
					</div>
				</div>	
				
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Academic Thai</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_academic" name="t_academic" class="p-5 max-width form-control">
							<option value="0">Choose ...</option>
							<?php 
								foreach ($academic as $row) {
									if(set_value('t_academic')){
										if (set_value('t_academic') == $row->ap_id)
										{
											echo "<option value='".$row->ap_id."' data-eng='".$row->ap_nameEN."' selected>".$row->ap_nameTH."</option>";
										}
										else
										{
											echo "<option value='".$row->ap_id."' data-eng='".$row->ap_nameEN."'>".$row->ap_nameTH."</option>";
										}
									}else{
										if ($mydata->t_academic == $row->ap_id)
										{
											echo "<option value='".$row->ap_id."' data-eng='".$row->ap_nameEN."' selected>".$row->ap_nameTH."</option>";
										}
										else
										{
											echo "<option value='".$row->ap_id."' data-eng='".$row->ap_nameEN."'>".$row->ap_nameTH."</option>";
										}
									}
									
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_academic'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">First Name Thai</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_fnameTH" name="t_fnameTH" value="<?php if(set_value('t_fnameTH')){echo set_value('t_fnameTH');}else{echo $mydata->t_fnameTH;} ?>" placeholder="First Name" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('t_fnameTH'); ?></span>
						</div>
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Last Name Thai</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_lnameTH" name="t_lnameTH" value="<?php if(set_value('t_lnameTH')){echo set_value('t_lnameTH');}else{echo $mydata->t_lnameTH;} ?>" placeholder="Last Name" class="p-5 max-width form-control">
						</div>	
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('t_lnameTH'); ?></span>
						</div>		
					</div>	
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Academic English</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="s_prenameeng" name="s_prenameeng" class="p-5 max-width form-control" disabled>
							<option value="0"></option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('s_prenameeng'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">First Name English</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_fnameEN" name="t_fnameEN" value="<?php if(set_value('t_fnameEN')){echo set_value('t_fnameEN');}else{echo $mydata->t_fnameEN;} ?>" placeholder="First Name" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('t_fnameEN'); ?></span>
						</div>
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Last Name English</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_lnameEN" name="t_lnameEN" value="<?php if(set_value('t_lnameEN')){echo set_value('t_lnameEN');}else{echo $mydata->t_lnameEN;} ?>" placeholder="Last Name" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('t_lnameEN'); ?></span>
						</div>	
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="col-md-12 col-lg-10 p-0">
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Email</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_email" name="t_email" value="<?php if(set_value('t_email')){echo set_value('t_email');}else{echo $mydata->t_email;} ?>" placeholder="Email" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('t_email'); ?></span>
						</div>	
					</div>
					<div class="col-md-1 p-0">
						
					</div>
					<div class="margin-bottom-15 col-md-5 col-lg-4 p-0">
						<div>
							<span class="max-width">Phone</span>
						</div>
						<div class="col-md-12 p-0">
							<input type="text" id="t_phone" name="t_phone" value="<?php if(set_value('t_phone')){echo set_value('t_phone');}else{echo $mydata->t_phone;} ?>" placeholder="Phone" maxlength="10" class="p-5 max-width form-control">
						</div>
						<div class="col-md-12 p-0">
							<span class="error max-width"><?php echo form_error('t_phone'); ?></span>
						</div>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">University</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_university" name="t_university" class="p-5 max-width form-control">
							<option value="0">Choose ...</option>
							<?php 
								foreach ($university as $row) {
									if(set_value('t_university') ? set_value('t_university') : $mydata->t_university == $row->u_id){
							?>
										<option value="<?=$row->u_id?>" selected><?=$row->u_nameEN?></option>
							<?php
									}
									else{
							?>
										<option value="<?=$row->u_id?>"><?=$row->u_nameEN?></option>
							<?php
									}
								}
							 ?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_university'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Faculty</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_faculty" name="t_faculty" class="p-5 max-width form-control" disabled>
							<option value="0">Choose ...</option>
						</select>		
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_faculty'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Major</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_major" name="t_major" class="p-5 max-width form-control" disabled>
							<option value="0">Choose ...</option>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_major'); ?></span>
					</div>
				</div>
					
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Special Research</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<textarea id="t_special_research" name="t_special_research" class="form-control" rows="3" placeholder="Special Research"><?php if(set_value('t_special_research')){echo set_value('t_special_research');}else{echo $mydata->t_special_research;} ?></textarea>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_special_research'); ?></span>
					</div>
				</div>

				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Detail</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<textarea id="t_detail" name="t_detail" class="form-control" rows="3" placeholder="Detail"><?php if(set_value('t_detail')){echo set_value('t_detail');}else{echo $mydata->t_detail;} ?></textarea>
					</div>
					<div class="col-md-12 p-0">
						<span class="error max-width"><?php echo form_error('t_detail'); ?></span>
					</div>
				</div>
				
				<div class="col-lg-2"></div>
				<div class="margin-bottom-15 col-md-12 col-lg-10 p-0">
					<div>
						<span class="max-width">Status VDO</span>
					</div>
					<div class="col-md-5 col-lg-4 p-0">
						<select id="t_vdo" name="t_vdo" class="p-5 max-width form-control">
							<?php 
								if(set_value('t_vdo')){
									if(set_value('t_vdo') == 0)
									{
										echo "<option value='0' selected>Allow</option>";
									}
									else
									{
										echo "<option value='0'>Allow</option>";
									}
									if(set_value('t_vdo') == 1)
									{
										echo "<option value='1' selected>Disallow</option>";
									}
									else
									{
										echo "<option value='1'>Disallow</option>";
									}
								}
								else{
									if($mydata->t_vdo == 0)
									{
										echo "<option value='0' selected>Allow</option>";
									}
									else
									{
										echo "<option value='0'>Allow</option>";
									}
									if($mydata->t_vdo == 1)
									{
										echo "<option value='1' selected>Disallow</option>";
									}
									else
									{
										echo "<option value='1'>Disallow</option>";
									}
								}
							?>
						</select>
					</div>
					<div class="col-md-12 p-0">
						<span class="error error_s_prename max-width"><?php echo form_error('t_vdo'); ?></span>
					</div>
				</div>

				<div class="col-md-3 col-lg-4"></div>
				<div class="margin-bottom-15 col-md-9 col-lg-8 p-0">
					<div class="col-md-7 col-lg-5 p-0">
						<input type="submit" id="admin-menu-btn-sub" name="admin-menu-btn-sub" class="b-n p-10 admin-btn-sub color-white max-width" value="Submit">
					</div>
				</div>
				
				
			</form>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	
	jQuery(document).ready(function($) {
		$('#slimenu').click(function(event) {
			$('#menu').toggle('fast', function() {
				
			});
		});

		$('#t_academic').change(function(event) {
			if ($('#t_academic').val() == 0) {
				$('#s_prenameeng').empty();
			}
			else {
				$('#s_prenameeng').empty();
				var tmp = document.createElement("option");
				tmp.setAttribute("value", $('#t_academic').val());
				var sel = document.getElementById('t_academic');
				var selected = sel.options[sel.selectedIndex];
				var extra = selected.getAttribute('data-eng');
				tmp.innerHTML = extra;
				$('#s_prenameeng').append(tmp);
			}
		});
		
		$('#s_prenameeng').empty();
		var tmp = document.createElement("option");
		tmp.setAttribute("value", $('#t_academic').val());
		var sel = document.getElementById('t_academic');
		var selected = sel.options[sel.selectedIndex];
		var extra = selected.getAttribute('data-eng');
		tmp.innerHTML = extra;
		$('#s_prenameeng').append(tmp);
		
		$('#t_university').change(function(event) {
			if ($('#t_university').val() == 0) {
				$('#t_faculty').prop('disabled', 'disabled');
			}
			else {
				loadFaculty($('#t_university').val());
			 	$('#t_faculty').prop('disabled', false);
			}
		});

		<?php 
			if(set_value('t_university')){
		?>
				loadFaculty($('#t_university').val());
				$('#t_faculty').prop('disabled', false);
		<?php
			}
			else{
		?>
				if(<?=$mydata->t_university?>){
					loadFaculty($('#t_university').val());
					$('#t_faculty').prop('disabled', false);
				}
		<?php
			}
		 ?>

		function loadFaculty(u_id){
			var sel = document.getElementById("t_faculty");
			var path = "<?php echo base_url('admin/loadFaculty'); ?>";
			console.log('loadFaculty');
			// get post
			$.post(path, {id: u_id}, function(res) {
				var result = JSON.parse(res);
				if(result.status == 1){ // success
					renderFaculty(result.mydata,sel);
					
				}else{
					// error
				}
			});
		}

		function renderFaculty(obj,root){

			for (var i = 0; i < obj.length; i++) {
				<?php 
				if(set_value('t_faculty')){
				?>
					if(<?=set_value('t_faculty'); ?> == obj[i]["f_id"]){
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["f_id"]);
						tmp.innerHTML = obj[i]["f_nameEN"];
						tmp.selected = true;
						root.append(tmp);

					}
					else{
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["f_id"]);
						tmp.innerHTML = obj[i]["f_nameEN"];
						root.append(tmp);
					}
				<?php
				}
				else{
				?>
					if(<?=$mydata->t_faculty?> == obj[i]["f_id"]){
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["f_id"]);
						tmp.innerHTML = obj[i]["f_nameEN"];
						tmp.selected = true;
						root.append(tmp);
					}
					else{
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["f_id"]);
						tmp.innerHTML = obj[i]["f_nameEN"];
						root.append(tmp);
					}
				<?php
				}
				?>
				
			}
		}
		
		$('#t_faculty').change(function(event) {
			if ($('#t_faculty').val() == 0) {
				$('#t_major').prop('disabled', 'disabled');
			}
			else {
				loadMajor($('#t_faculty').val(), $('#t_university').val());
			 	$('#t_major').prop('disabled', false);

			}		
		});

		<?php 
			if(set_value('t_faculty')){
		?>
				loadMajor(<?=set_value('t_faculty')?>, $('#t_university').val());
			 	$('#t_major').prop('disabled', false);
		<?php
			}
			else{
		?>
				loadMajor(<?=$mydata->t_faculty?>, <?=$mydata->t_university?>);
			 	$('#t_major').prop('disabled', false);
		<?php
			}
		 ?>

		function loadMajor(f_id,u_id){
			var sel = document.getElementById("t_major");
			var path = "<?php echo base_url('admin/loadMajor'); ?>";
			console.log('loadMajor');
			// get post
			$.post(path, {u_id: u_id,f_id: f_id}, function(res) {
				var result = JSON.parse(res);
				if(result.status == 1){ // success
					renderMajor(result.mydata,sel);
				}else{
					// error
					console.log('error');
				}
			});
		}

		function renderMajor(obj,root){
			for (var i = 0; i < obj.length; i++) {
				<?php 
				if(set_value('t_major')){
				?>
					if(<?=set_value('t_major'); ?> == obj[i]["m_id"]){
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["m_id"]);
						tmp.innerHTML = obj[i]["m_nameEN"];
						tmp.selected = true;
						root.append(tmp);

					}
					else{
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["m_id"]);
						tmp.innerHTML = obj[i]["m_nameEN"];
						root.append(tmp);
					}
				<?php
				}
				else{
				?>
					if(<?=$mydata->t_major?> == obj[i]["m_id"]){
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["m_id"]);
						tmp.innerHTML = obj[i]["m_nameEN"];
						tmp.selected = true;
						root.append(tmp);
					}	
					else{
						var tmp = document.createElement("option");
						tmp.setAttribute("value", obj[i]["m_id"]);
						tmp.innerHTML = obj[i]["m_nameEN"];
						root.append(tmp);
					}
					
				<?php
				}
				?>
				
			}
		}
	});

	
</script>