<div class="container ">
    <div class="row">
        <div class="login-container">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="login-img">
                    <img src="<?php echo base_url(); ?>assets/images/logo.png" class="">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <?php echo form_open('login/checklogin'); ?>
                    <div class="login-head margin-bottom-20">
                        <label>Member Login</label>
                    </div>
                    <div class="login-input margin-bottom-10">
                        <i class="login-icon fas fa-user"></i>
                        <input type="text" id="login-username" name="username" value="" placeholder="Username" class="<?php echo $this->session->flashdata('login-input-error'); ?>">
                    </div>
                    <div class="login-input margin-bottom-20">
                        <i class="login-icon fas fa-lock"></i>
                        <input type="password" id="login-password" name="password" value="" placeholder="Password" class="<?php echo $this->session->flashdata('login-input-error'); ?>">
                    </div>
                    <div class="login-btn margin-bottom-5">
                        <input type="submit" name="" value="Login" class="">
                    </div>
                    <div class="login-error">
                        <label class=""><?php echo $this->session->flashdata('error'); ?></label>
                    </div>
                </form>   
            </div>      
        </div>
        
         
                  

    </div>
</div>
