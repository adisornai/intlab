<!--Home page style-->
        <header id="home" class="home">
            <div class="overlay-fluid-block">
                <div class="container text-center">
                    <div class="row">
                        <div class="home-wrapper">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="home-content">
                                    <img src="<?php echo base_url(); ?>assets/images/logo.png">
                                    <h1>Intellect Laboratory</h1>
                                    <p>Faculty of Informatics, Mahasarakham University, Thailand</p>
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                                            <div class="home-contact">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>			
            </div>
        </header>



        

        <!-- Sections -->
        <section id="features" class="features sections">
            <div class="container">
                <div class="row">
                    <div class="main_features_content2">
                        <div class="head_title text-center">
                            <h2>Gallery</h2>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 community-edition">
                            <img src="<?php echo base_url(); ?>assets/images/Gallery/02.jpg" style="width:100%;height: 0 auto;">
                        </div>
                        
                    </div>

                </div>
            </div>
        </section><!--End of Features 2 Section -->

        
            

        <!-- Sections -->
        <section id="business" class="portfolio sections">
            <div class="container">
                <div class="head_title text-center">
                    <div id="Lecturer" class="Lecturer"></div>
                    <h1>Lecturer</h1>
                </div>

                <div class="row">
                    <div class="portfolio-wrapper text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <a href="<?php echo base_url(); ?>show">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Member/teacher1.jpg" alt="" />
                                <h4>ผศ.ดร.จันทิมา พลพินิจ</h4>
                                <p>หัวหน้าหน่วยปฏิบัติการวิจัย <br>
                                    ภาควิชาวิทยาการสารสนเทศ     <br>
                                    Email: jantima.p@msu.ac.th <br>
                                    Tel: 043 754 359 ext 5248 
                                </p>
                            </div>
                            </a>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Member/teacher2.jpg" alt="" />
                                <h4>ผศ.ชุมศักดิ์ สีบุญเรือง</h4>
                                <p>&nbsp; <br>
                                    ภาควิชาเทคโนโลยีสารสนเทศ <br>
                                    Email: chumsak.s@msu.ac.th <br>
                                    Tel: 043 754 359 ext 5354  
                                </p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Member/teacher3.jpg" alt="" />
                                <h4>ผศ.วุฒิชัย วิเชียรไชย</h4>
                                <p>&nbsp; <br>
                                    ภาควิชาเทคโนโลยีสารสนเทศ <br>
                                    Email: onizuka.p@gmail.com <br>
                                    Tel: 043 754 359 ext 5354 
                                </p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Member/teacher4.jpg" alt="" />
                                <h4>อ.ปริวัฒน์ พิสิษฐพงษ์</h4>
                                <p>&nbsp; <br>
                                    ภาควิชาสื่อนฤมิต <br>
                                    Email: vacabond@hotmail.com <br>
                                    Tel: 043 754 359 ext 5329 
                                </p>
                            </div>
                        </div>

                    </div>
                </div>                
            </div> <!-- /container -->       
        </section>

        <!-- Sections -->
        <section id="business" class="portfolio sections">
            <div class="container">
                <div class="head_title text-center">
                    <div id="Students" class="Students"></div>
                    <h1>Students</h1>
                    <p>Master Students</p>
                </div>

                <div class="row">
                    <div class="portfolio-wrapper text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Master/M1.jpg" alt="" />
                                <h4>นายสหชัย งามชัยภูมิ</h4>
                                <p>เทคโนโลยีสารสนเทศ<br>
                                    Research: Business Process Management<br> 
                                </p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Master/M2.jpg" alt="" />
                                <h4>นายทองปาน สุขเสมอ</h4>
                                <p>วิทยาการคอมพิวเตอร์<br>
                                    Research: Natural Language Processing<br> 
                                </p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Master/M3.jpg" alt="" />
                                <h4>นายคณิน งามมานะ</h4>
                                <p>วิทยาการคอมพิวเตอร์<br>
                                    Research: Business Process Management<br> 
                                </p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Master/M4.jpg" alt="" />
                                <h4>นายพฤทธิ์ นาเสงี่ยม</h4>
                                <p>วิทยาการคอมพิวเตอร์<br>
                                    Research: Business Process Management<br> 
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- row 2 -->
                <div class="row">
                    <div class="portfolio-wrapper text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Master/M5.jpg" alt="" />
                                <h4>นายณัฐกิตติ์ ศรีกาญจนเพริศ</h4>
                                <p>วิทยาการคอมพิวเตอร์<br>
                                    Research: -<br> 
                                </p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Master/M6.jpg" alt="" />
                                <h4>นายบุญชู ศรีขัดเค้า</h4>
                                <p>วิทยาการคอมพิวเตอร์<br>
                                    Research: Bugs Reports<br> 
                                </p>
                            </div>
                        </div>

                    </div>
                </div>   
                <!-- Master End -->
                <div class="head_title text-center margin-top-80">
                    <p>Bachelor Students</p>
                </div>
                <div class="row">
                    <div class="portfolio-wrapper text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B1.jpg" alt="" />
                                <h4>นายสกรานต์ อาษานาม <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="assets/images/Students/Bachelor/B2.jpg" alt="" />
                                <h4>นางสาวไอลดา กิ่งโรชา <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B3.jpg" alt="" />
                                <h4>นางสาวรดาวรรณ ศรีกาญจนเพริศ</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B4.jpg" alt="" />
                                <h4>นายฉัตรเพชร ทรงบรรพต <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                    </div>
                </div>     
                <!-- row 2 -->
                <div class="row">
                    <div class="portfolio-wrapper text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B5.jpg" alt="" />
                                <h4>นายยศกฤต สุระวิทย์ <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B6.jpg" alt="" />
                                <h4>นางสาวเยาวภา วงศ์อามาตย์ <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B7.jpg" alt="" />
                                <h4>นางสาวสรินยา แพงขวา <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B8.jpg" alt="" />
                                <h4>นายภาณุพงษ์ สิงหราช <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                    </div>
                </div>   
                <!-- row 3 -->
                <div class="row">
                    <div class="portfolio-wrapper text-center">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B9.jpg" alt="" />
                                <h4>กิตติธร เสริมไธสง <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="community-edition community-edition-bg-s">
                                <img src="<?php echo base_url(); ?>assets/images/Students/Bachelor/B10.jpg" alt="" />
                                <h4>นายอดิศร ฆารไสว <br>&nbsp;</h4>
                                <p>วิทยาการคอมพิวเตอร์</p>
                            </div>
                        </div>
                        

                    </div>
                </div>            
            </div> <!-- /container -->       
        </section>