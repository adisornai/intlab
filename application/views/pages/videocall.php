<?php 
	$data_user = array(
		'uid' => $this->session->U_id,
		'username' => $this->session->U_name,
		'password' => $this->session->U_vdopass,
		'room' => 'IntLab'
	);
 ?>
<script type="text/javascript">
var data_userPHP = <?php echo json_encode($data_user); ?>;
</script>
<div class="container-fluid vdo-bg vdo-container vdo-ht">
	<div class="container">
		<div>
			<span>Verion v.2.3</span>
		</div>
		<div class="row zmp">
			<div class="col-xs-3 zmp">
		
			</div>
			<div class="col-xs-9 zmp chat video-hide">
				<div class="zmp text-left chat-head">
					<h1>CHAT INTLAB <i class="far fa-comment-alt"></i></h1>
				</div>
				<div class="zmp chat-message" id="chat-message">
					
				</div>
				<div>
					<textarea class="form-control" id="chat-msg-send" rows="3" placeholder="Type a message"></textarea>
					<button type="" class="chat-btn-send" id="chat-btn-send">SEND</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div id="call-user-div" class="video-hide">
					<h3>Call From <span id="call-user"></span></h3>
					<button type="" class="margin-bottom-20 video-btn-accept j-accept">Accept</button>
					<button type="" class="video-btn-decline j-decline">Decline</button>
				</div>
			</div>
		</div>
		<div class="row margin-bottom-20">
			<button id="" class="video-btn-b j-login">Join Room</button>
			<button id="" class="video-btn-b j-logout video-hide">Leave</button>
		</div>
		<div class="load-time video-hide">
			<img src="<?php echo base_url(); ?>assets/images/loader2.gif" alt="">
		</div>
		<div class="row mainVideo video-hide">
			<div class="col-xs-3 col-sm-3 col-md-3">
				<div class="col-xs-10 col-sm-10 col-md-10">
					<span class="span-online">Online </span><i class="icon-online fas fa-circle"></i>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2">
					<button type="" class="video-j-refresh j-refresh"><i class="fas fa-sync-alt"></i></button>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 list-users">
					
				</div>
			</div>			
			<div class="col-xs-6 col-sm-6 col-md-6">
				<div class="vdo-main">
					<video id="main_video"></video>
					<span><?php echo $this->session->U_name; ?></span>
				</div>
				<div class="vdo-local">
					
				</div>
			</div>
			<div class="video-form-btn col-xs-3 col-sm-3 col-md-3">
				<div class="jj-actions">
					<button type="" class="margin-bottom-20 video-btn-a j-actions" data-call="video">VIDEO CALL</button>
					<button type="" class="margin-bottom-20 video-btn-a j-actions" data-call="audio">AUDIO CALL</button>
				</div>
				<div class="jj-end video-hide">
					<button type="" class="margin-bottom-20 video-btn-a j-end" data-call="endcall">END CALL</button>
				</div>
				
			</div>
		</div>
	</div>
	
</div>






