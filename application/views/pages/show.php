<section id="contact" class="contact sections">
            <div class="container show-bg">
                <div class="row">
                    <h3 class="show-head-top text-thai">Lecturer</h3>
                </div>
                <div class="row text-thai" style="margin-bottom: 30px;">
                    <div class="col-lg-3 show-div-img">
                        <img src="<?php echo base_url(); ?>assets/images/Member/teacher1.jpg" alt="" />
                    </div>
                    <div class="col-lg-8 show-div-text">
                        <div>
                            <span class="show-head">ชื่ออาจารย์ : </span>
                            <span class="show-text">ผศ. จันทิมา พลพินิจ</span>
                        </div>
                        <div>
                            <span class="show-head">ชื่ออาจารย์(อังกฤษ) : </span>
                            <span class="show-text">Assistant Profes Jantima Polpinij</span>
                        </div>
                        <div>
                            <span class="show-head">ตำแหน่งปัจจุบัน : </span>
                            <span class="show-text">ผู้ช่วยศาสตราจารย์</span>
                        </div>
                        <div>
                            <span class="show-head">หน่วยงานสังกัด : </span>
                            <span class="show-text">มหาวิทยาลัยมหาสารคาม</span>
                        </div>
                        <div>
                            <span class="show-head">ที่อยู่ที่สามารถติดต่อได้ : </span>
                            <span class="show-text">คณะวิทยาการสารสนเทศ มหาวิทยาลัยมหาสารคาม ต.ขามเรียง อ.กันทรวิชัย จ.มหาสารคาม 44150</span>
                        </div>
                        <div>
                            <span class="show-head">โทรศัพท์ : </span>
                            <span class="show-text"> - </span>
                        </div>
                        <div>
                            <span class="show-head">อีเมล์ : </span>
                            <span class="show-text">jantima.p@msu.ac.th</span>
                        </div>
                    </div>
                </div>
                <div class="row text-thai">
                    <div>
                        <h3 class="show-head-top">Research</h3>
                        <table class="table">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th style="width:50%;">ชื่องานวิจัย</th>
                                    <th>ปีที่ทำงานวิจัย</th>
                                    <th>สถานะ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="<?php echo base_url(); ?>research/showresearch">1</a></td>
                                    <td><a href="<?php echo base_url(); ?>research/showresearch">วิจัยเรื่องที่ 1</a></td>
                                    <td>2552</td>
                                    <td>งานวิจัยที่ทำสำเร็จ</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row text-thai">
                    <div>
                        <h3 class="show-head-top">Publications</h3>
                        <table class="table">
                            <thead>
                                <tr >
                                    <th>#</th>
                                    <th style="width:50%;">ชื่องานวิจัยที่ตีพิมพ์</th>
                                    <th>ปีที่ตีพิมพ์</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="<?php echo base_url(); ?>research/showresearch">1</a></td>
                                    <td><a href="<?php echo base_url(); ?>research/showresearch">วิจัยเรื่องที่ 1</a></td>
                                    <td>2552</td>
                             
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section><!-- End of Contact Section -->