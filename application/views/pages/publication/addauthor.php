<div class="container profile-bg">
    <div class="row p-10 pl-stc-20 btn-r-t bg-def">
         <a class="text-light show-b" href="<?=base_url('publication')?>" title=""><i class="fas fa-chevron-left"></i> Back</a>
    </div>
    <div class="row rpt-20">
        <div class="col-md-offset-1 col-md-10">
            <div>
                <div class="form-group">
                    <label>
                        Publication :
                    </label>
                    <span>
                        <?=$mypublication->p_nameEN?>
                    </span>
                </div>
            </div>
            <div class="form-group" id="">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th> 
                            <th class="text-center">Remove</th> 
                        </tr>
                    </thead>
                    <tbody id="show-publication">
                        <?php 
                        /*foreach ($mycoresearcher as $row) {
                            echo "<tr>";
                            echo "<td></td>";
                            echo "<td></td>";
                            echo "<td><button class='delco'>DEL</button></td>";
                            echo "<tr>";
                        }*/
                         ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-offset-1 col-md-10">
            <form action="" method="post" id="form-core-submit">  
                <div class="col-md-5 p-0">
                    <div class="form-group">
                        <select class="form-control" id="users">
                            <option value=""></option>
                            <?php 
                            $myid = $this->session->U_id;
                            foreach ($student as $row) {
                                if($myid != $row->s_id && $mypublication->p_first_author != $row->s_id && $mypublication->p_second_author != $row->s_id){
                                    echo "<option value='$row->s_id'>$row->s_fnameEN $row->s_lnameEN</option>";
                                }  
                            }
                             ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 p-0">
                    <div class="form-group">
                        <input type="submit" class="form-control profile-btn" id="" name="" value="ADD">
                    </div>
                </div>
            </form>
            
        </div>
    </div>
    <div class="row p-10 ">
        <div class="">
            
        </div>
        <div class="">
            
        </div>
        
    </div>
    
</div>
<script type="text/javascript">
    

    $(document).ready(function() {
        var path = "<?=base_url()?>";
        var pid = "<?=$myidpublication?>";
        function load_author(){
            $.post(path+'publication/load_author_js', {pid: pid}, function(data) {
                var result = JSON.parse(data);
                console.log(result);
                var length = result.length;
                $('#show-publication').empty()
                if(length > 0){
                    for (var i = 0; i < length; i++) {
                        $('#show-publication').append(
                            '<tr>'+
                            '<td>'+(i+1)+'</td>'+
                            '<td>'+result[i].s_fnameEN+' '+result[i].s_lnameEN+'</td>'+
                            '<td align="center" ><button class="delco btn-remove" data-auid="'+result[i].au_id+'" data-sid="'+result[i].au_sid+'"><i class="fas fa-minus-circle"></i></button></td>'+
                            '</tr>'
                        );
                    }
                }
                else{
                    $('#show-publication').append(
                        "<tr>"+
                        "<td align='center' colspan='3'>Not Author</td>"+
                        "</tr>"
                    );
                }
            });
        }

        function add_author(){
            var s_id = $('#users').val();
            $.post(path+'publication/add_author', {sid: s_id,pid: pid}, function(data) {
                var result = JSON.parse(data);
                console.log(result);
                if(result.status){
                    swal(
                      'Add Success',
                      '',
                      'success'
                    );
                    load_author();
                }
                else{
                    swal({
                      type: 'error',
                      title: 'Oops...',
                      text: 'Something went wrong!',
                      footer: ''
                  });
                }
                
            });
        }

        function delete_author(id, sid){
            swal({
              title: 'Are you sure?',
  
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                $.post(path+'publication/delete_author', {au_id: id , au_sid: sid}, function(data) {
                    if(data){
                        swal(
                          'Deleted!',
                          '',
                          'success'
                        );
                        setTimeout(function(){
                            load_author();
                        }, 100);
                        

                    }
                });
                
              }

            });
        }


        /* load */
        load_author();

        $('#form-core-submit').submit(function(event) {
            event.preventDefault();
            add_author();
            
        });

        /*$('.delco').click(function(event) {
            console.log('test');
        }); */

        $(document).on('click', '.delco', function(){ 
            var au_id = $(this).data('auid');
            var au_sid = $(this).data('sid');
            delete_author(au_id, au_sid);
        });
       
    });
</script>
