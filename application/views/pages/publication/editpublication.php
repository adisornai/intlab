<div class="container profile-bg">
    <div class="row p-10 pl-stc-20 btn-r-t bg-def">
        <a href="<?=base_url('publication');?>" class="text-light show-b"><i class="fas fa-chevron-left"></i> Back</a>
    </div>
    <div class="row p-10">
        <div class="col-md-offset-3 col-md-6">
            <form action="" method="post">
                <div class="col-md-6 zmp pr-5">
                    <div class="form-group">
                        <span>Publication Standards</span>
                        <select class="form-control" id="p_ps_id" name="p_ps_id">
                            <option value="">Choose ...</option>
                        </select>
                        <span class="text-error"><?=form_error('p_ps_id');?></span>
                    </div>
                </div>
                <div class="col-md-6 zmp pl-5">
                    <div class="form-group">
                        <span>Publication Type</span>
                        <select class="form-control" id="p_status_type" name="p_status_type">
                            <option value="">Choose ...</option>
                        </select>
                        <span class="text-error"><?=form_error('p_status_type');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <span>Name Publication Thai</span>
                    <input type="text" class="form-control" name="p_nameTH" value="<?=set_value('p_nameTH')?set_value('p_nameTH'):$mydata->p_nameTH?>" placeholder="">
                    <span class="text-error"><?=form_error('p_nameTH');?></span>
                </div>
                <div class="form-group">
                    <span>Name Publication English</span>
                    <input type="text" class="form-control" name="p_nameEN" value="<?=set_value('p_nameEN')?set_value('p_nameEN'):$mydata->p_nameEN?>" placeholder="">
                    <span class="text-error"><?=form_error('p_nameEN');?></span>
                </div>
                 <div class="form-group">
                    <span>First Author</span>
                    <select class="form-control" id="p_first_author" name="p_first_author">
                        <option value="">Choose ...</option>
                    </select>
                    <span class="text-error"><?=form_error('p_first_author');?></span>
                </div>
                <div class="form-group">
                    <span>Second Author</span>
                    <select class="form-control" id="p_second_author" name="p_second_author">
                        <option value="">Choose ...</option>
                    </select>
                    <span class="text-error"><?=form_error('p_second_author');?></span>
                </div>
                <div class="form-group">
                    <span>Year</span>
                    <input type="date" class="form-control" name="p_year" value="<?=set_value('p_year')?set_value('p_year'):$mydata->p_year?>" placeholder="">
                    <span class="text-error"><?=form_error('p_year');?></span>
                </div>
                <div class="form-group">
                    <span>Detail</span>
                    <textarea rows="5" class="form-control" name="p_detail"><?=set_value('p_detail')?set_value('p_detail'):$mydata->p_detail?></textarea>
                    <span class="text-error"><?=form_error('p_detail');?></span>
                </div>
                <div class="form-group">
                    <span>Language</span>
                    <select class="form-control" id="p_language" name="p_language">
                        
                    </select>
                    <span class="text-error"><?=form_error('p_language');?></span>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-8 p-0">
                        <input type="submit" class="form-control profile-btn btn-r" name="edit_publication" value="Submit" placeholder="">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var path = "<?=base_url();?>";
        function load_PS(){
            $.post(path+"publication/load_PS", function(data) {
                var result = JSON.parse(data);
                var r = result.length;
                var val = "<?=set_value('p_ps_id')?set_value('p_ps_id'):$mydata->p_ps_id?>";
                for(var i = 0 ; i < r ; i ++){
                    if(val == result[i].ps_id){
                        $('#p_ps_id').append(
                            '<option value="'+result[i].ps_id+'" selected>'+result[i].ps_name+'</option>'
                        );
                    }
                    else{
                        $('#p_ps_id').append(
                            '<option value="'+result[i].ps_id+'">'+result[i].ps_name+'</option>'
                        );
                    }
                }
            });
        }
        function load_PT(){
            $.post(path+"publication/load_PT", function(data) {
                var result = JSON.parse(data);
                var r = result.length;
                var val = "<?=set_value('p_status_type')?set_value('p_status_type'):$mydata->p_status_type?>";
                for(var i = 0 ; i < r ; i ++){
                    if(val == result[i].pt_id){
                        $('#p_status_type').append(
                            '<option value="'+result[i].pt_id+'" selected>'+result[i].pt_name+'</option>'
                        );
                    }
                    else{
                        $('#p_status_type').append(
                            '<option value="'+result[i].pt_id+'">'+result[i].pt_name+'</option>'
                        );
                    }
                    
                }
            });
        }
        function load_Language(){
            var val = "<?=set_value('p_language')?set_value('p_language'):$mydata->p_language?>";
            if(val){
                if(val == 1){
                    $('#p_language').append('<option value="1" selected>Thai</option>');
                    $('#p_language').append('<option value="0">English</option>');
                }
                else{
                    $('#p_language').append('<option value="1">Thai</option>');
                    $('#p_language').append('<option value="0" selected>English</option>');
                }
            }
            else{
                $('#p_language').append('<option value="1">Thai</option>');
                $('#p_language').append('<option value="0">English</option>');
            }
            
        }
        function load_First(){
            $.post(path+"publication/load_author", function(data) {
                var result = JSON.parse(data);
                result = result.student;
                var r = result.length;
                var val = "<?=set_value('p_first_author')?set_value('p_first_author'):$mydata->p_first_author?>";
                for(var i = 0 ; i < r ; i ++){
                    if(val == result[i].s_id){
                        $('#p_first_author').append(
                            '<option value="'+result[i].s_id+'" selected>'+result[i].s_fnameEN+' '+result[i].s_lnameEN+'</option>'
                        );
                    }
                    else{
                        $('#p_first_author').append(
                            '<option value="'+result[i].s_id+'">'+result[i].s_fnameEN+' '+result[i].s_lnameEN+'</option>'
                        );
                    }
                    
                }
            });
        }
        function load_Second(){
            $.post(path+"publication/load_author", function(data) {
                var result = JSON.parse(data);
                result = result.student;
                var r = result.length;
                var val = "<?=set_value('p_second_author')?set_value('p_second_author'):$mydata->p_second_author?>";
                for(var i = 0 ; i < r ; i ++){
                    if(val == result[i].s_id){
                        $('#p_second_author').append(
                            '<option value="'+result[i].s_id+'" selected>'+result[i].s_fnameEN+' '+result[i].s_lnameEN+'</option>'
                        );
                    }
                    else{
                        $('#p_second_author').append(
                            '<option value="'+result[i].s_id+'">'+result[i].s_fnameEN+' '+result[i].s_lnameEN+'</option>'
                        );
                    }
                    
                }
            });
        }
        load_PS();
        load_PT();
        load_Language();
        load_First();
        load_Second();
    });
</script>