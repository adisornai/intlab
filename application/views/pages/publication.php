<?php 
	function language($language){
		if($language == 1){
			$str = 'Thai';
		}
		else
		{	
			$str = 'English';
		}
		return $str;
	}
	function checkfile($file){
		if($file == ''){
			$f = 'upload-file-no';
		}
		else{
			$f = 'upload-file-yes';
		}
		return $f;
	}
	function year($y){
		$date=date_create($y);
		return date_format($date,"Y");
	}

 ?>
<div class="container profile-bg">
	<div class="row p-10 pl-stc-20 btn-r-t bg-def">
			<a href="<?php echo base_url('publication/addpublication'); ?>" class="text-light show-b"><i class="fas fa-external-link-alt"></i> Add Publication</a>
	</div>
	<div class="row p-10">
		<div class="table-responsive">
			<table class="table ">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Language</th>
						<th>Year</th>
						<th class="text-center">Upload File</th>
						<th class="text-center">Add Author</th>
						<th class="text-center">Edit</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1;
					$id = $this->session->U_id;
					foreach ($publication as $row) {
						if($row->p_first_author == $id || $row->p_second_author == $id){
							$language = language($row->p_language);
							$f = checkfile($row->p_links);
							$y = year($row->p_year);
							echo "<tr>";
							echo "<td><a href=".base_url('research/showpublication/'.$row->p_id).">$i</a></td>";
							echo "<td><a href=".base_url('research/showpublication/'.$row->p_id).">$row->p_nameEN $row->p_nameTH</a></td>";
							echo "<td>$language</td>";
							echo "<td>$y</td>";
							echo "<td align='center'><a href=".base_url('publication/uploadpublication/'.$row->p_id)." class=".$f."><i class='fas fa-upload'></i></a></td>";
							echo "<td align='center'><a href=".base_url('publication/addauthor/'.$row->p_id)."><i class='fas fa-plus-square'></i></a></td>";
							echo "<td align='center'><a href=".base_url('publication/edit-publication/'.$row->p_id)."><i class='fas fa-edit'></i></a></td>";
							echo "</tr>";
							$i = $i + 1;
						}
					}
					 ?>
				</tbody>
			</table>
		</div>
	</div>
</div>