<div class="container profile-bg btn-r m-h">
    <div class="row p-10 pl-stc-20 btn-r-t bg-def">
        <a class="text-light show-b" href="<?=base_url('research')?>" title=""><i class="fas fa-chevron-left"></i> Back</a>
    </div>
    <div class="row">
        <div class="pt-10">
            <div class="col-xs-12 col-sm-4 col-md-4 col-sm-offset-1 col-md-offset-1">
                <div class="col-xs-5 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <div class="pt-10">
                            <h1><img class="show-img" src="<?=base_url('assets/images/microscope.png')?>"></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-6 show-size-font-content">
                <div class="form-group">
                    <h3>Research</h3>
                </div>
                <div class="form-group">
                    <label>Research :</label><span> <?=$myres->r_research?></span>
                </div>
                <div class="form-group">
                    <label>Research Name Thai :</label><span> <?=$myres->r_nameTH?></span>
                </div>
                <div class="form-group">
                    <label>Research Name English :</label><span> <?=$myres->r_nameEN?></span>
                </div>
                <div class="form-group">
                    <label>Researcher :</label><span> <?=$prename->n_engname?> <?=$head->s_fnameEN?> <?=$head->s_lnameEN?></span>
                </div>
                <div class="form-group">
                    <label>Co-Researcher</label>
                    <?php 
                    $n = count($coresearcher);
                    if($n > 0){
                        echo "<br>";
                        foreach ($coresearcher as $row) {
                            echo $row->n_engname." ".$row->s_fnameEN." ".$row->s_lnameEN;
                        }
                    }
                    else{
                        echo "<span>: None</span>";
                    }


                     ?>
                    
                </div>
                <div class="form-group">
                    <label>Source of Investment Funds :</label><span> <?=$myres->r_source?></span>
                </div>
                <div class="form-group">
                    <label>Budget :</label><span> <?=$myres->r_budget?></span>
                </div>
                <?php 
                if($myres->r_status == 0){
                    $str = 'InComplete';
                }
                else{
                    $str = 'Complete';
                }

                 ?>
                <div class="form-group">
                    <label>Status :</label><span> <?=$str?></span>
                </div>
                <div class="form-group">
                    <label>File :</label><span> <a href="<?=base_url($myres->r_file)?>" title="">Download</a></span>
                </div>
            </div>
                
        </div>
    </div>
</div>