<div class="container profile-bg">
    <div class="row p-10 pl-stc-20 btn-r-t bg-def">
        <a href="<?=base_url('research');?>" class="text-light show-b"><i class="fas fa-chevron-left"></i> Back</a>
    </div>
    <div class="row p-10">
        <div class="col-md-offset-3 col-md-6">
            <form action="" method="post">
                <div class="form-group">
                    <span>Research</span>
                    <input type="text" class="form-control" name="r_research" value="" placeholder="">
                    <span class="text-error"><?=form_error('r_research');?></span>
                </div>
                <div class="form-group">
                    <span>Name Research Thai</span>
                    <input type="text" class="form-control" name="r_nameTH" value="" placeholder="">
                    <span class="text-error"><?=form_error('r_nameTH');?></span>
                </div>
                <div class="form-group">
                    <span>Name Research English</span>
                    <input type="text" class="form-control" name="r_nameEN" value="" placeholder="">
                    <span class="text-error"><?=form_error('r_nameEN');?></span>
                </div>
                <div class="form-group">
                    <span>Source of Investment Funds</span>
                    <input type="text" class="form-control" name="r_source" value="" placeholder="">
                    <span class="text-error"><?=form_error('r_source');?></span>
                </div>
                <div class="form-group">
                    <span>Budget</span>
                    <input type="text" class="form-control" name="r_budget" value="" placeholder="">
                    <span class="text-error"><?=form_error('r_budget');?></span>
                </div>
                <div class="col-md-6 zmp pr-5">
                    <div class="form-group">
                        <span>Begin</span>
                        <select class="form-control" id="begin" name="r_begin">
                            
                        </select>
                        <span class="text-error"><?=form_error('r_begin');?></span>
                    </div>
                </div>
                <div class="col-md-6 zmp pl-5">
                     <div class="form-group">
                        <span>End</span>
                        <select class="form-control" id="end" name="r_end">
                            
                        </select>
                        <span class="text-error"><?=form_error('r_end');?></span>
                    </div>
                </div>
                <div class="form-group">
                    <span>Status</span>
                    <select class="form-control" id="status" name="r_status"></select>
                    <span class="text-error"><?=form_error('r_status');?></span>
                </div>
                
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-8 p-0">
                        <input type="submit" class="form-control profile-btn btn-r" name="add_research" value="Submit" placeholder="">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        function load_begin(){
            $('#begin').append('<option value="">Choose ...</option>');
            var d = new Date();
            var datenow = d.getFullYear();
            for (var i = 1900; i <= datenow; i++) {
                $('#begin').append('<option value="'+i+'">'+i+'</option>');
            }
        }
        function load_end(){
            $('#end').append('<option value="">Choose ...</option>');
            var d = new Date();
            var datenow = d.getFullYear();
            for (var i = 1900; i <= datenow; i++) {
                $('#end').append('<option value="'+i+'">'+i+'</option>');
            }
        }
        function load_status(){
            $('#status').append('<option value="1">Complete</option>');
            $('#status').append('<option value="0">InComplete</option>');
        }
        load_begin();
        load_end();
        load_status();
        
    });
</script>