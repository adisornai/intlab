<div class="container profile-bg">
	<div class="row p-10 pl-stc-20 btn-r-t bg-def">
		 <a class="text-light show-b" href="<?=base_url('research')?>" title=""><i class="fas fa-chevron-left"></i> Back</a>
	</div>
    <div class="row p-10 ">
        <div class="col-md-12">
            <form method="post" id="upload_form" align="center" enctype="multipart/form-data">
                <div class="col-xs-12 margin-bottom-5">
                    <div class="col-sm-offset-4 col-sm-4">
                        <div id="upload-show-img">
                        	
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="upload-btn-wrapper">
                        <button type="btn" class="choose-btn">Choose File .pdf</button>
                        <input type="file" name="image_file" id="image_file" value="" placeholder="" directory multiple accept=".pdf">
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" name="upload" id="upload" value="Upload" class="upload-btn">
                </div>
            </form>
        </div>
        
    </div>
    
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#image_file').change(function(event) {
			var tmppath = event.target.files[0].name;
			$('#upload-show-img').html("<h1 class='upload-file-select'><i class='fas fa-file-pdf'></i></h1><h4>"+tmppath+"</h4>");
		});
		$('#upload_form').on('submit', function(event) {
			event.preventDefault();
			$('#uploaded_image').empty();
			if($('#image_file').val() == ''){
				alert('Please Select the File');
			}
			else{
				var formData = new FormData(this);
				formData.append('id',<?=$rid?>)
				$.ajax({
					url: '<?=base_url('research/ajax_upload_file');?>',
					method: 'POST',
					data:formData,
					contentType: false,
					cache: false,
					processData:false,
					success:function(data){
						console.log(data);
						location.href = "<?=base_url('research');?>"
					}
				});

			}
		});
	});
</script>
