<div class="container profile-bg">
	<div class="row p-10 pl-stc-20 btn-r-t bg-def">
			<a href="<?php echo base_url('research/addresearch'); ?>" class="text-light show-b"><i class="fas fa-external-link-alt"></i> Add Research</a>
	</div>
	<div class="row p-10">
		<div class="table-responsive">
			<table class="table ">
				<thead>
					<tr>
						<th>#</th>
						<th>Research</th>
						<th>Status</th>
						<th class="text-center">Upload File</th>
						<th class="text-center">Add Co-Researcher</th>
						<th class="text-center">Edit</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i = 1;
					foreach ($research as $row) {
						$status = $row->r_status;
						if($status == 1){
							$str = 'Complete';
						}
						else
						{	
							$str = 'InComplete';
						}
						if($row->r_file == ''){
							$f = 'upload-file-no';
						}
						else{
							$f = 'upload-file-yes';
						}
						echo "<tr>";
						echo "<td><a href=".base_url('research/showresearch/'.$row->r_id).">$i</a></td>";
						echo "<td><a href=".base_url('research/showresearch/'.$row->r_id).">$row->r_nameEN $row->r_nameTH</a></td>";
						echo "<td>$str</td>";
						echo "<td align='center'><a href=".base_url('research/uploadfileresearch/'.$row->r_id)." class=".$f."><i class='fas fa-upload'></i></a></td>";
						echo "<td align='center'><a href=".base_url('research/addcoresearcher/'.$row->r_id)."><i class='fas fa-plus-square'></i></a></td>";
						echo "<td align='center'><a href=".base_url('research/edit-research/'.$row->r_id)."><i class='fas fa-edit'></i></a></td>";
						echo "</tr>";
						$i = $i + 1;
					}
					 ?>
				</tbody>
			</table>
		</div>
	</div>
</div>