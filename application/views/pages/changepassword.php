<div class="container profile-bg">
    <div class="row p-10">
        <div class="col-md-offset-1 col-md-4">
            <form method="post" action="" name="" id="change_form">
                <div class="form-group">
                    <span>New Password</span>
                    <input type="password" class="form-control" name="pass" value="" placeholder="">
                    <span class="text-error"><?=form_error('pass');?></span>
                </div>

                <div class="form-group">
                    <span>Confime Password</span>
                    <input type="password" class="form-control" name="cpass" value="" placeholder="">
                    <span class="text-error"><?=form_error('cpass');?></span>
                </div>

                <div class="form-group margin-top-20">
                    <div class="col-md-6 zmp">
                        <input type="submit" class="upload-btn form-control" name="change_form" id="submit" value="Submit">
                    </div>  
                </div>
            </form>
        </div>
        
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
    });
</script>
