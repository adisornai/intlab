<div class="container">
	<div class="admin-col-1 col-md-3 zero-margin-padding">
		<div class="admin-home">
			<a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a>
		</div>
		<div class="admin-box-1">
			<button type="" id="manage-student"><label class="admin-label1">Manage Student</label><label class="admin-label2"><i class="fas fa-list"></i></label></button>
			<div class="admin-student-menu">
				<a href="<?php echo base_url(); ?>admin/student"><label class="admin-label1-menu"><i class="fas fa-address-card"></i></label><label class="">Manage Personal</label></a>
				<a href="<?php echo base_url(); ?>admin/research"><label class="admin-label1-menu"><i class="fab fa-researchgate"></i></label><label class="">Manage Research</label></a>
				<a href="<?php echo base_url(); ?>admin/publish"><label class="admin-label1-menu"><i class="fab fa-leanpub"></i></label><label class="">Manage Publish</label></a>
			</div>
		</div>
		<div class="admin-box-2">
			<button type="" id="manage-lecturer"><label class="admin-label1">Manage Lecturer</label><label class="admin-label2"><i class="fas fa-list"></i></label></button>
			<div class="admin-lecturer-menu">
				<a href="<?php echo base_url(); ?>admin/lecturer"><label class="admin-label1-menu"><i class="fas fa-address-card"></i></label><label class="">Manage Personal</label></a>
				<a href="<?php echo base_url(); ?>admin/research_lt"><label class="admin-label1-menu"><i class="fab fa-researchgate"></i></label><label class="">Manage Research</label></a>
				<a href="<?php echo base_url(); ?>admin/publish_lt"><label class="admin-label1-menu"><i class="fab fa-leanpub"></i></label><label class="">Manage Publish</label></a>
			</div>
		</div>
	</div>
	<div class="admin-col-2 col-md-9">
		<div class="row">	
			
		</div>
	</div>
</div>