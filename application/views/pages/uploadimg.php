<div class="container profile-bg">
    <div class="row p-10">
        <div class="col-md-3">
            <div id="uploaded_image">
                
            </div>
        </div>
        <div class="col-md-12">
            <form method="post" id="upload_form" align="center" enctype="multipart/form-data">
                <div class="col-xs-12 margin-bottom-5">
                    <div class="col-sm-offset-5 col-sm-2">
                        <div id="upload-show-img">
                        
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="upload-btn-wrapper">
                        <button type="btn" class="choose-btn">Choose img .jpg</button>
                        <input type="file" name="image_file" id="image_file" value="" placeholder="" directory multiple>
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" name="upload" id="upload" value="Upload" class="upload-btn">
                </div>
            </form>
        </div>
        
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#image_file').change(function(event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            $('#upload-show-img').html("<img src="+tmppath+" class='profile-img'>");
        });
        $('#upload_form').on('submit', function(event) {
            event.preventDefault();
            $('#uploaded_image').empty();
            if($('#image_file').val() == ''){
                alert('Please Select the File');
            }
            else{
                $.ajax({
                    url: '<?=base_url('profile/ajax_upload');?>',
                    method: 'POST',
                    data:new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success:function(data){
                        console.log(data);
                        location.href = "<?=base_url('profile');?>"
                    }
                });
                
            }
        });
    });
</script>
