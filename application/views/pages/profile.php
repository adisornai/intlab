<section id="contact" class="contact sections text-thai">
            <div class="container">
                <div class="row">
                    <h3 class="show-head-top">ข้อมูลส่วนตัว</h3>
                </div>
                
                <?php echo form_open('profile/update'); ?>
                <div class="row" style="margin-top:15px;">
                    <div class="col-sm-12 div-input-profile">
                        <div class="div-input-profile-d1">
                            
                            <div class="div-input-profile-padding div-input-profile-margin-15">
                                <input type="text" name="s_id" value="<?php echo $f_data->s_id ?>" placeholder="id" class="input-profile-width-one" disabled>
                            </div> 

                            <div class="div-input-profile-padding div-input-profile-margin-15">
                                <div class="div-input-profile-margin-5">
                                    <select name="s_prename" class="input-profile-width-one">
                                        <?php foreach ($prename as $row) { ?>

                                            <option value="<?php echo $row->n_id; ?>"><?php echo $row->n_engname; ?></option>

                                        <?php } ?>
                                    </select>
                                </div>
                                <div>
                                    <input type="text" name="s_fnameEN" value="<?php echo $f_data->s_fnameEN ?>" placeholder="first name" id="profile-fname" class="input-profile-width-two div-input-profile-margin-5">
                                    <input type="text" name="s_lnameEN" value="<?php echo $f_data->s_lnameEN ?>" placeholder="last name" class="input-profile-width-two"> 
                                </div>
                            </div>


                            <div class="div-input-profile-padding div-input-profile-margin-15">
                                <div class="input-profile-width-one margin profile-title">
                                    <span>Birthday</span>
                                </div>
                                <input type="date" name="s_birthday" value="" placeholder="" class="input-profile-date">
                            </div>
                            
                            <div class="div-input-profile-margin-15">
                                <div class="div-input-profile-padding div-input-profile-margin-5">
                                    <select class="input-profile-width-one">
                                        <option value="">Computer Science</option>
                                    </select>
                                </div>
                                <div class="div-input-profile-padding div-input-profile-margin-5">
                                    <select class="input-profile-width-one">
                                        <option value="">Information</option>
                                    </select>
                                </div>
                                <div class="div-input-profile-padding">
                                    <input type="text" name="" value="Mahasarakham University" placeholder="university" class="input-profile-width-one" disabled>
                                </div>
                            </div>
                            

                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <select class="input-profile-width-one">
                                    <option value="">research</option>
                                </select>
                            </div>

                            <div class="div-input-profile-padding div-input-profile-margin-15">
                                <select class="input-profile-width-one">
                                    <?php foreach ($education as $row) { ?>

                                        <option value="<?php echo $row->e_id; ?>"><?php echo $row->e_name; ?></option>

                                    <?php } ?>
                                </select>
                            </div>

                            <div class="div-input-profile-padding div-input-profile-margin-15">
                                <div class="input-profile-width-one margin profile-title">
                                    <span>Time Period</span>
                                </div>
                                <input type="date" name="" value="" placeholder="" class="input-profile-width-two">
                                <input type="date" name="" value="" placeholder="" class="input-profile-width-two">
                            </div>

                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="adviser" class="input-profile-width-one">
                            </div>

                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="co-adviser" class="input-profile-width-one">
                            </div>

                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="phone" class="input-profile-width-one">
                            </div>

                            <div class="div-input-profile-padding">
                                <input type="text" name="" value="" placeholder="email" class="input-profile-width-one">
                            </div>

                        </div>
                        <div class="div-input-profile-d2 hide">
                            <div class="input-profile-width-one margin profile-title">
                                <span>Address</span>
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="house no" class="input-profile-width-one">
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="village no" class="input-profile-width-one">
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="lane" class="input-profile-width-one">
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <input type="text" name="" value="" placeholder="road" class="input-profile-width-one">
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                <select class="input-profile-width-one">
                                    <option value="">Province</option>
                                </select>
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                
                                <select class="input-profile-width-one">
                                    <option value="">District</option>
                                </select>
                               
                            </div>
                            <div class="div-input-profile-padding div-input-profile-margin-5">
                                
                                <select class="input-profile-width-one">
                                    <option value="">Sub District</option>
                                </select>
                            </div>
                            <div class="div-input-profile-padding">
                                <input type="text" name="" value="" placeholder="ZIP code" class="input-profile-width-one">
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="row" style="margin-top:30px;">
                    <div class="col-lg-12 div-add-re-2">
                        <div>
                            <input type="button" name="" value="ถัดไป" class="" id="next">
                            <input type="button" name="" value="ย้อนกลับ" class="back hide" id="back" style="margin-bottom:5px;">
                            <input type="submit" name="" value="ยืนยัน" class="back hide" id="">
                        </div>
                        
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </section><!-- End of Contact Section -->