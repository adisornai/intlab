<div class="container profile-bg p-0">
    <form id="update_form" method="post" action="">
        <div class="row zmp">
            <div class="col-sm-3 col-md-3 m-0 p-5 pt-5 btn-r bg-def">
                <?php 
                if($profile->s_img != NULL){
                ?>
                    <img class="profile-img" src="<?=base_url($profile->s_img);?>" alt="">
                <?php
                }
                else{
                ?>
                    <img class="profile-img" src="<?=base_url('assets/images/user-null.png');?>" alt="">
                <?php
                }
                 ?>
                
                <ul class="profile-menu">
                    <li><a class="text-light" href="<?=base_url('profile/uploadimg');?>"><i class="fas fa-file-image"></i><span>Upload image Profile</span></a></li>
                    <li><a class="text-light" href="<?=base_url('profile/changepassword')?>"><i class="fas fa-key"></i><span>Change Password</span></a></li>
                    <li><a class="text-light" href="<?=base_url('research')?>"><i class="fas fa-clipboard"></i><span>Research</span></a></li>
                    <li><a class="text-light" href="<?=base_url('publication')?>"><i class="fas fa-copy"></i><span>Publication</span></a></li>
                </ul>
                
            </div>
            <div class="col-sm-9 col-md-6 col-md-offset-1 p-5 rpt-20">
                <div class="form-group">
                    <span>Pre name</span>
                    <select class="form-control" id="s_prename" name="s_prename">
                        <?php 
                        foreach ($prename as $row) {
                            if((set_value('s_prename')?set_value('s_prename'):$profile->s_prename) == $row->n_id){
                                echo "<option value=".$row->n_id." data-prename_eng='".$row->n_engname."' selected>".$row->n_thainame."</option>";
                            }
                            else{
                                echo "<option value=".$row->n_id." data-prename_eng='".$row->n_engname."'>".$row->n_thainame."</option>";
                            }
                        }
                         ?>  
                    </select>
                    <span class="text-danger"><?=form_error('s_prename')?></span>
                </div>
                <div class="col-md-6 zmp pr-5">
                    <div class="form-group">
                        <span>First Name Thai</span>
                        <input type="text" class="form-control" id="s_fnameTH" name="s_fnameTH" value="<?php if(set_value('s_fnameTH')){echo set_value('s_fnameTH');}else{echo $profile->s_fnameTH;} ?>">
                        <span class="text-danger"><?=form_error('s_fnameTH')?></span>
                    </div>
                </div>
                <div class="col-md-6 zmp pl-5">
                    <div class="form-group">
                        <span>Last Name Thai</span>
                        <input type="text" class="form-control" id="s_lnameTH" name="s_lnameTH" value="<?php if(set_value('s_lnameTH')){echo set_value('s_lnameTH');}else{echo $profile->s_lnameTH;} ?>">
                        <span class="text-danger"><?=form_error('s_lnameTH')?></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <span>Pre name</span>
                    <select class="form-control" id="s_prenameEN" name="s_prenameEN" disabled></select>
                </div>
                <div class="col-md-6 zmp pr-5">
                    <div class="form-group">
                        <span>First Name English</span>
                        <input type="text" class="form-control" id="s_fnameEN" name="s_fnameEN" value="<?php if(set_value('s_fnameEN')){echo set_value('s_fnameEN');}else{echo $profile->s_fnameEN;} ?>">
                        <span class="text-danger"><?=form_error('s_fnameEN')?></span>
                    </div>
                </div>
                <div class="col-md-6 zmp pl-5">
                    <div class="form-group">
                        <span>Last Name English</span>
                        <input type="text" class="form-control" id="s_lnameEN" name="s_lnameEN" value="<?php if(set_value('s_lnameEN')){echo set_value('s_lnameEN');}else{echo $profile->s_lnameEN;} ?>">
                        <span class="text-danger"><?=form_error('s_lnameEN')?></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <span>Email</span>
                    <input type="text" class="form-control" id="s_email" name="s_email" value="<?php if(set_value('s_email')){echo set_value('s_email');}else{echo $profile->s_email;} ?>">
                    <span class="text-danger"><?=form_error('s_email')?></span>
                </div>
                <div class="form-group">
                    <span>Phone</span>
                    <input type="text" class="form-control" id="s_phone" name="s_phone" value="<?php if(set_value('s_phone')){echo set_value('s_phone');}else{echo $profile->s_phone;} ?>">
                    <span class="text-danger"><?=form_error('s_phone')?></span>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-6 col-md-offset-4 p-5">
            	<div class="col-md-6 zmp pr-5">
            		<div class="form-group">
	            		<span>Begin</span>
	                    <input type="date" class="form-control" id="s_begin" name="s_begin" value="<?php if(set_value('s_begin')){echo set_value('s_begin');}else{echo $profile->s_begin;} ?>">
	                    <span class="text-danger"><?=form_error('s_begin')?></span>
	            	</div>
            	</div>
            	<div class="col-md-6 zmp pl-5">
            		<div class="form-group">
	            		<span>End</span>
	                    <input type="date" class="form-control" id="s_end" name="s_end" value="<?php if(set_value('s_end')){echo set_value('s_end');}else{echo $profile->s_end;} ?>">
	                    <span class="text-danger"><?=form_error('s_end')?></span>
	            	</div>
            	</div>
            	
                <div class="form-group">
                    <span>University</span>
                    <select class="form-control" id="s_university" name="s_university">
                        <?php 
                        foreach ($university as $row) {
                            if((set_value('s_university')?set_value('s_university'):$profile->s_university) == $row->u_id){
                                echo "<option value=".$row->u_id." selected>".$row->u_nameEN."</option>";
                            }else{
                                echo "<option value=".$row->u_id.">".$row->u_nameEN."</option>";
                            }
                        }
                         ?>
                    </select>
                    <span class="text-danger"><?=form_error('s_university')?></span>
                </div>
                <div class="form-group">
                    <span>Faculty</span>
                    <select class="form-control" id="s_faculty" name="s_faculty"></select>
                    <span class="text-danger"><?=form_error('s_faculty')?></span>
                </div>
                <div class="form-group">
                    <span>Major</span>
                    <select class="form-control" id="s_major" name="s_major"></select>
                    <span class="text-danger"><?=form_error('s_major')?></span>
                </div>

                <div class="col-md-6 zmp pr-5">
	                 <div class="form-group">
	                    <span>Adviser</span>
	                    <select class="form-control" id="s_adviser" name="s_adviser">
	                    	<option value="">Choose ...</option>
	                    	<?php 
	                    	foreach ($lecturer as $row) {
	                    		if(set_value('s_adviser'))
	                    		{
	                    			if(set_value('s_adviser') == $row->t_id)
	                    			{
	                    				echo "<option value='".$row->t_id."' selected>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    			else
	                    			{
	                    				echo "<option value='".$row->t_id."'>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    		}
	                    		else
	                    		{
	                    			if($profile->s_adviser == $row->t_id)
	                    			{
	                    				echo "<option value='".$row->t_id."' selected>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    			else
	                    			{
	                    				echo "<option value='".$row->t_id."'>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    		}

	                    	}
	                    	?>
	                    </select>
	                    <span class="text-danger"><?=form_error('s_adviser')?></span>
	                </div>
				</div>
                <div class="col-md-6 zmp pl-5">
	                <div class="form-group">
	                    <span>Co-Adviser</span>
	                    <select class="form-control" id="s_coadviser" name="s_coadviser">
	                    	<option value="0">Choose ...</option>
	                    	<?php 
	                    	foreach ($lecturer as $row) {
	                    		if(set_value('s_coadviser'))
	                    		{
	                    			if(set_value('s_coadviser') == $row->t_id)
	                    			{
	                    				echo "<option value='".$row->t_id."' selected>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    			else
	                    			{
	                    				echo "<option value='".$row->t_id."'>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    		}
	                    		else
	                    		{
	                    			if($profile->s_coadviser == $row->t_id)
	                    			{
	                    				echo "<option value='".$row->t_id."' selected>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    			else
	                    			{
	                    				echo "<option value='".$row->t_id."'>".$row->t_fnameEN. " " .$row->t_lnameEN. "</option>";
	                    			}
	                    		}

	                    	}
	                    	?>
	                    </select>
	                    <span class="text-danger"><?=form_error('s_coadviser')?></span>
	                </div>
	            </div>

	            <div class="form-group">
	            	<span>Province</span>
	            	<select class="form-control" id="s_province" name="s_province">
	            		<option value="">Choose ...</option>
	            		<?php 
	            		foreach ($province as $row) {
	            			if(set_value('s_province'))
	            			{
	            				if(set_value('s_province') == $row->PROVINCE_ID)
	            				{
	            					echo "<option value='".$row->PROVINCE_ID."' selected>".$row->PROVINCE_NAME."</option>";
	            				}
	            				else
	            				{
	            					echo "<option value='".$row->PROVINCE_ID."'>".$row->PROVINCE_NAME."</option>";
	            				}
	            			}
	            			else
	            			{
	            				if($profile->s_province == $row->PROVINCE_ID)
	            				{
	            					echo "<option value='".$row->PROVINCE_ID."' selected>".$row->PROVINCE_NAME."</option>";
	            				}
	            				else
	            				{
	            					echo "<option value='".$row->PROVINCE_ID."'>".$row->PROVINCE_NAME."</option>";
	            				}
	            			}

	            		}
	            		?>
	            	</select>
	            	<span class="text-danger"><?=form_error('s_coadviser')?></span>
	            </div>
	       		
	       		<div class="col-md-6 zmp pr-5">
		            <div class="form-group">
		            	<span>Amphur</span>
		            	<select class="form-control" id="s_amphur" name="s_amphur">
		            		<option value="">Choose ...</option>
		            	</select>
		            	<span class="text-danger"><?=form_error('s_amphur')?></span>
		            </div>	
	       		</div>

	            <div class="col-md-6 zmp pl-5">
	         		<div class="form-group">
	                    <span>Postcode</span>
	                    <input type="text" class="form-control" id="s_zip" name="s_zip" value="<?php if(set_value('s_zip')){echo set_value('s_zip');}else{echo $profile->s_zip;} ?>">
	                    <span class="text-danger"><?=form_error('s_zip')?></span>
	                </div>
	            </div>

	            <div class="form-group">
	            	<span>District</span>
	            	<select class="form-control" id="s_district" name="s_district">
	            		<option value="">Choose ...</option>
	            	</select>
	            	<span class="text-danger"><?=form_error('s_district')?></span>
	            </div>


                <div class="col-md-6 zmp pr-5">
	         		<div class="form-group">
	                    <span>House No.</span>
	                    <input type="text" class="form-control" id="s_house_no" name="s_house_no" value="<?php if(set_value('s_house_no')){echo set_value('s_house_no');}else{echo $profile->s_house_no;} ?>">
	                    <span class="text-danger"><?=form_error('s_house_no')?></span>
	                </div>
	            </div>
	            <div class="col-md-6 zmp pl-5">
	            	<div class="form-group">
	                    <span>Village No.</span>
	                    <input type="text" class="form-control" id="s_village_no" name="s_village_no" value="<?php if(set_value('s_village_no')){echo set_value('s_village_no');}else{echo $profile->s_village_no;} ?>">
	                    <span class="text-danger"><?=form_error('s_village_no')?></span>
	                </div>
	            </div>
                
	            <div class="col-md-6 zmp pr-5">
	            	<div class="form-group">
	                    <span>Lane</span>
	                    <input type="text" class="form-control" id="s_lane" name="s_lane" value="<?php if(set_value('s_lane')){echo set_value('s_lane');}else{echo $profile->s_lane;} ?>">
	                    <span class="text-danger"><?=form_error('s_lane')?></span>
	                </div>
	            </div>
	            <div class="col-md-6 zmp pl-5">
	            	<div class="form-group">
	                    <span>Road</span>
	                    <input type="text" class="form-control" id="s_road" name="s_road" value="<?php if(set_value('s_road')){echo set_value('s_road');}else{echo $profile->s_road;} ?>">
	                    <span class="text-danger"><?=form_error('s_road')?></span>
	                </div>
	            </div>
                
                
            </div>

        </div>
    
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9 col-md-6 col-md-offset-4">
                <div class="form-group">
                    <input type="submit" class="form-control btn-block profile-btn btn-r" id="" name="update_profile" value="Save">
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    
    $(document).ready(function() {
        function load_prenameEN(){
            $('#s_prenameEN').empty();
            let prenameEN_ID = $('#s_prename option:selected').val();
            let prenameEN = $('#s_prename option:selected').data('prename_eng');
            if(prenameEN_ID){
                $('#s_prenameEN').append('<option value='+prenameEN_ID+'>'+prenameEN+'</option>');
            }

        }
        load_prenameEN();
        $('#s_prename').change(function(event) {
            load_prenameEN();
        });

        function load_faculty(){
            $('#s_faculty').empty();
            let path = "<?=base_url('pages/load_faculty')?>";
            let id = $('#s_university').val();
            $.post(path, {id: id}, function(res) {
                let result = JSON.parse(res);
                for (var i = 0; i < result.length; i++) {
                    let fid = result[i].f_id;
                    let fname = result[i].f_nameEN;
                    if(fid == <?=set_value('s_faculty')?set_value('s_faculty'):$profile->s_faculty?>)
                        $('#s_faculty').append('<option value='+fid+' selected>'+fname+'</option>');
                    else
                        $('#s_faculty').append('<option value='+fid+'>'+fname+'</option>');
                }
                load_major();
            });
        }
        load_faculty();
        $('#s_university').change(function(event) {
            load_faculty();
        });

        function load_major(){
            $('#s_major').empty();
            let path = "<?=base_url('pages/load_major')?>";
            let fid = $('#s_faculty').val();
            let uid = $('#s_university').val();
            $.post(path, {fid: fid,uid: uid}, function(res) {
                let result = JSON.parse(res);
                for (var i = 0; i < result.length; i++) {
                    let mid = result[i].m_id;
                    let mname = result[i].m_nameEN;
                    if(mid == <?=set_value('s_major')?set_value('s_major'):$profile->s_major?>)
                        $('#s_major').append('<option value='+mid+' selected>'+mname+'</option>');
                    else
                        $('#s_major').append('<option value='+mid+'>'+mname+'</option>');
                }
            });
            //console.log(fid+' '+uid);
        } 
        $('#s_faculty').change(function(event) {
            load_major();
        });       

        if($('#s_province').val() != "")
		{

			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_amphur',
				type: 'post',
				dataType: 'json',
				data: {id_p: $('#s_province').val()},
			})
			.done(function(res) {
				$('#s_amphur').empty();
				$('#s_amphur').append('<option value="">Choose ...</option>');
				var id = <?php if(set_value('s_amphur')){echo set_value('s_amphur');}else{echo $profile->s_amphur;} ?> ;
				
				$.each(res.amphur,function(index, el) {
					if(id == el.AMPHUR_ID)
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'" selected>'+el.AMPHUR_NAME+'</option>');
					}
					else
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'">'+el.AMPHUR_NAME+'</option>');
					}	
					
					
				});

				$.ajax({
				url: '<?php echo base_url(); ?>admin/get_district',
				type: 'post',
				dataType: 'json',
				data: {id_a: $('#s_amphur').val()},
				})
				.done(function(res) {
					$('#s_district').empty();
					$('#s_district').append('<option value="">Choose ...</option>');
					var did = <?php if(set_value('s_district')){echo set_value('s_district');}else{echo $profile->s_district;} ?> 
					$.each(res.district,function(index, el) {
						if(did == el.DISTRICT_ID)
						{
							$('#s_district').append('<option value="'+el.DISTRICT_ID+'" selected>'+el.DISTRICT_NAME+'</option>');
						}
						else
						{
							$('#s_district').append('<option value="'+el.DISTRICT_ID+'">'+el.DISTRICT_NAME+'</option>');
						}
						
						
					});

				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});

				$.ajax({
					url: '<?php echo base_url(); ?>admin/get_postcode',
					type: 'post',
					dataType: 'json',
					data: {param1: $('#s_amphur').val()},
				})
				.done(function(res) {
					$('#s_zip').val(res.postcode[0].POSTCODE);
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

		$('#s_province').change(function(event) {
			
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_amphur',
				type: 'post',
				dataType: 'json',
				data: {id_p: $(this).val()},
			})
			.done(function(res) {
				$('#s_amphur').empty();
				$('#s_amphur').append('<option value="">Choose ...</option>');
				console.log('test');
				$.each(res.amphur,function(index, el) {

					console.log(<?php echo set_value('s_amphur'); ?>);
					if("" == el.AMPHUR_ID)
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'" selected>'+el.AMPHUR_NAME+'</option>');
					}
					else
					{
						$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'">'+el.AMPHUR_NAME+'</option>');
					}	
					$('#s_amphur').append('<option value="'+el.AMPHUR_ID+'">'+el.AMPHUR_NAME+'</option>');
					
				});
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});

		$('#s_amphur').change(function(event) {
			
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_district',
				type: 'post',
				dataType: 'json',
				data: {id_a: $(this).val()},
			})
			.done(function(res) {
				$('#s_district').empty();
				$('#s_district').append('<option value="">Choose ...</option>');
				$.each(res.district,function(index, el) {
					$('#s_district').append('<option value="'+el.DISTRICT_ID+'">'+el.DISTRICT_NAME+'</option>');

				});

			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});


			
			$.ajax({
				url: '<?php echo base_url(); ?>admin/get_postcode',
				type: 'post',
				dataType: 'json',
				data: {param1: $(this).val()},
			})
			.done(function(res) {
				$('#s_zip').val(res.postcode[0].POSTCODE);
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});
    });
    
    
</script>