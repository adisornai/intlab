<div class="container profile-bg p-0">
    <form id="update_form" method="post" action="">
        <div class="row zmp">
            <div class="col-sm-3 col-md-3 m-0 p-5 pt-5 btn-r bg-def">
                <?php 
                if($profile->t_img != NULL){
                ?>
                    <img class="profile-img" src="<?=base_url($profile->t_img);?>" alt="">
                <?php
                }
                else{
                ?>
                    <img class="profile-img" src="<?=base_url('assets/images/user-null.png');?>" alt="">
                <?php
                }
                 ?>
                
                <ul class="profile-menu">
                    <li><a class="text-light" href="<?=base_url('profile/uploadimg');?>"><i class="fas fa-file-image"></i><span>Upload image Profile</span></a></li>
                    <li><a class="text-light" href="<?=base_url('profile/changepassword')?>"><i class="fas fa-key"></i><span>Change Password</span></a></li>
                    <li><a class="text-light" href="<?=base_url('research')?>"><i class="fas fa-clipboard"></i><span>Research</span></a></li>
                    <li><a class="text-light" href="<?=base_url('publication')?>"><i class="fas fa-copy"></i><span>Publication</span></a></li>
                </ul>
                
            </div>
            <div class="col-sm-9 col-md-6 col-md-offset-1 p-5 rpt-20">
                <div class="form-group">
                    <span>Academic</span>
                    <select class="form-control" id="t_academic" name="t_academic">
                        <?php 
                        foreach ($academic as $row) {
                            if((set_value('t_academic')?set_value('t_academic'):$profile->t_academic) == $row->ap_id){
                                echo "<option value=".$row->ap_id." data-academic_eng='".$row->ap_nameEN."' selected>".$row->ap_nameTH."</option>";
                            }
                            else{
                                echo "<option value=".$row->ap_id." data-academic_eng='".$row->ap_nameEN."'>".$row->ap_nameTH."</option>";
                            }
                        }
                         ?>  
                    </select>
                    <span class="text-danger"><?=form_error('t_academic')?></span>
                </div>
                <div class="col-md-6 zmp pr-5">
                    <div class="form-group">
                        <span>First Name Thai</span>
                        <input type="text" class="form-control" id="t_fnameTH" name="t_fnameTH" value="<?php if(set_value('t_fnameTH')){echo set_value('t_fnameTH');}else{echo $profile->t_fnameTH;} ?>">
                        <span class="text-danger"><?=form_error('t_fnameTH')?></span>
                    </div>
                </div>
                <div class="col-md-6 zmp pl-5">
                    <div class="form-group">
                        <span>Last Name Thai</span>
                        <input type="text" class="form-control" id="t_lnameTH" name="t_lnameTH" value="<?php if(set_value('t_lnameTH')){echo set_value('t_lnameTH');}else{echo $profile->t_lnameTH;} ?>">
                        <span class="text-danger"><?=form_error('t_lnameTH')?></span>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <span>Academic</span>
                    <select class="form-control" id="t_academicEN" name="t_academicEN" disabled></select>
                </div>
                <div class="col-md-6 zmp pr-5">
                    <div class="form-group">
                        <span>First Name English</span>
                        <input type="text" class="form-control" id="t_fnameEN" name="t_fnameEN" value="<?php if(set_value('t_fnameEN')){echo set_value('t_fnameEN');}else{echo $profile->t_fnameEN;} ?>">
                        <span class="text-danger"><?=form_error('t_fnameEN')?></span>
                    </div>
                </div>
                <div class="col-md-6 zmp pl-5">
                    <div class="form-group">
                        <span>Last Name English</span>
                        <input type="text" class="form-control" id="t_lnameEN" name="t_lnameEN" value="<?php if(set_value('t_lnameEN')){echo set_value('t_lnameEN');}else{echo $profile->t_lnameEN;} ?>">
                        <span class="text-danger"><?=form_error('t_lnameEN')?></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <span>Email</span>
                    <input type="text" class="form-control" id="t_email" name="t_email" value="<?php if(set_value('t_email')){echo set_value('t_email');}else{echo $profile->t_email;} ?>">
                    <span class="text-danger"><?=form_error('t_email')?></span>
                </div>
                <div class="form-group">
                    <span>Phone</span>
                    <input type="text" class="form-control" id="t_phone" name="t_phone" value="<?php if(set_value('t_phone')){echo set_value('t_phone');}else{echo $profile->t_phone;} ?>">
                    <span class="text-danger"><?=form_error('t_phone')?></span>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-6 col-md-offset-4 p-5">
                <div class="form-group">
                    <span>University</span>
                    <select class="form-control" id="t_university" name="t_university">
                        <?php 
                        foreach ($university as $row) {
                            if((set_value('t_university')?set_value('t_university'):$profile->t_university) == $row->u_id){
                                echo "<option value=".$row->u_id." selected>".$row->u_nameEN."</option>";
                            }else{
                                echo "<option value=".$row->u_id.">".$row->u_nameEN."</option>";
                            }
                        }
                         ?>
                    </select>
                    <span class="text-danger"><?=form_error('t_university')?></span>
                </div>
                <div class="form-group">
                    <span>Faculty</span>
                    <select class="form-control" id="t_faculty" name="t_faculty"></select>
                    <span class="text-danger"><?=form_error('t_faculty')?></span>
                </div>
                <div class="form-group">
                    <span>Major</span>
                    <select class="form-control" id="t_major" name="t_major"></select>
                    <span class="text-danger"><?=form_error('t_major')?></span>
                </div>
                <div class="form-group">
                    <span>Special Research</span>
                    <textarea rows="5" class="form-control" id="t_special_research" name="t_special_research"><?php 
                    echo (set_value('t_special_research')?set_value('t_special_research'):$profile->t_special_research); ?></textarea>
                    <span class="text-danger"><?=form_error('t_special_research')?></span>
                </div>
                <div class="form-group">
                    <span>Detail</span>
                    <textarea rows="5" class="form-control" id="t_detail" name="t_detail"><?php echo (set_value('t_detail')?set_value('t_detail'):$profile->t_detail); ?></textarea>
                    <span class="text-danger"><?=form_error('t_detail')?></span>
                </div>
            </div>

        </div>
    
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9 col-md-6 col-md-offset-4">
                <div class="form-group">
                    <input type="submit" class="form-control btn-block profile-btn btn-r" id="" name="update_profile" value="Save">
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    
    $(document).ready(function() {
        function load_academicEN(){
            $('#t_academicEN').empty();
            let academicEN_ID = $('#t_academic option:selected').val();
            let academicEN = $('#t_academic option:selected').data('academic_eng');
            if(academicEN_ID){
                $('#t_academicEN').append('<option value='+academicEN_ID+'>'+academicEN+'</option>');
            }

        }
        load_academicEN();
        $('#t_academic').change(function(event) {
            load_academicEN();
        });

        function load_faculty(){
            $('#t_faculty').empty();
            let path = "<?=base_url('pages/load_faculty')?>";
            let id = $('#t_university').val();
            $.post(path, {id: id}, function(res) {
                let result = JSON.parse(res);
                for (var i = 0; i < result.length; i++) {
                    let fid = result[i].f_id;
                    let fname = result[i].f_nameEN;
                    if(fid == <?=set_value('t_faculty')?set_value('t_faculty'):$profile->t_faculty?>)
                        $('#t_faculty').append('<option value='+fid+' selected>'+fname+'</option>');
                    else
                        $('#t_faculty').append('<option value='+fid+'>'+fname+'</option>');
                }
                load_major();
            });
        }
        load_faculty();
        $('#t_university').change(function(event) {
            load_faculty();
        });

        function load_major(){
            $('#t_major').empty();
            let path = "<?=base_url('pages/load_major')?>";
            let fid = $('#t_faculty').val();
            let uid = $('#t_university').val();
            $.post(path, {fid: fid,uid: uid}, function(res) {
                let result = JSON.parse(res);
                for (var i = 0; i < result.length; i++) {
                    let mid = result[i].m_id;
                    let mname = result[i].m_nameEN;
                    if(mid == <?=set_value('t_major')?set_value('t_major'):$profile->t_major?>)
                        $('#t_major').append('<option value='+mid+' selected>'+mname+'</option>');
                    else
                        $('#t_major').append('<option value='+mid+'>'+mname+'</option>');
                }
            });
            //console.log(fid+' '+uid);
        }        
    });
    
    
</script>