
	<!--Footer-->
        
            
</div>

        <footer id="footer" class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-wrapper">
                        <div class="">
                            <div class="copycenter">
                                <p>© Intellect Laboratory 2018 Faculty of Informatics, Mahasarakham University, Thailand</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
		
		
		<div class="scrollup">
			<a href=""><i class="fa fa-chevron-up"></i></a>
		</div>

	
	
    <script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>