
	<!--Footer-->
        
            
</div>

        <footer id="footer" class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-wrapper">
                        <div class="">
                            <div class="copycenter">
                                <p>© Intellect Laboratory 2016 Faculty of Informatics, Mahasarakham University, Thailand</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
		
		
		<div class="scrollup">
			<a href=""><i class="fa fa-chevron-up"></i></a>
		</div>

    <audio id="endCallSignal" preload="auto">
        <source src="<?php echo base_url(); ?>assets/audio/end_of_call.ogg" type="audio/ogg" />
        <source src="<?php echo base_url(); ?>assets/audio/end_of_call.mp3" type="audio/mp3" />
    </audio>

    <audio id="callingSignal" loop preload="auto">
        <source src="<?php echo base_url(); ?>assets/audio/calling.ogg" type="audio/ogg" />
        <source src="<?php echo base_url(); ?>assets/audio/calling.mp3" type="audio/mp3" />
    </audio>

    <audio id="ringtoneSignal" loop preload="auto">
        <source src="<?php echo base_url(); ?>assets/audio/ringtone.ogg" type="audio/ogg" />
        <source src="<?php echo base_url(); ?>assets/audio/ringtone.mp3" type="audio/mp3" />
    </audio>
	
	
    <!--<script src="<?php //echo base_url(); ?>assets/js/vendor/jquery-1.11.2.min.js"></script>-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.2.3/backbone-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>

    <script src="https://unpkg.com/media-recorder-js@2.0.0/qbMediaRecorder.js"></script>

    <!-- QB -->
    <script src="<?php echo base_url(); ?>assets/js/QB/quickblox.min.js"></script>

    <!-- app -->
    <script src="<?php echo base_url(); ?>assets/js/QB/config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/QB/helpers.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/QB/stateBoard.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/QB/app.js"></script>
</body>
</html>