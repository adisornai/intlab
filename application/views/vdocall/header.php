<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Intellect Laboratory</title>
	<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<!--For Plugins external css-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/roboto-webfont.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!--Theme custom css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-a.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/videocall/style-vdoc.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/videocall/vdocall.css">

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        

    <!-- font thai -->
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">



</head>
<body>
<div class="wrapper">
    <div class='preloader'><div class='loaded'>&nbsp;</div></div>
            <!-- Sections -->
            

            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                    </div>
                    
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-left">
                            <li class=""><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>about">About</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Member</a>
                                <ul class="dropdown-menu">
                                  <li class="bLecturer"><a href="<?php echo base_url(); ?>#Lecturer">Lecturer</a></li>
                                  <li class="bStudents"><a href="<?php echo base_url(); ?>#Students">Students</a></li>
                              </ul>
                            </li>
                            <?php if($this->session->has_userdata('U_id')){ ?>
                                <li class=""><a href="<?php echo base_url(); ?>profile" class="color-w">Information Management</a></li>
                                <li class=""><a id="<?php echo $ac3; ?>" href="<?php echo base_url(); ?>videocall">VIDEO CALL</a></li>
                                <?php 
                                    if($this->session->U_status==1)
                                    {
                                        echo '<li class=""><a href="admin">ADMIN</a></li>';
                                    }
                                 ?>

                                
                                <?php 
                                    }
                                    
                                ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php 
                                if($this->session->has_userdata('U_id'))
                                {
                             ?>
                                    <li><a><i class="fas fa-user-circle i-size-user"></i> <span class="login-font user_name"><?php echo $this->session->U_name; ?></span></a></li>
                                    <li class="login"><a href="<?php echo base_url(); ?>pages/logout" class="text-center"><span class="login-font-sign">Sign Out</span><i class="fas fa-sign-out-alt i-size"></i></a></li>
                             <?php 
                                }
                                else
                                {
                              ?>
                                    <li class="login"><a href="<?php echo base_url(); ?>login"><span class="login-font-sign">Sign In</span><i class="fas fa-sign-in-alt i-size"></i></a></li>
                            <?php 
                                }
                             ?>
                        </ul>
                        
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>