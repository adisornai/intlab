<?php
	/**
	 * 
	 */
	class Profile extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model('User_model');
		}

		public function update(){
			if($this->session->has_userdata('U_id')){
				$status = $this->session->U_status;
				if($status == 0)
				{
					$profile = array(
						's_prename' => $this->input->post('s_prename'),
						's_fnameEN' => $this->input->post('s_fnameEN'),
						's_lnameEN' => $this->input->post('s_lnameEN'),
						's_phone' => $this->input->post('s_phone'),
						's_email' => $this->input->post('s_email'),
						's_university' => $this->input->post('s_university'),
						's_faculty' => $this->input->post('s_faculty'),
						's_major' => $this->input->post('s_major'),
						's_research' => $this->input->post('s_research'),
						's_education' => $this->input->post('s_education'),
						's_begin' => $this->input->post('s_begin'),
						's_end' => $this->input->post('s_end'),
						's_adviser' => $this->input->post('s_adviser'),
						's_coadviser' => $this->input->post('s_coadviser'),
						's_house_no' => $this->input->post('s_house_no'),
						's_village_no' => $this->input->post('s_village_no'),
						's_lane' => $this->input->post('s_lane'),
						's_road' => $this->input->post('s_road'),
						's_province' => $this->input->post('s_province'),
						's_amphur' => $this->input->post('s_amphur'),
						's_district' => $this->input->post('s_district'),
						's_zip' => $this->input->post('s_zip')
					);
					$this->User_model->update_data_student($this->session->U_id,$profile);	
				}
				else
				{
					$profile = array(
						't_academic' => $this->input->post('t_academic'),
						't_fnameTH' => $this->input->post('t_fnameTH'),
						't_lnameTH' => $this->input->post('t_lnameTH'),
						't_fnameEN' => $this->input->post('t_fnameEN'),
						't_lnameEN' => $this->input->post('t_lnameEN'),
						't_phone' => $this->input->post('t_phone'),
						't_email' => $this->input->post('t_email'),
						't_university' => $this->input->post('t_university'),
						't_faculty' => $this->input->post('t_faculty'),
						't_major' => $this->input->post('t_major'),
						't_special_research' => $this->input->post('t_special_research'),
						't_detail' => $this->input->post('t_detail'),
					);
					$this->User_model->update_data_lecturer($this->session->U_id,$profile);	
				}
				redirect('profile');
			}else{
				redirect('login');
			}	
		}

		public function uploadimg(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('pages/header');
				$this->load->view('pages/uploadimg');
				$this->load->view('pages/footer');
			}
			else{
				redirect('login');
			}
		}

		public function changepassword(){
			if($this->session->has_userdata('U_id')){
				if($this->input->post('change_form')){
					$this->form_validation->set_rules('pass','Password','required|min_length[8]');
					$this->form_validation->set_rules('cpass','Password Confirmation','required|min_length[8]|matches[pass]');

					if($this->form_validation->run()){
						$hash = hash('sha3-256' ,'IntL@b'.$this->input->post('cpass'));
						$s = $this->session->U_status;
						if($s == 0){
							$data = array(
								's_pass' => $hash
							);
						}
						else{
							$data = array(
								't_pass' => $hash
							);
						}
						
						$id = $this->session->U_id;
						$this->User_model->change_password($id,$data,$s);
						redirect('profile');
					}
				}
				$this->load->view('pages/header');
				$this->load->view('pages/changepassword');
				$this->load->view('pages/footer');
			}
			else{
				redirect('login');
			}
		}

		public function ajax_upload(){
			if(isset($_FILES["image_file"]["name"])){
				$s = $this->session->U_status;
				$id = $this->session->U_id;

				if($s == 0){
					$profile = $this->User_model->select_row_student($id);
					if($profile->s_education == 1){
						$config['upload_path'] = './assets/images/Students/Bachelor/';
					}
					elseif($profile->s_education == 2){
						$config['upload_path'] = './assets/images/Students/Master/';
					}
					
				}
				else{
					$config['upload_path'] = './assets/images/Member/';
				}
				$config['allowed_types'] = 'jpg';
				$new_name = $this->session->U_user;
				$config['file_name'] = $new_name;
				
				
				$this->load->library('upload' ,$config);
				if($s == 0){
					if($profile->s_education == 1){
						@unlink(FCPATH.'assets\images\Students\Bachelor\\'.$new_name.'.jpg');
					}
					elseif($profile->s_education == 2){
						@unlink(FCPATH.'assets\images\Students\Master\\'.$new_name.'.jpg');
					}
				}
				else{
					@unlink(FCPATH.'assets\images\Member\\'.$new_name.'.jpg');
				}
				
				if(!$this->upload->do_upload('image_file')){
					echo $this->upload->display_errors();
				}
				else{
					$data = $this->upload->data();
					if($s == 0){
						if($profile->s_education == 1){
							$img = array('s_img' => 'assets/images/Students/Bachelor/'.$data["file_name"]);
						}
						elseif($profile->s_education == 2){
							$img = array('s_img' => 'assets/images/Students/Master/'.$data["file_name"]);
						}	
					}
					else{
						$img = array('t_img' => 'assets/images/Member/'.$data["file_name"]);
					}
					$this->User_model->upload_img($id,$img,$s);
					echo "Success";

				}
			}
		}
	}