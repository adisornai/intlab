<?php
	class Admin extends CI_Controller{
		public function __construct(){
			parent::__construct();
			if($this->session->U_admin != 1){
				redirect('');
			}
			$this->load->model('Admin_model');
			$this->load->model('User_model');
		}

		public function index(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->Admin_model->load_all_data_student();
				$data['prename'] = $this->User_model->load_prename();
				$data['academic_ranks'] = $this->User_model->load_academic_ranks();
				$data['education'] = $this->User_model->load_education();
				$data['province'] = $this->User_model->load_province();
				$data['major'] = $this->User_model->load_major();
				$data['faculty'] = $this->User_model->load_faculty();
				$data['result'] = $this->Admin_model->load_all_data_student();
				$data['load_lecturer'] = $this->User_model->load_lecturer();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/index');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}
		/* Student */
		public function student(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->Admin_model->load_all_data_student();
				$data['prename'] = $this->User_model->load_prename();
				$data['academic_ranks'] = $this->User_model->load_academic_ranks();
				$data['education'] = $this->User_model->load_education();
				$data['province'] = $this->User_model->load_province();
				$data['major'] = $this->User_model->load_major();
				$data['faculty'] = $this->User_model->load_faculty();
				$data['result'] = $this->Admin_model->load_all_data_student();
				$data['load_lecturer'] = $this->User_model->load_lecturer();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/student');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_student(){	
			$this->form_validation->set_rules('s_sid','Student ID','required|min_length[11]|numeric|is_unique[student.s_sid]');
			$this->form_validation->set_rules('s_user','Username','required|is_unique[student.s_user]');
			$this->form_validation->set_rules('s_pass','Password','required|min_length[8]');
			$this->form_validation->set_rules('s_prename','Prename','required');
			$this->form_validation->set_rules('s_fnameTH','First Name TH','required|is_unique[student.s_fnameTH]');
			$this->form_validation->set_rules('s_lnameTH','Last Name TH','required|is_unique[student.s_lnameTH]');
			$this->form_validation->set_rules('s_fnameEN','First Name EN','required|is_unique[student.s_fnameEN]');
			$this->form_validation->set_rules('s_lnameEN','Last Name EN','required|is_unique[student.s_lnameEN]');
			$this->form_validation->set_rules('s_email','Email','required|valid_email|is_unique[student.s_email]');
			$this->form_validation->set_rules('s_phone','Phone','required|numeric|min_length[10]|is_unique[student.s_phone]');
			$this->form_validation->set_rules('s_vdo','Status VDO','required');
			$this->form_validation->set_rules('s_disable','Status Login','required');
			$this->form_validation->set_rules('s_status','Status','required');
			$this->form_validation->set_rules('s_begin','Begin','required');
			//$this->form_validation->set_rules('s_end','End','required');
			$this->form_validation->set_rules('s_education','Education','required');
			$this->form_validation->set_rules('s_university','University','required');
			$this->form_validation->set_rules('s_faculty','Faculty','required');
			$this->form_validation->set_rules('s_major','Major','required');
			$this->form_validation->set_rules('s_adviser','Adviser','required');
			//$this->form_validation->set_rules('s_coadviser','Co-Adviser','required');
			$this->form_validation->set_rules('s_house_no','House no.','required');
			$this->form_validation->set_rules('s_village_no','Village no.','required');
			$this->form_validation->set_rules('s_lane','Lane','required');
			$this->form_validation->set_rules('s_road','Road','required');
			$this->form_validation->set_rules('s_province','Province','required');
			$this->form_validation->set_rules('s_amphur','Amphur','required');
			$this->form_validation->set_rules('s_district','District','required');
			$this->form_validation->set_rules('s_zip','Post Code','required');


			if($this->form_validation->run())
			{
				$hash = hash('sha3-256' ,'IntL@b'.$this->input->post('s_pass'));
				$data = array(
					's_sid' => $this->input->post('s_sid'),
					's_user' => $this->input->post('s_user'),
					's_pass' => $hash,
					's_prename' => $this->input->post('s_prename'),
					's_fnameTH' => $this->input->post('s_fnameTH'),
					's_lnameTH' => $this->input->post('s_lnameTH'),
					's_fnameEN' => $this->input->post('s_fnameEN'),
					's_lnameEN' => $this->input->post('s_lnameEN'),
					's_email' => $this->input->post('s_email'),
					's_phone' => $this->input->post('s_phone'),
					's_vdo' => $this->input->post('s_vdo'),
					's_disable' => $this->input->post('s_disable'),
					's_status' => $this->input->post('s_status'),
					's_begin' => $this->input->post('s_begin'),
					's_end' => $this->input->post('s_end'),
					's_education' => $this->input->post('s_education'),
					's_university' => $this->input->post('s_university'),
					's_faculty' => $this->input->post('s_faculty'),
					's_major' => $this->input->post('s_major'),
					's_adviser' => $this->input->post('s_adviser'),
					's_coadviser' => $this->input->post('s_coadviser'),
					's_house_no' => $this->input->post('s_house_no'),
					's_village_no' => $this->input->post('s_village_no'),
					's_lane' => $this->input->post('s_lane'),
					's_road' => $this->input->post('s_road'),
					's_province' => $this->input->post('s_province'),
					's_amphur' => $this->input->post('s_amphur'),
					's_district' => $this->input->post('s_district'),
					's_zip' => $this->input->post('s_zip'),
				);
				$this->Admin_model->insert_student($data);
				redirect('admin/student');
			}
			else
			{
				$this->addstudent();
				//redirect('admin/addstudent');
			}
		}

		public function edit_student($id){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->Admin_model->load_all_data_student();
				$data['prename'] = $this->User_model->load_prename();
				$data['academic_ranks'] = $this->User_model->load_academic_ranks();
				$data['education'] = $this->User_model->load_education();
				$data['province'] = $this->User_model->load_province();
				$data['university'] = $this->User_model->load_university();
				$data['major'] = $this->User_model->load_major();
				$data['faculty'] = $this->User_model->load_faculty();
				$data['result'] = $this->Admin_model->load_all_data_student();
				$data['load_lecturer'] = $this->User_model->load_lecturer();
				$data['mydata'] = $this->User_model->select_row_student($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editstudent');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function update_student($id){
			$this->form_validation->set_rules('s_sid','Student ID','required|min_length[11]|numeric');
			$this->form_validation->set_rules('s_user','Username','required');
			$this->form_validation->set_rules('s_pass','Password','required|min_length[8]');
			$this->form_validation->set_rules('s_prename','Prename','required');
			$this->form_validation->set_rules('s_fnameTH','First Name TH','required');
			$this->form_validation->set_rules('s_lnameTH','Last Name TH','required');
			$this->form_validation->set_rules('s_fnameEN','First Name EN','required');
			$this->form_validation->set_rules('s_lnameEN','Last Name EN','required');
			$this->form_validation->set_rules('s_email','Email','required|valid_email');
			$this->form_validation->set_rules('s_phone','Phone','required|numeric|min_length[10]');
			$this->form_validation->set_rules('s_vdo','Status VDO','required');
			$this->form_validation->set_rules('s_disable','Status Login','required');
			$this->form_validation->set_rules('s_status','Status','required');
			$this->form_validation->set_rules('s_begin','Begin','required');
			//$this->form_validation->set_rules('s_end','End','required');
			$this->form_validation->set_rules('s_education','Education','required');
			$this->form_validation->set_rules('s_university','University','required');
			$this->form_validation->set_rules('s_faculty','Faculty','required');
			$this->form_validation->set_rules('s_major','Major','required');
			$this->form_validation->set_rules('s_adviser','Adviser','required');
			//$this->form_validation->set_rules('s_coadviser','Co-Adviser','required');
			$this->form_validation->set_rules('s_house_no','House no.','required');
			$this->form_validation->set_rules('s_village_no','Village no.','required');
			$this->form_validation->set_rules('s_lane','Lane','required');
			$this->form_validation->set_rules('s_road','Road','required');
			$this->form_validation->set_rules('s_province','Province','required');
			$this->form_validation->set_rules('s_amphur','Amphur','required');
			$this->form_validation->set_rules('s_district','District','required');
			$this->form_validation->set_rules('s_zip','Post Code','required');


			if($this->form_validation->run())
			{
				$hash = hash('sha3-256' ,'IntL@b'.$this->input->post('s_pass'));
				$data = array(
					's_sid' => $this->input->post('s_sid'),
					's_user' => $this->input->post('s_user'),
					's_pass' => $hash,
					's_prename' => $this->input->post('s_prename'),
					's_fnameTH' => $this->input->post('s_fnameTH'),
					's_lnameTH' => $this->input->post('s_lnameTH'),
					's_fnameEN' => $this->input->post('s_fnameEN'),
					's_lnameEN' => $this->input->post('s_lnameEN'),
					's_email' => $this->input->post('s_email'),
					's_phone' => $this->input->post('s_phone'),
					's_vdo' => $this->input->post('s_vdo'),
					's_disable' => $this->input->post('s_disable'),
					's_status' => $this->input->post('s_status'),
					's_begin' => $this->input->post('s_begin'),
					's_end' => $this->input->post('s_end'),
					's_education' => $this->input->post('s_education'),
					's_university' => $this->input->post('s_university'),
					's_faculty' => $this->input->post('s_faculty'),
					's_major' => $this->input->post('s_major'),
					's_adviser' => $this->input->post('s_adviser'),
					's_coadviser' => $this->input->post('s_coadviser'),
					's_house_no' => $this->input->post('s_house_no'),
					's_village_no' => $this->input->post('s_village_no'),
					's_lane' => $this->input->post('s_lane'),
					's_road' => $this->input->post('s_road'),
					's_province' => $this->input->post('s_province'),
					's_amphur' => $this->input->post('s_amphur'),
					's_district' => $this->input->post('s_district'),
					's_zip' => $this->input->post('s_zip'),
				);
				$this->Admin_model->update_student($id,$data);
				redirect('admin/student');
			}
			else
			{
				$this->edit_student($id);
				//redirect('admin/addstudent');
			}
		}

		public function delete_student(){
			$id = $this->input->post('id');
			$this->Admin_model->delete_student($id);
			echo $id;
		} 

		/*public function showstudent(){
			$sid = $this->input->post('s_id');
			echo json_encode($this->User_model->select_student($sid));
		}*/

		public function addstudent(){
			if($this->session->has_userdata('U_id')){
				$data['prename'] = $this->User_model->load_prename();
				$data['education'] = $this->User_model->load_education();
				$data['province'] = $this->User_model->load_province();
				$data['major'] = $this->User_model->load_major();
				$data['university'] = $this->User_model->load_university();
				$data['faculty'] = $this->User_model->load_faculty();
				$data['load_lecturer'] = $this->User_model->load_lecturer();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/addstudent');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}
		/* End Student */

		public function lecturer(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_lecturer();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/lecturer');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function addlecturer(){
			if($this->session->has_userdata('U_id')){
				$data['academic'] = $this->User_model->load_academic_ranks();
				$data['province'] = $this->User_model->load_province();
				$data['major'] = $this->User_model->load_major();
				$data['university'] = $this->User_model->load_university();
				$data['faculty'] = $this->User_model->load_faculty();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/addlecturer');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function edit_lecturer($id){
			if($this->session->has_userdata('U_id')){
				$data['academic'] = $this->User_model->load_academic_ranks();
				$data['province'] = $this->User_model->load_province();
				$data['major'] = $this->User_model->load_major();
				$data['university'] = $this->User_model->load_university();
				$data['faculty'] = $this->User_model->load_faculty();
				$data['mydata'] = $this->User_model->select_row_lecturer($id);
 				$this->load->view('admin/header',$data);
				$this->load->view('admin/editlecturer');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_lecturer(){
			$this->form_validation->set_rules('t_lid','Lecturer ID','required|min_length[3]|numeric|is_unique[lecturer.t_lid]');
			$this->form_validation->set_rules('t_user','Username','required|is_unique[lecturer.t_user]');
			$this->form_validation->set_rules('t_pass','Password','required|min_length[8]');
			$this->form_validation->set_rules('t_academic','Academic','required|is_natural_no_zero',
				array('is_natural_no_zero' => 'Please Choose Academic')

			);
			$this->form_validation->set_rules('t_fnameTH','First Name Thai','required|is_unique[lecturer.t_fnameTH]');
			$this->form_validation->set_rules('t_lnameTH','Last Name Thai','required|is_unique[lecturer.t_lnameTH]');
			$this->form_validation->set_rules('t_fnameEN','First Name English','required|is_unique[lecturer.t_fnameEN]');
			$this->form_validation->set_rules('t_lnameEN','Last Name English','required|is_unique[lecturer.t_lnameEN]');
			$this->form_validation->set_rules('t_email','Email','required|valid_email|is_unique[lecturer.t_email]');
			$this->form_validation->set_rules('t_phone','Phone','required|numeric|min_length[10]');
			$this->form_validation->set_rules('t_status_admin','Status Admin','required');
			$this->form_validation->set_rules('t_status_it','Status IntLab','required');
			$this->form_validation->set_rules('t_disable','Status Login','required');
			$this->form_validation->set_rules('t_university','University','required|is_natural_no_zero',
				array('is_natural_no_zero' => 'Please Choose University')
			);
			$this->form_validation->set_rules('t_faculty','Faculty','required');
			$this->form_validation->set_rules('t_major','Major','required');
			$this->form_validation->set_rules('t_special_research','Special Research','required');
			$this->form_validation->set_rules('t_detail','Detail','required');
			$this->form_validation->set_rules('t_vdo','Status VDO','required');
			


			if($this->form_validation->run())
			{
				$hash = hash('sha3-256' ,'IntL@b'.$this->input->post('t_pass'));
				$data = array(
					't_lid' => $this->input->post('t_lid'),
					't_user' => $this->input->post('t_user'),
					't_pass' => $hash,
					't_academic' => $this->input->post('t_academic'),
					't_fnameTH' => $this->input->post('t_fnameTH'),
					't_lnameTH' => $this->input->post('t_lnameTH'),
					't_fnameEN' => $this->input->post('t_fnameEN'),
					't_lnameEN' => $this->input->post('t_lnameEN'),
					't_email' => $this->input->post('t_email'),
					't_phone' => $this->input->post('t_phone'),
					't_status_admin' => $this->input->post('t_status_admin'),
					't_status_it' => $this->input->post('t_status_it'),
					't_disable' => $this->input->post('t_disable'),
					't_university' => $this->input->post('t_university'),
					't_faculty' => $this->input->post('t_faculty'),
					't_major' => $this->input->post('t_major'),
					't_special_research' => $this->input->post('t_special_research'),
					't_detail' => $this->input->post('t_detail'),
					't_vdo' => $this->input->post('t_vdo')
				);
				$this->Admin_model->insert_lecturer($data);
				redirect('admin/lecturer');
			}
			else
			{
				$this->addlecturer();
				//redirect('admin/addstudent');
			}
		}

		public function update_lecturer($id){
			$this->form_validation->set_rules('t_lid','Lecturer ID','required|min_length[3]|numeric');
			$this->form_validation->set_rules('t_user','Username','required');
			$this->form_validation->set_rules('t_pass','Password','required|min_length[8]');
			$this->form_validation->set_rules('t_academic','Academic','required|is_natural_no_zero',
				array('is_natural_no_zero' => 'Please Choose Academic')

			);
			$this->form_validation->set_rules('t_fnameTH','First Name Thai','required');
			$this->form_validation->set_rules('t_lnameTH','Last Name Thai','required');
			$this->form_validation->set_rules('t_fnameEN','First Name English','required');
			$this->form_validation->set_rules('t_lnameEN','Last Name English','required');
			$this->form_validation->set_rules('t_email','Email','required|valid_email');
			$this->form_validation->set_rules('t_phone','Phone','required|numeric|min_length[10]');
			$this->form_validation->set_rules('t_status_admin','Status Admin','required');
			$this->form_validation->set_rules('t_status_it','Status IntLab','required');
			$this->form_validation->set_rules('t_disable','Status Login','required');
			$this->form_validation->set_rules('t_university','University','required|is_natural_no_zero',
				array('is_natural_no_zero' => 'Please Choose University')
			);
			$this->form_validation->set_rules('t_faculty','Faculty','required');
			$this->form_validation->set_rules('t_major','Major','required');
			$this->form_validation->set_rules('t_special_research','Special Research','required');
			$this->form_validation->set_rules('t_detail','Detail','required');
			$this->form_validation->set_rules('t_vdo','Status VDO','required');
			


			if($this->form_validation->run())
			{	
				$hash = hash('sha3-256' ,'IntL@b'.$this->input->post('t_pass'));
				$data = array(
					't_lid' => $this->input->post('t_lid'),
					't_user' => $this->input->post('t_user'),
					't_pass' => $hash,
					't_academic' => $this->input->post('t_academic'),
					't_fnameTH' => $this->input->post('t_fnameTH'),
					't_lnameTH' => $this->input->post('t_lnameTH'),
					't_fnameEN' => $this->input->post('t_fnameEN'),
					't_lnameEN' => $this->input->post('t_lnameEN'),
					't_email' => $this->input->post('t_email'),
					't_phone' => $this->input->post('t_phone'),
					't_status_admin' => $this->input->post('t_status_admin'),
					't_status_it' => $this->input->post('t_status_it'),
					't_disable' => $this->input->post('t_disable'),
					't_university' => $this->input->post('t_university'),
					't_faculty' => $this->input->post('t_faculty'),
					't_major' => $this->input->post('t_major'),
					't_special_research' => $this->input->post('t_special_research'),
					't_detail' => $this->input->post('t_detail'),
					't_vdo' => $this->input->post('t_vdo')
				);
				$this->Admin_model->update_lecturer($id,$data);
				redirect('admin/lecturer');
			}
			else
			{
				$this->edit_lecturer($id);
				//redirect('admin/addstudent');
			}
		}

		public function delete_lecturer(){
			$id = $this->input->post('id');
			$this->Admin_model->delete_lecturer($id);
			echo $id;
		} 

		
		
		public function research(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->Admin_model->load_all_data_research();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/research');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function addresearch(){
			if($this->session->has_userdata('U_id')){
				$data['load_lecturer'] = $this->User_model->load_lecturer();
				$data['load_student'] = $this->User_model->load_student();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/addresearch');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function edit_research($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_research($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editresearch');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_research(){
			$this->form_validation->set_rules('r_research','Research','required');
			$this->form_validation->set_rules('r_nameTH','Name Thai','required');
			$this->form_validation->set_rules('r_nameEN','Name Eng','required');
			$this->form_validation->set_rules('r_head','Head of Project','required');
			$this->form_validation->set_rules('r_source','Source of Investment Funds','required');
			$this->form_validation->set_rules('r_budget','Budget','required|numeric');
			$this->form_validation->set_rules('r_begin','Begin','required');
			$this->form_validation->set_rules('r_end','End','required');
			$this->form_validation->set_rules('r_status','Status','required');
			$this->form_validation->set_rules('r_file','File','required');

			if($this->form_validation->run())
			{
				$data = array(
					'r_research' => $this->input->post('r_research'),
					'r_nameTH' => $this->input->post('r_nameTH'),
					'r_nameEN' => $this->input->post('r_nameEN'),
					'r_head' => $this->input->post('r_head'),
					'r_source' => $this->input->post('r_source'),
					'r_budget' => $this->input->post('r_budget'),
					'r_begin' => $this->input->post('r_begin'),
					'r_end' => $this->input->post('r_end'),
					'r_status' => $this->input->post('r_status'),
					'r_file' => $this->input->post('r_file')
				);
				$this->Admin_model->insert_research($data);
				redirect('admin/research');
			}
			else
			{
				$this->addresearch();
			}

		}

		public function update_research($id){
				$this->form_validation->set_rules('r_research','Research','required');
				$this->form_validation->set_rules('r_nameTH','Name Thai','required');
				$this->form_validation->set_rules('r_nameEN','Name Eng','required');
				$this->form_validation->set_rules('r_head','Head of Project','required');
				$this->form_validation->set_rules('r_source','Source of Investment Funds','required');
				$this->form_validation->set_rules('r_budget','Budget','required|numeric');
				$this->form_validation->set_rules('r_begin','Begin','required');
				$this->form_validation->set_rules('r_end','End','required');
				$this->form_validation->set_rules('r_status','Status','required');
				$this->form_validation->set_rules('r_file','File','required');
				
				if($this->form_validation->run())
				{
					$data = array(
						'r_research' => $this->input->post('r_research'),
						'r_nameTH' => $this->input->post('r_nameTH'),
						'r_nameEN' => $this->input->post('r_nameEN'),
						'r_head' => $this->input->post('r_head'),
						'r_source' => $this->input->post('r_source'),
						'r_budget' => $this->input->post('r_budget'),
						'r_begin' => $this->input->post('r_begin'),
						'r_end' => $this->input->post('r_end'),
						'r_status' => $this->input->post('r_status'),
						'r_file' => $this->input->post('r_file')
					);

					$this->Admin_model->update_research($id,$data);
					redirect('admin/research');
				}
				else
				{
					$this->edit_research($id);
				}
		}

		public function delete_research(){
			$id = $this->input->post('id');
			$this->Admin_model->delete_research($id);
			echo $id;
		} 
		
		public function publish_lt(){
			if($this->session->has_userdata('U_id')){
				$data['publish_st'] = $this->User_model->load_ps();
				$data['publish_type'] = $this->User_model->load_publish_type();
				$data['publish'] = $this->User_model->load_publish();
				$data['result'] = $this->Admin_model->load_all_data_publish();
				$data['hide'] = '';
				$this->load->view('admin/header',$data);
				$this->load->view('admin/publish_lt',$data);
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}
		
		public function research_lt(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->Admin_model->load_all_data_research();
				$data['hide'] = '';
				$this->load->view('admin/header',$data);
				$this->load->view('admin/research_lt');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function publication(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->Admin_model->load_all_data_publish();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/publish');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function addpublication(){
			if($this->session->has_userdata('U_id')){
				$data['load_student'] = $this->User_model->load_student();
				$data['load_ps'] = $this->User_model->load_ps();
				$data['load_pt'] = $this->User_model->load_publish_type();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/addpublication');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function edit_publication($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_publication($id);
				$data['load_student'] = $this->User_model->load_student();
				$data['load_ps'] = $this->User_model->load_ps();
				$data['load_pt'] = $this->User_model->load_publish_type();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editpublication');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_publication(){
				//$this->form_validation->set_rules('p_user','User','required');
				$this->form_validation->set_rules('p_ps_id','Published Standards','required');
				$this->form_validation->set_rules('p_nameTH','Name Thai','required');
				$this->form_validation->set_rules('p_nameEN','Name Eng','required');
				$this->form_validation->set_rules('p_detail','Detail','required');
				$this->form_validation->set_rules('p_year','Year','required');
				$this->form_validation->set_rules('p_status_type','Publish Type','required');
				$this->form_validation->set_rules('p_first_author','First Author','required');
				$this->form_validation->set_rules('p_second_author','Second Author','required');
				$this->form_validation->set_rules('p_language','Language','required');
				//$this->form_validation->set_rules('p_links','File','required');

				if($this->form_validation->run())
				{
					$data = array(
						'p_user' => $this->input->post('p_user'),
						'p_ps_id' => $this->input->post('p_ps_id'),
						'p_nameTH' => $this->input->post('p_nameTH'),
						'p_nameEN' => $this->input->post('p_nameEN'),
						'p_detail' => $this->input->post('p_detail'),
						'p_year' => $this->input->post('p_year'),
						'p_status_type' => $this->input->post('p_status_type'),
						'p_first_author' => $this->input->post('p_first_author'),
						'p_second_author' => $this->input->post('p_second_author'),
						'p_language' => $this->input->post('p_language'),
						'p_links' => $this->input->post('p_links'),
					);

					$this->Admin_model->insert_publication($data);
					redirect('admin/publication');
				}
				else
				{
					$this->addpublication();
				}
		}

		public function update_publication($id){
				//$this->form_validation->set_rules('p_user','User','required');
				//$this->form_validation->set_rules('p_ps_id','Published Standards','required');
				$this->form_validation->set_rules('p_nameTH','Name Thai','required');
				$this->form_validation->set_rules('p_nameEN','Name Eng','required');
				$this->form_validation->set_rules('p_detail','Detail','required');
				$this->form_validation->set_rules('p_year','Year','required');
				//$this->form_validation->set_rules('p_status_type','Publish Type','required');
				$this->form_validation->set_rules('p_first_author','First Author','required');
				$this->form_validation->set_rules('p_second_author','Second Author','required');
				$this->form_validation->set_rules('p_language','Language','required');
				//$this->form_validation->set_rules('p_links','File','required');

				if($this->form_validation->run())
				{
					$data = array(
						'p_user' => $this->input->post('p_user'),
						'p_ps_id' => $this->input->post('p_ps_id'),
						'p_nameTH' => $this->input->post('p_nameTH'),
						'p_nameEN' => $this->input->post('p_nameEN'),
						'p_detail' => $this->input->post('p_detail'),
						'p_year' => $this->input->post('p_year'),
						'p_status_type' => $this->input->post('p_status_type'),
						'p_first_author' => $this->input->post('p_first_author'),
						'p_second_author' => $this->input->post('p_second_author'),
						'p_language' => $this->input->post('p_language'),
						'p_links' => $this->input->post('p_links'),
					);

					$this->Admin_model->update_publication($id,$data);
					redirect('admin/publication');
				}
				else
				{
					$this->addpublication();
				}
		}
		public function delete_publication(){
			$id = $this->input->post('id');
			$this->Admin_model->delete_publication($id);
			echo $id;
		}

		public function university(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_university();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/university');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function adduniversity(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/adduniversity');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function edit_university($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_university($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/edituniversity');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_university(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('u_nameTH','Name Thai','required');
				$this->form_validation->set_rules('u_nameEN','Name English','required');

				if ($this->form_validation->run()) {
					$data = array(
						'u_nameTH' => $this->input->post('u_nameTH'),
						'u_nameEN' => $this->input->post('u_nameEN')
					);

					$this->Admin_model->insert_university($data);
					redirect('admin/university');

				}
				else{
					$this->adduniversity();
				}

			}else{
				redirect('login');
			}		
		}

		

		public function update_university($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('u_nameTH','Name Thai','required');
				$this->form_validation->set_rules('u_nameEN','Name English','required');

				if ($this->form_validation->run()) {
					$data = array(
						'u_nameTH' => $this->input->post('u_nameTH'),
						'u_nameEN' => $this->input->post('u_nameEN')
					);

					$this->Admin_model->update_university($id,$data);
					redirect('admin/university');

				}
				else{
					$this->edit_university($id);
				}
			}else{
				redirect('login');
			}		
		}

		public function delete_university(){
			$id = $this->input->post('id');
			$this->Admin_model->delete_university($id);
			echo $id;
		}

		public function faculty(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_faculty();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/faculty');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}

		public function addfaculty(){
			if($this->session->has_userdata('U_id')){
				$data['data_university'] = $this->User_model->load_university();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/addfaculty');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}		
		}	

		public function edit_faculty($id){
			if($this->session->has_userdata('U_id')){
				$data['data_university'] = $this->User_model->load_university();
				$data['mydata'] = $this->User_model->select_row_faculty($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editfaculty');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function insert_faculty(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('u_id','University','required');
				$this->form_validation->set_rules('f_nameTH','Faculty Thai','required');
				$this->form_validation->set_rules('f_nameEN','Faculty English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'u_id' => $this->input->post('u_id'),
						'f_nameTH' => $this->input->post('f_nameTH'),
						'f_nameEN' => $this->input->post('f_nameEN')
					);

					$this->Admin_model->insert_faculty($data);
					redirect('admin/faculty');
				}
				else
				{
					$this->addfaculty();
				}

			}else{
				redirect('login');
			}	
		}

		public function update_faculty($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('u_id','University','required');
				$this->form_validation->set_rules('f_nameTH','Faculty Thai','required');
				$this->form_validation->set_rules('f_nameEN','Faculty English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'u_id' => $this->input->post('u_id'),
						'f_nameTH' => $this->input->post('f_nameTH'),
						'f_nameEN' => $this->input->post('f_nameEN')
					);

					$this->Admin_model->update_faculty($id,$data);
					redirect('admin/faculty');
				}
				else
				{
					$this->edit_faculty($id);
				}
			}else{
				redirect('login');
			}
		}

		public function delete_faculty(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_faculty($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function major(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_major();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/major');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function addmajor(){
			if($this->session->has_userdata('U_id')){
				$data['data_faculty'] = $this->User_model->load_faculty();
				$data['data_university'] = $this->User_model->load_university();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/addmajor');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function edit_major($id){
			if($this->session->has_userdata('U_id')){
				$data['data_faculty'] = $this->User_model->load_faculty();
				$data['data_university'] = $this->User_model->load_university();
				$data['mydata'] = $this->User_model->select_row_major($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editmajor');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_major(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('u_id','University','required');
				$this->form_validation->set_rules('f_id','University','required');
				$this->form_validation->set_rules('m_nameTH','Faculty Thai','required');
				$this->form_validation->set_rules('m_nameEN','Faculty English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'u_id' => $this->input->post('u_id'),
						'f_id' => $this->input->post('f_id'),
						'm_nameTH' => $this->input->post('m_nameTH'),
						'm_nameEN' => $this->input->post('m_nameEN')
					);

					$this->Admin_model->insert_major($data);
					redirect('admin/major');
				}
				else
				{
					$this->addmajor();
				}

			}else{
				redirect('login');
			}	
		}

		public function update_major($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('u_id','University','required');
				$this->form_validation->set_rules('f_id','University','required');
				$this->form_validation->set_rules('m_nameTH','Faculty Thai','required');
				$this->form_validation->set_rules('m_nameEN','Faculty English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'u_id' => $this->input->post('u_id'),
						'f_id' => $this->input->post('f_id'),
						'm_nameTH' => $this->input->post('m_nameTH'),
						'm_nameEN' => $this->input->post('m_nameEN')
					);

					$this->Admin_model->update_major($id,$data);
					redirect('admin/major');
				}
				else
				{
					$this->edit_major($id_p);
				}

			}else{
				redirect('login');
			}	
		}
		
		public function delete_major(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_major($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function prename(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_prename();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/prename');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function addprename(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/addprename');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function edit_prename($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_prename($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editprename');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_prename(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('n_thainame','Name Title Thai','required');
				$this->form_validation->set_rules('n_engname','Name Title English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'n_thainame' => $this->input->post('n_thainame'),
						'n_engname' => $this->input->post('n_engname')
					);

					$this->Admin_model->insert_prename($data);
					redirect('admin/prename');
				}
				else
				{
					$this->addprename();
				}

			}else{
				redirect('login');
			}	
		}

		public function update_prename($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('n_thainame','Name Title Thai','required');
				$this->form_validation->set_rules('n_engname','Name Title English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'n_thainame' => $this->input->post('n_thainame'),
						'n_engname' => $this->input->post('n_engname')
					);

					$this->Admin_model->update_prename($id,$data);
					redirect('admin/prename');
				}
				else
				{
					$this->edit_prename($id);
				}

			}else{
				redirect('login');
			}	
		}

		public function delete_prename(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_prename($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function academic_ranks(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_academic_ranks();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/academic_ranks');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function addacademic_ranks(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/addacademic_ranks');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function edit_academic_ranks($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_academic_ranks($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editacademic_ranks');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}	
		}

		public function insert_academic_ranks(){
				$this->form_validation->set_rules('ap_nameTH','Academic Ranks Thai','required');
				$this->form_validation->set_rules('ap_nameEN','Academic Ranks English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'ap_nameTH' => $this->input->post('ap_nameTH'),
						'ap_nameEN' => $this->input->post('ap_nameEN')
					);

					$this->Admin_model->insert_academic_ranks($data);
					redirect('admin/academic_ranks');
				}
				else
				{
					$this->addacademic_ranks();
				}
		}

		public function update_academic_ranks($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('ap_nameTH','Academic Ranks Thai','required');
				$this->form_validation->set_rules('ap_nameEN','Academic Ranks English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'ap_nameTH' => $this->input->post('ap_nameTH'),
						'ap_nameEN' => $this->input->post('ap_nameEN')
					);

					$this->Admin_model->update_academic_ranks($id,$data);
					redirect('admin/academic_ranks');
				}
				else
				{
					$this->edit_academic_ranks($id);
				}

			}else{
				redirect('login');
			}	
		}

		public function delete_academic_ranks(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_academic_ranks($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function published_standards(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_published_standards();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/published_standards');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function addpublished_standards(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/addpublished_standards');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function edit_published_standards($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_published_standards($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editpublished_standards');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function insert_published_standards(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('ps_name','Published Standards','required');

				if($this->form_validation->run())
				{
					$data = array(
						'ps_name' => $this->input->post('ps_name')
					);

					$this->Admin_model->insert_published_standards($data);
					redirect('admin/published_standards');
				}
				else
				{
					$this->addpublished_standards();
				}
			}else{
				redirect('login');
			}
		}

		public function update_published_standards($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('ps_name','Published Standards','required');

				if($this->form_validation->run())
				{
					$data = array(
						'ps_name' => $this->input->post('ps_name')
					);

					$this->Admin_model->update_published_standards($id,$data);
					redirect('admin/published_standards');
				}
				else
				{
					$this->edit_published_standards($id);
				}
			}else{
				redirect('login');
			}
		}

		public function delete_published_standards(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_published_standards($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function publish_type(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_publish_type();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/publish_type');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function addpublish_type(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/addpublish_type');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function edit_publish_type($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_publish_type($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editpublish_type');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function insert_publish_type(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('pt_name','Publish Type','required');

				if($this->form_validation->run())
				{
					$data = array(
						'pt_name' => $this->input->post('pt_name')
					);

					$this->Admin_model->insert_publish_type($data);
					redirect('admin/publish_type');
				}
				else
				{
					$this->addpublish_type();
				}
			}else{
				redirect('login');
			}	
		}	

		public function update_publish_type($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('pt_name','Publish Type','required');

				if($this->form_validation->run())
				{
					$data = array(
						'pt_name' => $this->input->post('pt_name')
					);

					$this->Admin_model->update_publish_type($id,$data);
					redirect('admin/publish_type');
				}
				else
				{
					$this->edit_publish_type($id);
				}
			}else{
				redirect('login');
			}
		}

		public function delete_publish_type(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_publish_type($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function education(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_education();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/education');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function addeducation(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/addeducation');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function edit_education($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_education($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editeducation');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function insert_education(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('e_nameTH','Education Thai','required');
				$this->form_validation->set_rules('e_nameENG','Education English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'e_nameTH' => $this->input->post('e_nameTH'),
						'e_nameENG' => $this->input->post('e_nameENG')
					);

					$this->Admin_model->insert_education($data);
					redirect('admin/education');
				}
				else
				{
					$this->addeducation();
				}
			}else{
				redirect('login');
			}
		}
		
		public function update_education($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('e_nameTH','Education Thai','required');
				$this->form_validation->set_rules('e_nameENG','Education English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'e_nameTH' => $this->input->post('e_nameTH'),
						'e_nameENG' => $this->input->post('e_nameENG')
					);

					$this->Admin_model->update_education($id,$data);
					redirect('admin/education');
				}
				else
				{
					$this->edit_education($id);
				}
			}else{
				redirect('login');
			}
		}

		public function delete_education(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_education($id);
				echo $id;
			}else{
				redirect('login');
			}
		}
		
		public function activity(){
			if($this->session->has_userdata('U_id')){
				$data['result'] = $this->User_model->load_activity();
				$this->load->view('admin/header',$data);
				$this->load->view('admin/activity');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function addactivity(){
			if($this->session->has_userdata('U_id')){
				$this->load->view('admin/header');
				$this->load->view('admin/addactivity');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}

		public function edit_activity($id){
			if($this->session->has_userdata('U_id')){
				$data['mydata'] = $this->User_model->select_row_activity($id);
				$this->load->view('admin/header',$data);
				$this->load->view('admin/editactivity');
				$this->load->view('admin/footer');
			}else{
				redirect('login');
			}
		}
		
		public function insert_activity(){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('a_nameTH','Activity Thai','required');
				$this->form_validation->set_rules('a_nameENG','Activity English','required');
				$this->form_validation->set_rules('a_detail','Detail','required');
				$this->form_validation->set_rules('a_date','Date English','required');
				//$this->form_validation->set_rules('a_img','Image English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'a_nameTH' => $this->input->post('a_nameTH'),
						'a_nameENG' => $this->input->post('a_nameENG'),
						'a_detail' => $this->input->post('a_detail'),
						'a_date' => $this->input->post('a_date'),
						'a_img' => $this->input->post('a_img')
					);

					$this->Admin_model->insert_activity($data);
					redirect('admin/activity');
				}
				else
				{
					$this->addactivity();
				}
			}else{
				redirect('login');
			}
		}

		public function update_activity($id){
			if($this->session->has_userdata('U_id')){
				$this->form_validation->set_rules('a_nameTH','Activity Thai','required');
				$this->form_validation->set_rules('a_nameENG','Activity English','required');
				$this->form_validation->set_rules('a_detail','Detail','required');
				$this->form_validation->set_rules('a_date','Date English','required');
				//$this->form_validation->set_rules('a_img','Image English','required');

				if($this->form_validation->run())
				{
					$data = array(
						'a_nameTH' => $this->input->post('a_nameTH'),
						'a_nameENG' => $this->input->post('a_nameENG'),
						'a_detail' => $this->input->post('a_detail'),
						'a_date' => $this->input->post('a_date'),
						'a_img' => $this->input->post('a_img')
					);

					$this->Admin_model->update_activity($id,$data);
					redirect('admin/activity');
				}
				else
				{
					$this->edit_activity($id);
				}
			}else{
				redirect('login');
			}
		}

		public function delete_activity(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				$this->Admin_model->delete_activity($id);
				echo $id;
			}else{
				redirect('login');
			}
		}

		public function loadFaculty(){
			$id = $this->input->post('id');
			$result = $this->User_model->select_rows_faculty($id);

			$answer['status'] = 1;
			$answer['mydata'] = $result;
			echo json_encode($answer);
		}

		public function loadMajor(){
			$u_id = $this->input->post('u_id');
			$f_id = $this->input->post('f_id');
			$result = $this->User_model->select_rows_major($u_id,$f_id);

			$answer['status'] = 1;
			$answer['mydata'] = $result;
			echo json_encode($answer);
		}

		public function loadhead(){
			$s = $this->input->post('s');
			if($s == 1){
				$result = $this->User_model->load_lecturer();
			}else{
				$result = $this->User_model->load_student();
			}	
			$answer = $result;
			echo json_encode($answer);
		}


		public function login(){
			if($this->session->has_userdata('U_id')){
				redirect('admin/student');
			}else{
				$data['hide'] = 'hide';
				$this->load->view('admin/header',$data);
				$this->load->view('admin/index');
				$this->load->view('admin/footer');
			}
			
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('admin/login');
		}

		public function get_amphur()
		{
			$id_p = $this->input->post('id_p');
			$data['amphur'] = $this->User_model->load_amphur($id_p);
			echo json_encode($data);
		}
		
		public function get_district()
		{
			$id_a = $this->input->post('id_a');
			$data['district'] = $this->User_model->load_district($id_a);
			echo json_encode($data);
		}
		
		public function get_postcode()
		{
			$id = $this->input->post('param1');
			$data['postcode'] = $this->User_model->load_postcode($id);
			echo json_encode($data);
		}

		public function check_sid()
		{
			$id = $this->input->post('id');
			if ($id != "") 
			{
				if(strlen($id) == 11)
				{
					if($this->Admin_model->check_count_st($id)<1)
					{
						$msg = "";
						echo json_encode($msg);
					}
					else
					{
						$msg = "that SID is taken. try another";
						echo json_encode($msg);
					}
					
				}
				else
				{
					$msg = "Use 11 Characters";
					echo json_encode($msg);
				}
			}
			else
			{
				$msg = "Enter SID";
				echo json_encode($msg);
			}	
		}

		/* From

		if($this->session->has_userdata('U_id')){
				
		}else{
			redirect('login');
		}	
		
		*/

	}