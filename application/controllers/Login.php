<?php
class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
	}

	public function checklogin(){
		$this->form_validation->set_rules('username' , 'Username' , 'required');
		$this->form_validation->set_rules('password' , 'Password' , 'required');
		
		if($this->form_validation->run())
		{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			$pass = $this->db->escape_str($pass);
			
			$vdopass = $pass;
			$pass = hash('sha3-256' ,'IntL@b'.$pass);
			//echo $pass;

			$status = $this->User_model->checkstatus($user,$pass);
			if($status == 0)
			{
				$result = $this->User_model->get_data_student($user,$pass);
				$session_data = array(
					'U_id' => $result->s_id,
					'U_user' => $result->s_user,
					'U_pass' => $result->s_pass,
					'U_vdopass' => $vdopass,
					'U_name' => $result->s_fnameEN." ".$result->s_lnameEN,
					'U_status' => 0,
					'U_admin' => 2,
				);
				$this->session->set_userdata($session_data);
				redirect('');
			}
			elseif($status == 1)
			{
				$result = $this->User_model->get_data_lecturer($user,$pass);
				$session_data = array(
					'U_id' => $result->t_id,
					'U_user' => $result->t_user,
					'U_pass' => $result->t_pass,
					'U_vdopass' => $vdopass,
					'U_name' => $result->t_fnameEN." ".$result->t_lnameEN,
					'U_status' => 1,
					'U_admin' => $result->t_status_admin,
				);
				$this->session->set_userdata($session_data);
				redirect('');
			}
			else
			{
				$this->session->set_flashdata('error' , 'Invalid Username and Password 1');
				$this->session->set_flashdata('login-input-error' , 'login-input-error');
				redirect('login');
			}
		}
		else
		{
			$this->session->set_flashdata('error' , 'Invalid Username and Password 2');
			$this->session->set_flashdata('login-input-error' , 'login-input-error');
			redirect('login');
			//redirect('login');
		}

		/*if($this->input->server('REQUEST_METHOD') == TRUE){
			$status = $this->checkstatus($this->input->post('username'), $this->input->post('password'));
			if($status == 0){
				if($this->User_model->get_count_student($this->input->post('username'), $this->input->post('password')) == 1)
				{
					$result = $this->User_model->get_data_student($this->input->post('username'),$this->input->post('password'));
					$this->session->set_userdata(array('U_id'=>$result->s_id , 'U_user'=>$result->s_user , 'U_pass'=>$result->s_pass , 'U_status'=>$status));
					redirect('');
				}
				else
				{
					$this->session->set_flashdata('main_error','กรุณากรอกชื่อผู้ใช้ หรือ รหัสผ่านให้ถูกต้อง');
					$this->session->set_flashdata('form-control-input-error','form-control-input-error');
					redirect('login');
				}
			}
			elseif($status == 1)
			{
				if($this->User_model->get_count_lecturer($this->input->post('username'), $this->input->post('password')) == 1)
				{
					$result = $this->User_model->get_data_lecturer($this->input->post('username'),$this->input->post('password'));
					$this->session->set_userdata(array('U_id'=>$result->t_id , 'U_user'=>$result->t_user , 'U_pass'=>$result->t_pass));
					redirect('');
				}
				else
				{
					$this->session->set_flashdata('main_error','กรุณากรอกชื่อผู้ใช้ หรือ รหัสผ่านให้ถูกต้อง');
					$this->session->set_flashdata('form-control-input-error','form-control-input-error');
					redirect('login');
				}
			}
			else
			{
				$this->session->set_flashdata('main_error','กรุณากรอกชื่อผู้ใช้ หรือ รหัสผ่านให้ถูกต้อง');
				redirect('login');
			}

					
		}*/
	}

	public function checkstatus($username,$password){
		$this->db->where('s_user',$username);
		$this->db->where('s_pass',$password);
		$stu = $this->db->count_all_results('student');

		$this->db->where('t_user',$username);
		$this->db->where('t_pass',$password);
		$lt = $this->db->count_all_results('lecturer');
		
		if($stu > 0){
			return 0;
		}
		if($lt > 0){
			return 1;
		}
	}

}