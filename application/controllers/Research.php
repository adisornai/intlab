<?php  
	class Research extends CI_Controller{
		public function __construct(){
			parent::__construct();
			$this->load->model('User_model');
		}
		public function addresearch(){
			if($this->session->has_userdata('U_id')){
				if($this->input->post('add_research'))
				{
					$this->form_validation->set_rules('r_research','Research','required');
					$this->form_validation->set_rules('r_nameTH','Name Thai','required');
					$this->form_validation->set_rules('r_nameEN','Name Eng','required');
					$this->form_validation->set_rules('r_source','Source of Investment Funds','required');
					$this->form_validation->set_rules('r_budget','Budget','required|numeric');
					$this->form_validation->set_rules('r_begin','Begin','required');
					$this->form_validation->set_rules('r_end','End','required');
					$this->form_validation->set_rules('r_status','Status','required');

					if($this->form_validation->run()){
						$data = array(
							'r_research' => $this->input->post('r_research'),
							'r_nameTH' => $this->input->post('r_nameTH'),
							'r_nameEN' => $this->input->post('r_nameEN'),
							'r_head'=> $this->session->U_id,
							'r_source' => $this->input->post('r_source'),
							'r_budget' => $this->input->post('r_budget'),
							'r_begin' => $this->input->post('r_begin'),
							'r_end' => $this->input->post('r_end'),
							'r_status' => $this->input->post('r_status'),
						);
						$this->User_model->insert_re($data);
					}
					
				}
				$this->load->view('pages/header');
				$this->load->view('pages/research/addresearch');
				$this->load->view('pages/footer');
			}else{
				redirect('login');
			}
		}

		public function uploadfileresearch($rid){
			if($this->session->has_userdata('U_id')){
				$data['profile'] = $this->User_model->select_row_research($rid);
				$data['rid'] = $rid;
				$this->load->view('pages/header');
				$this->load->view('pages/research/uploadfileresearch',$data);
				$this->load->view('pages/footer');
			}
			else{
				redirect('login');
			}
		}

		public function edit_research($rid){
			if($this->session->has_userdata('U_id')){
				if($this->input->post('edit_research')){
					$this->form_validation->set_rules('r_research','Research','required');
					$this->form_validation->set_rules('r_nameTH','Name Thai','required');
					$this->form_validation->set_rules('r_nameEN','Name Eng','required');
					$this->form_validation->set_rules('r_source','Source of Investment Funds','required');
					$this->form_validation->set_rules('r_budget','Budget','required|numeric');
					$this->form_validation->set_rules('r_begin','Begin','required');
					$this->form_validation->set_rules('r_end','End','required');
					$this->form_validation->set_rules('r_status','Status','required');

					if($this->form_validation->run()){
						$data = array(
							'r_research' => $this->input->post('r_research'),
							'r_nameTH' => $this->input->post('r_nameTH'),
							'r_nameEN' => $this->input->post('r_nameEN'),
							'r_source' => $this->input->post('r_source'),
							'r_budget' => $this->input->post('r_budget'),
							'r_begin' => $this->input->post('r_begin'),
							'r_end' => $this->input->post('r_end'),
							'r_status' => $this->input->post('r_status'),
						);
						$this->User_model->update_re($data ,$rid);
					}
					
				}
				$data['mydata'] = $this->User_model->select_row_research($rid);
				$this->load->view('pages/header');
				$this->load->view('pages/research/editresearch',$data);
				$this->load->view('pages/footer');
			}
			else{
				redirect('login');
			}
		}

		public function showresearch($id){
			if($this->session->has_userdata('U_id')){
				$data['myres'] = $this->User_model->select_row_research($id);
				$data['head'] = $this->User_model->select_row_student($data['myres']->r_head);
				$data['prename'] = $this->User_model->select_row_prename($data['head']->s_prename);
				$data['coresearcher'] = $this->User_model->select_rows_coresearcher($id);
				$this->load->view('pages/header');
				$this->load->view('pages/research/showresearch',$data);
				$this->load->view('pages/footer');
			}
			else{
				redirect('login');
			}
			
		}

		public function addcoresearcher($id){
			if($this->session->has_userdata('U_id')){
				$data['student'] = $this->User_model->load_student();
				$data['myresearcher'] = $this->User_model->select_row_research($id);
				$data['mycoresearcher'] = $this->User_model->select_rows_coresearcher($id);
				$data['myidresearch'] = $id;
				$this->load->view('pages/header');
				$this->load->view('pages/research/addcoresearcher',$data);
				$this->load->view('pages/footer');
			}
			else
			{
				redirect('login');
			}
		}
		public function load_coresearcher(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('reid');
				$ans = $this->User_model->select_rows_coresearcher($id);

				echo json_encode($ans);
			}
			else{
				redirect('login');
			}
		}	

		public function add_coresearcher(){
			if($this->session->has_userdata('U_id')){
				$c_rid = $this->input->post('rid');
				$c_sid = $this->input->post('sid');
				$data = array(
					'c_rid' => $c_rid,
					'c_sid' => $c_sid,
				);
				$count_row = $this->User_model->select_num_row_coresearcher($c_rid, $c_sid);
				if($count_row > 0){
					$ans['status'] = FALSE;
				}
				else{
					$ans['status'] = $this->User_model->insert_coresearcher($data);
				}
				echo json_encode($ans);
			}
			else{
				redirect('login');
			}
		}



		public function delete_coresearcher(){
			if($this->session->has_userdata('U_id')){
				$data = array(
					'c_id' => $this->input->post('c_id'),
					'c_sid' => $this->input->post('c_sid'),
				);

				$ans = $this->User_model->delete_coresearcher($data);
				echo json_encode($ans);
			}
			else{
				redirect('login');
			}
		}

		public function ajax_upload_file(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				if(isset($_FILES["image_file"]["name"])){
					$config['upload_path'] = './assets/file/research/';
					$config['allowed_types'] = 'pdf';
					$config['file_name'] = $id;
					$new_name = $id;
					$this->load->library('upload' ,$config);
					@unlink(FCPATH.'assets\file\research\\'.$new_name.'.pdf');
					if(!$this->upload->do_upload('image_file')){
						echo $this->upload->display_errors();
					}
					else{
						$data = $this->upload->data();
						$file = array('r_file' => 'assets/file/research/'.$data["file_name"]);
						$this->User_model->upload_file($id,$file);
						echo "Success";
					}
				}
			}
			else{
				redirect('login');
			}

		}
	}