<?php  
	class Pages extends CI_Controller{

		public function __construct(){
			parent::__construct();
			$this->load->model('User_model');
		}

		public function index(){
			$data['ac1'] = 'na-ac';
			$data['ac2'] = '';
			$data['ac3'] = '';
			$this->load->view('pages/header',$data);
			$this->load->view('pages/index');
			$this->load->view('pages/footer');
		}

		public function show(){
			$this->load->view('pages/header');
			$this->load->view('pages/show');
			$this->load->view('pages/footer');
		}

		public function about(){
			$data['ac1'] = '';
			$data['ac2'] = 'na-ac';
			$data['ac3'] = '';
			$this->load->view('pages/header',$data);
			$this->load->view('pages/about');
			$this->load->view('pages/footer');
		}

		public function profile(){
			if($this->session->has_userdata('U_id'))
			{
				if($this->input->post('update_profile')){
					if($this->session->U_status == 0){
						$this->form_validation->set_rules('s_prename','Prename','required');
						$this->form_validation->set_rules('s_fnameTH','First Name TH','required');
						$this->form_validation->set_rules('s_lnameTH','Last Name TH','required');
						$this->form_validation->set_rules('s_fnameEN','First Name EN','required');
						$this->form_validation->set_rules('s_lnameEN','Last Name EN','required');
						$this->form_validation->set_rules('s_email','Email','required|valid_email');
						$this->form_validation->set_rules('s_phone','Phone','required|numeric|min_length[10]');
						$this->form_validation->set_rules('s_begin','Begin','required');
						//$this->form_validation->set_rules('s_end','End','required');
						$this->form_validation->set_rules('s_university','University','required');
						$this->form_validation->set_rules('s_faculty','Faculty','required');
						$this->form_validation->set_rules('s_major','Major','required');
						$this->form_validation->set_rules('s_adviser','Adviser','required');
						//$this->form_validation->set_rules('s_coadviser','Co-Adviser','required');
						$this->form_validation->set_rules('s_house_no','House no.','required');
						$this->form_validation->set_rules('s_village_no','Village no.','required');
						$this->form_validation->set_rules('s_lane','Lane','required');
						$this->form_validation->set_rules('s_road','Road','required');
						$this->form_validation->set_rules('s_province','Province','required');
						$this->form_validation->set_rules('s_amphur','Amphur','required');
						$this->form_validation->set_rules('s_district','District','required');
						$this->form_validation->set_rules('s_zip','Post Code','required');
						if($this->form_validation->run()){
							$read = array(
								's_prename' => $this->input->post('s_prename'),
								's_fnameTH' => $this->input->post('s_fnameTH'),
								's_lnameTH' => $this->input->post('s_lnameTH'),
								's_fnameEN' => $this->input->post('s_fnameEN'),
								's_lnameEN' => $this->input->post('s_lnameEN'),
								's_email' => $this->input->post('s_email'),
								's_phone' => $this->input->post('s_phone'),
								's_begin' => $this->input->post('s_begin'),
								's_end' => $this->input->post('s_end'),
								's_university' => $this->input->post('s_university'),
								's_faculty' => $this->input->post('s_faculty'),
								's_major' => $this->input->post('s_major'),
								's_adviser' => $this->input->post('s_adviser'),
								's_coadviser' => $this->input->post('s_coadviser'),
								's_house_no' => $this->input->post('s_house_no'),
								's_village_no' => $this->input->post('s_village_no'),
								's_lane' => $this->input->post('s_lane'),
								's_road' => $this->input->post('s_road'),
								's_province' => $this->input->post('s_province'),
								's_amphur' => $this->input->post('s_amphur'),
								's_district' => $this->input->post('s_district'),
								's_zip' => $this->input->post('s_zip')
							);
							$UID = $this->session->U_id;
							$this->User_model->update_data_student($UID,$read);
						}
					}	
					else{
						$this->form_validation->set_rules('t_academic','Academic','required');
						$this->form_validation->set_rules('t_fnameTH','First Name Thai','required');
						$this->form_validation->set_rules('t_lnameTH','Last Name Thai','required');
						$this->form_validation->set_rules('t_fnameEN','First Name English','required');
						$this->form_validation->set_rules('t_lnameEN','Last Name English','required');
						$this->form_validation->set_rules('t_email','Email','required');
						$this->form_validation->set_rules('t_phone','Phone','required');
						$this->form_validation->set_rules('t_university','University','required');
						$this->form_validation->set_rules('t_faculty','Faculty','required');
						$this->form_validation->set_rules('t_major','Major','required');
						$this->form_validation->set_rules('t_special_research','Special Research','required');
						$this->form_validation->set_rules('t_detail','Detail','required');
						if($this->form_validation->run()){
							$read = array(
								't_academic' => $this->input->post('t_academic'),
								't_fnameTH' => $this->input->post('t_fnameTH'),
								't_lnameTH' => $this->input->post('t_lnameTH'),
								't_fnameEN' => $this->input->post('t_fnameEN'),
								't_lnameEN' => $this->input->post('t_lnameEN'),
								't_email' => $this->input->post('t_email'),
								't_phone' => $this->input->post('t_phone'),
								't_university' => $this->input->post('t_university'),
								't_faculty' => $this->input->post('t_faculty'),
								't_major' => $this->input->post('t_major'),
								't_special_research' => $this->input->post('t_special_research'),
								't_detail' => $this->input->post('t_detail')
							);
							$UID = $this->session->U_id;
							$this->User_model->update_data_lecturer($UID,$read);
						}
					}
				}
				
				$data['prename'] = $this->User_model->load_prename();
				$data['academic'] = $this->User_model->load_academic_ranks();
				$data['education'] = $this->User_model->load_education();
				$data['province'] = $this->User_model->load_province();
				$data['university'] = $this->User_model->load_university();
				$data['lecturer'] = $this->User_model->load_lecturer();
				if($this->session->U_status == 0)
				{
					$data['profile'] = $this->User_model->select_row_student($this->session->U_id);
					$this->load->view('pages/header');
					$this->load->view('pages/profile/profile_student',$data);
					$this->load->view('pages/footer');
				}
				else
				{
					$data['profile'] = $this->User_model->select_row_lecturer($this->session->U_id);
					$this->load->view('pages/header');
					$this->load->view('pages/profile/profile_lecturer',$data);
					$this->load->view('pages/footer');
				}
			}
			else
			{
				redirect('login');
			}
			/*
			if(!$this->session->has_userdata('U_id')){
				redirect('');
			}else{
				$status = $this->User_model->checkstatus($this->session->U_user , $this->session->U_pass);
				if($status == 0){
					$data['f_data'] = $this->User_model->get_data_student($this->session->U_user , $this->session->U_pass);
				}else{
					$data['f_data'] = $this->User_model->get_data_lecturer($this->session->U_user , $this->session->U_pass);
				}
				$data['prename'] = $this->User_model->load_prename();
				$data['education'] = $this->User_model->load_education();
				$this->load->view('pages/header');
				$this->load->view('pages/profile',$data);
				$this->load->view('pages/footer');
			}
			*/
		}

		public function research(){
			if(!$this->session->has_userdata('U_id')){
				redirect('login');
			}else{
				$id = $this->session->U_id;
				$s = 0;
				$data['lecturer'] = $this->User_model->load_lecturer();
				$data['student'] = $this->User_model->load_student();
				$data['research'] = $this->User_model->select_rows_research_user($id,$s);
				$this->load->view('pages/header',$data);
				$this->load->view('pages/research');
				$this->load->view('pages/footer');
			}
		}
		public function research_select(){
			$reid = $this->input->post('reid');
			echo json_encode($this->User_model->select_re($reid));

		}
		public function research_insert(){
			$re = array(
				'r_nameTH' => $this->input->post('r_nameTH'),
				'r_nameEN' => $this->input->post('r_nameEN'),
				'r_head' => $this->input->post('r_head'),
				'r_contributor1' => $this->input->post('r_contributor1'),
				'r_contributor2' => $this->input->post('r_contributor2'),
				'r_contributor3' => $this->input->post('r_contributor3'),
				'r_budget' => $this->input->post('r_budget'),
				'r_begin' => $this->input->post('r_begin'),
				'r_end' => $this->input->post('r_end'),
				'r_status' => $this->input->post('r_status'),
				'r_file' => $this->input->post('r_file')
			);
			$this->User_model->insert_re($re);
			redirect('research');
		}
		public function research_update(){
			$re = array(
				'r_nameTH' => $this->input->post('r_nameTH'),
				'r_nameEN' => $this->input->post('r_nameEN'),
				'r_head' => $this->input->post('r_head'),
				'r_contributor1' => $this->input->post('r_contributor1'),
				'r_contributor2' => $this->input->post('r_contributor2'),
				'r_contributor3' => $this->input->post('r_contributor3'),
				'r_budget' => $this->input->post('r_budget'),
				'r_begin' => $this->input->post('r_begin'),
				'r_end' => $this->input->post('r_end'),
				'r_status' => $this->input->post('r_status'),
				'r_file' => $this->input->post('r_file')
			);
			$this->User_model->update_re($re,$this->input->post('r_id'));
			redirect('research');
		}

		public function publication(){
			if($this->session->has_userdata('U_id')){
				$data['publication'] = $this->User_model->load_publish();
				$this->load->view('pages/header');
				$this->load->view('pages/publication',$data);
				$this->load->view('pages/footer');
			}else{
				redirect('login');
			}
		}
		public function update_publish(){
			$pu = array(
				'p_user' => $this->session->U_id,
				'p_ps_id' => $this->input->post('p_ps_id'),
				'p_status_type' => $this->input->post('p_status_type'),
				'p_name' => $this->input->post('p_name'),
				'p_author' => $this->input->post('p_author'),
				'p_sources' => $this->input->post('p_sources'),
				'p_year' => $this->input->post('p_year'),
				'p_volume' => $this->input->post('p_volume'),
				'p_issue' => $this->input->post('p_issue'),
				'p_page' => $this->input->post('p_page'),
				'p_links' => $this->input->post('p_links'),
				'p_status' => $this->input->post('p_status')
			);
			$this->User_model->insert_pu($pu);
			redirect('publish');
		}

		public function videocall(){
			$data['ac1'] = '';
			$data['ac2'] = '';
			$data['ac3'] = 'na-ac';
			if(!$this->session->has_userdata('U_id')){
				redirect('login');
			}else{
				if($this->session->U_status == 0)
				{
					$data['profile'] = $this->User_model->get_data_student($this->session->U_user , $this->session->U_pass);
					$this->load->view('vdocall/header',$data);
					$this->load->view('pages/videocall');
					$this->load->view('vdocall/footer');
				}
				else
				{
					$data['profile'] = $this->User_model->get_data_lecturer($this->session->U_user , $this->session->U_pass);
					$this->load->view('vdocall/header',$data);
					$this->load->view('pages/videocall');
					$this->load->view('vdocall/footer');
				}
			}
		}

		public function admin(){
			if($this->session->has_userdata('U_id') && $this->session->U_admin == 1)
			{
				if($this->session->U_status==1)
				{
					$this->load->view('admin/header');
					$this->load->view('pages/admin');
					$this->load->view('admin/footer');
				}
				else
				{
					redirect('');
				}
			}
			else
			{
				redirect('');
			}
			
		}

		public function login(){

			if($this->session->has_userdata('U_id'))
			{
				redirect('');
			}else{
				$this->load->view('pages/header');
				$this->load->view('pages/login');
				$this->load->view('pages/footer');
			}
			
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('');
		}

		public function load_faculty()
		{
			$id = $this->input->post('id');
			$answer = $this->User_model->select_rows_faculty($id);
			echo json_encode($answer);
		}
		
		public function load_major()
		{
			$uid = $this->input->post('uid');
			$fid = $this->input->post('fid');
			$answer = $this->User_model->select_rows_major($uid, $fid);
			echo json_encode($answer);
		}

	}