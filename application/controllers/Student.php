<?php  
	/**
	 * 
	 */
	class Student extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model('User_model');
		}

		public function edit($id)
		{
			$data['data'] = $this->User_model->get_data_student_id($id);
			$data['prename'] = $this->User_model->load_prename();
			$this->load->view('admin/header',$data);
			$this->load->view('admin/studentedit');
			$this->load->view('admin/footer');
		}
	}
?>