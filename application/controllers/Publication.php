<?php 
	class Publication extends CI_Controller{
		public function __construct(){
			parent::__construct();
			
			$this->load->model('User_model');
		}

		public function addpublication(){
			if($this->session->has_userdata('U_id')){
				if($this->input->post('add_publication')){
					//$this->form_validation->set_rules('p_user','User','required');
					$this->form_validation->set_rules('p_ps_id','Published Standards','required');
					$this->form_validation->set_rules('p_nameTH','Name Thai','required');
					$this->form_validation->set_rules('p_nameEN','Name Eng','required');
					$this->form_validation->set_rules('p_detail','Detail','required');
					$this->form_validation->set_rules('p_year','Year','required');
					$this->form_validation->set_rules('p_status_type','Publish Type','required');
					$this->form_validation->set_rules('p_first_author','First Author','required');
					$this->form_validation->set_rules('p_second_author','Second Author','required');
					$this->form_validation->set_rules('p_language','Language','required');
					//$this->form_validation->set_rules('p_links','File','required');

					if($this->form_validation->run()){
						$data = array(
							'p_user' => $this->session->U_id,
							'p_ps_id' => $this->input->post('p_ps_id'),
							'p_nameTH' => $this->input->post('p_nameTH'),
							'p_nameEN' => $this->input->post('p_nameEN'),
							'p_detail' => $this->input->post('p_detail'),
							'p_year' => $this->input->post('p_year'),
							'p_status_type' => $this->input->post('p_status_type'),
							'p_first_author' => $this->input->post('p_first_author'),
							'p_second_author' => $this->input->post('p_second_author'),
							'p_language' => $this->input->post('p_language'),
						);

						$this->User_model->insert_publication($data);
						redirect('publication');

					}
				}
				$this->load->view('pages/header');
				$this->load->view('pages/publication/addpublication');
				$this->load->view('pages/footer');
			}else{
				redirect('login');
			}
		}

		public function uploadpublication($pid){
			if($this->session->has_userdata('U_id')){
				$data['id'] = $pid;
				$this->load->view('pages/header');
				$this->load->view('pages/publication/uploadpublication',$data);
				$this->load->view('pages/footer');
			}else{
				redirect('login');
			}
		}

		public function edit_publication($pid){
			if($this->session->has_userdata('U_id')){
				if($this->input->post('edit_publication')){
					//$this->form_validation->set_rules('p_user','User','required');
					$this->form_validation->set_rules('p_ps_id','Published Standards','required');
					$this->form_validation->set_rules('p_nameTH','Name Thai','required');
					$this->form_validation->set_rules('p_nameEN','Name Eng','required');
					$this->form_validation->set_rules('p_detail','Detail','required');
					$this->form_validation->set_rules('p_year','Year','required');
					$this->form_validation->set_rules('p_status_type','Publish Type','required');
					$this->form_validation->set_rules('p_first_author','First Author','required');
					$this->form_validation->set_rules('p_second_author','Second Author','required');
					$this->form_validation->set_rules('p_language','Language','required');
					//$this->form_validation->set_rules('p_links','File','required');

					if($this->form_validation->run()){
						$data = array(
							'p_ps_id' => $this->input->post('p_ps_id'),
							'p_nameTH' => $this->input->post('p_nameTH'),
							'p_nameEN' => $this->input->post('p_nameEN'),
							'p_detail' => $this->input->post('p_detail'),
							'p_year' => $this->input->post('p_year'),
							'p_status_type' => $this->input->post('p_status_type'),
							'p_first_author' => $this->input->post('p_first_author'),
							'p_second_author' => $this->input->post('p_second_author'),
							'p_language' => $this->input->post('p_language'),
						);
						//$this->User_model->update_re($data ,$rid);
					}
					
				}
				$data['mydata'] = $this->User_model->select_row_publication($pid);
				$this->load->view('pages/header');
				$this->load->view('pages/publication/editpublication',$data);
				$this->load->view('pages/footer');
			}
			else{
				redirect('login');
			}
		}

		public function addauthor($id){
			if($this->session->has_userdata('U_id')){
				$data['student'] = $this->User_model->load_student();
				$data['mypublication'] = $this->User_model->select_row_publication($id);
				$data['myauthor'] = $this->User_model->select_rows_author($id);
				$data['myidpublication'] = $id;
				$this->load->view('pages/header');
				$this->load->view('pages/publication/addauthor',$data);
				$this->load->view('pages/footer');
			}
			else
			{
				redirect('login');
			}
		}

		public function add_author(){
			if($this->session->has_userdata('U_id')){
				$au_pid = $this->input->post('pid');
				$au_sid = $this->input->post('sid');
				$data = array(
					'au_pid' => $au_pid,
					'au_sid' => $au_sid,
				);
				$count_row = $this->User_model->select_num_row_author($au_pid, $au_sid);
				if($count_row > 0){
					$ans['status'] = FALSE;
				}
				else{
					$ans['status'] = $this->User_model->insert_author($data);
				}
				echo json_encode($ans);
			}
			else{
				redirect('login');
			}
		}

		public function delete_author(){
			if($this->session->has_userdata('U_id')){
				$data = array(
					'au_id' => $this->input->post('au_id'),
					'au_sid' => $this->input->post('au_sid'),
				);
				$ans = $this->User_model->delete_author($data);
				echo json_encode($ans);
			}
			else{
				redirect('login');
			}
		}

		public function ajax_upload_file(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('id');
				if(isset($_FILES["image_file"]["name"])){
					$config['upload_path'] = './assets/file/publication/';
					$config['max_size'] = '20480';
					$config['allowed_types'] = 'pdf';
					$config['file_name'] = $id;
					$new_name = $id;
					$this->load->library('upload' ,$config);
					@unlink(FCPATH.'assets\file\publication\\'.$new_name.'.pdf');
					if(!$this->upload->do_upload('image_file')){
						echo $this->upload->display_errors();
					}
					else{
						$data = $this->upload->data();
						$file = array('p_links' => 'assets/file/publication/'.$data["file_name"]);
						$this->User_model->upload_file_publish($id,$file);
						echo "Success";
					}
				}
			}
			else{
				redirect('login');
			}

		}

		public function load_author_js(){
			if($this->session->has_userdata('U_id')){
				$id = $this->input->post('pid');
				$ans = $this->User_model->select_rows_author($id);
				echo json_encode($ans);
			}else{
				redirect('login');
			}
		}

		public function load_author(){
			if($this->session->has_userdata('U_id')){
				$ans['student'] = $this->User_model->load_student();
				$ans['lecturer'] = $this->User_model->load_lecturer();
				echo json_encode($ans);
			}else{
				redirect('login');
			}
		}

		public function load_PS(){
			if($this->session->has_userdata('U_id')){
				$ans = $this->User_model->load_ps();
				echo json_encode($ans);
			}else{
				redirect('login');
			}
		}

		public function load_PT(){
			if($this->session->has_userdata('U_id')){
				$ans = $this->User_model->load_publish_type();
				echo json_encode($ans);
			}else{
				redirect('login');
			}
		}
		
	}
 ?>