<?php  
class Admin_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function insert_student($data){
		return $this->db->insert('student', $data);
	}
	public function update_student($stdid,$data){
		$this->db->where('s_id',$stdid);
		return $this->db->update('student', $data);
	}
	public function delete_student($id){
		$this->db->where('s_id',$id);
		return $this->db->delete('student');
	}

	public function insert_research($data){
		return $this->db->insert('research', $data);
	}
	public function update_research($id,$data){
		$this->db->where('r_id',$id);
		return $this->db->update('research', $data);
	}
	public function delete_research($id){
		$this->db->where('r_id',$id);
		return $this->db->delete('research');
	}

	public function insert_publication($data){
		return $this->db->insert('publish', $data);
	}
	public function update_publication($id,$data){
		$this->db->where('p_id',$id);
		return $this->db->update('publish', $data);
	}
	public function delete_publication($id){
		$this->db->where('p_id',$id);
		return $this->db->delete('publish');
	}
	
	public function insert_lecturer($data){
		return $this->db->insert('lecturer', $data);
	}
	public function update_lecturer($id,$data){
		$this->db->where('t_id',$id);
		return $this->db->update('lecturer', $data);
	}
	public function delete_lecturer($id){
		$this->db->where('t_id',$id);
		return $this->db->delete('lecturer');
	}

	public function check_count_st($id){
		$this->db->where('s_sid',$id);
		return $this->db->count_all_results('student');
	}
	public function check_count_le($id,$user){
		$this->db->where('t_id',$id);
		$this->db->where('t_user',$user);
		return $this->db->count_all_results('lecturer');
	}

	public function insert_university($data){
		return $this->db->insert('university', $data);
	}
	public function update_university($id,$data){
		$this->db->where('u_id',$id);
		return $this->db->update('university', $data);
	}
	public function delete_university($id){
		$this->db->where('u_id',$id);
		return $this->db->delete('university');
	}

	public function insert_faculty($data){
		return $this->db->insert('faculty', $data);
	}
	public function update_faculty($id,$data){
		$this->db->where('f_id',$id);
		return $this->db->update('faculty', $data);
	}
	public function delete_faculty($id){
		$this->db->where('f_id',$id);
		return $this->db->delete('faculty');
	}

	public function insert_major($data){
		return $this->db->insert('major', $data);
	}
	public function update_major($id,$data){
		$this->db->where('m_id',$id);
		return $this->db->update('major', $data);
	}
	public function delete_major($id){
		$this->db->where('m_id',$id);
		return $this->db->delete('major');
	}

	public function insert_prename($data){
		return $this->db->insert('prename', $data);
	}
	public function update_prename($id,$data){
		$this->db->where('n_id',$id);
		return $this->db->update('prename', $data);
	}
	public function delete_prename($id){
		$this->db->where('n_id',$id);
		return $this->db->delete('prename');
	}

	public function insert_academic_ranks($data){
		return $this->db->insert('academic_ranks', $data);
	}
	public function update_academic_ranks($id,$data){
		$this->db->where('ap_id',$id);
		return $this->db->update('academic_ranks', $data);
	}
	public function delete_academic_ranks($id){
		$this->db->where('ap_id',$id);
		return $this->db->delete('academic_ranks');
	}

	public function insert_published_standards($data){
		return $this->db->insert('published_standards', $data);
	}
	public function update_published_standards($id,$data){
		$this->db->where('ps_id',$id);
		return $this->db->update('published_standards', $data);
	}
	public function delete_published_standards($id){
		$this->db->where('ps_id',$id);
		return $this->db->delete('published_standards');
	}
	
	public function insert_publish_type($data){
		return $this->db->insert('publish_type', $data);
	}
	public function update_publish_type($id,$data){
		$this->db->where('pt_id',$id);
		return $this->db->update('publish_type', $data);
	}
	public function delete_publish_type($id){
		$this->db->where('pt_id',$id);
		return $this->db->delete('publish_type');
	}

	public function insert_education($data){
		return $this->db->insert('education', $data);
	}
	public function update_education($id,$data){
		$this->db->where('e_id',$id);
		return $this->db->update('education', $data);
	}
	public function delete_education($id){
		$this->db->where('e_id',$id);
		return $this->db->delete('education');
	}
	
	public function insert_activity($data){
		return $this->db->insert('activity', $data);
	}
	public function update_activity($id,$data){
		$this->db->where('a_id',$id);
		return $this->db->update('activity', $data);
	}
	public function delete_activity($id){
		$this->db->where('a_id',$id);
		return $this->db->delete('activity');
	}


	public function load_all_data_student(){
		$query = $this->db->get('student');
		return $query->result();
	}

	public function load_all_data_lecturer(){
		$query = $this->db->get('lecturer');
		return $query;
	}

	public function load_all_data_research(){
		$query = $this->db->get('research');
		return $query->result();
	}

	public function load_all_data_publish(){
		$query = $this->db->get('publish');
		return $query->result();
	}
}