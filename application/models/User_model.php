<?php
class User_model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_count_student($username,$password)
	{
		$this->db->where('s_user',$username);
		$this->db->where('s_pass',$password);
		return $this->db->count_all_results('student');
	}
	public function get_count_lecturer($username,$password)
	{
		$this->db->where('t_user',$username);
		$this->db->where('t_pass',$password);
		return $this->db->count_all_results('lecturer');
	}
	
	public function get_data_student($username,$password){
		$this->db->where('s_user',$username);
		$this->db->where('s_pass',$password);
		$query = $this->db->get('student');
		return $query->row();
	}
	public function get_data_student_id($id){
		$this->db->where('s_id',$id);
		$query = $this->db->get('student');
		return $query->row();
	}
	public function get_data_lecturer($username,$password){
		$this->db->where('t_user',$username);
		$this->db->where('t_pass',$password);
		$query = $this->db->get('lecturer');
		return $query->row();
	}
	public function get_data_lecturer_id($id){
		$this->db->where('t_id',$id);
		$query = $this->db->get('lecturer');
		return $query->row();
	}
	public function update_data_student($id,$profile){
		$this->db->where('s_id',$id);
		$this->db->update('student',$profile);
	}
	public function update_data_lecturer($id,$profile){
		$this->db->where('t_id' , $id);
		$this->db->update('lecturer',$profile);
	}

	public function upload_file($id,$file){
		$this->db->where('r_id',$id);
		$this->db->update('research',$file);
	}

	public function upload_file_publish($id,$file){
		$this->db->where('p_id',$id);
		$this->db->update('publish',$file);
	}

	public function insert_pu($pu){
		$this->db->insert('publish',$pu);
	}
	
	public function select_row_student($id){
		$this->db->where('s_id',$id);
		$query = $this->db->get('student');
		return $query->row();
	}

	public function select_row_lecturer($id){
		$this->db->where('t_id',$id);
		$query = $this->db->get('lecturer');
		return $query->row();
	}
	
	public function select_row_research($id){
		$this->db->where('r_id',$id);
		$query = $this->db->get('research');
		return $query->row();
	}

	public function select_row_major($id){
		$this->db->where('m_id',$id);
		$query = $this->db->get('major');
		return $query->row();
	}	

	public function select_rows_major($u_id, $f_id){
		$this->db->where('u_id',$u_id);
		$this->db->where('f_id',$f_id);
		$query = $this->db->get('major');
		return $query->result();
	}	

	public function select_rows_coresearcher($id){
		$this->db->select('c_id, c_rid, c_sid, n_engname, s_fnameEN, s_lnameEN');
		$this->db->where('c_rid',$id);
		$this->db->from('coresearcher');
		$this->db->join('student', 'coresearcher.c_sid = student.s_id');
		$this->db->join('prename', 'student.s_prename = prename.n_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function select_row_prename($id){
		$this->db->where('n_id',$id);
		$query = $this->db->get('prename');
		return $query->row();
	}

	public function select_row_academic_ranks($id){
		$this->db->where('ap_id',$id);
		$query = $this->db->get('academic_ranks');
		return $query->row();
	}

	public function select_row_published_standards($id){
		$this->db->where('ps_id',$id);
		$query = $this->db->get('published_standards');
		return $query->row();
	}
	

	public function insert_re($re){
		return $this->db->insert('research',$re);
	}

	public function select_rows_research_user($id,$s){
		$this->db->where('r_head',$id);
		$this->db->where('r_status_head',$s);
		$query = $this->db->get('research');
		return $query->result();
	}

	public function update_re($re,$reid){
		$this->db->where('r_id',$reid);
		$this->db->update('research',$re);
	}

	public function select_row_publication($id){
		$this->db->where('p_id',$id);
		$query = $this->db->get('publish');
		return $query->row();
	}

	public function select_rows_author($id){
		$this->db->select('au_id, au_pid, au_sid, n_engname, s_fnameEN, s_lnameEN');
		$this->db->where('au_pid',$id);
		$this->db->from('author');
		$this->db->join('student', 'author.au_sid = student.s_id');
		$this->db->join('prename', 'student.s_prename = prename.n_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function select_row_university($id){
		$this->db->where('u_id',$id);
		$query = $this->db->get('university');
		return $query->row();
	}

	public function select_row_faculty($id){
		$this->db->where('f_id',$id);
		$query = $this->db->get('faculty');
		return $query->row();
	}

	public function select_rows_faculty($id){
		$this->db->where('u_id',$id);
		$query = $this->db->get('faculty');
		return $query->result();
	}
	
	public function select_row_publish_type($id){
		$this->db->where('pt_id',$id);
		$query = $this->db->get('publish_type');
		return $query->row();
	}
	
	public function select_row_education($id){
		$this->db->where('e_id',$id);
		$query = $this->db->get('education');
		return $query->row();
	}

	public function select_row_activity($id){
		$this->db->where('a_id',$id);
		$query = $this->db->get('activity');
		return $query->row();
	}

	public function upload_img($id,$img,$s){
		if($s == 0){
			$this->db->where('s_id',$id);
			$this->db->update('student',$img);
		}
		else{
			$this->db->where('t_id',$id);
			$this->db->update('lecturer',$img);
		}
		
	}

	public function select_row_coresearcher($c_rid, $c_sid){
		$this->db->where('c_rid',$c_rid);
		$this->db->where('c_sid',$c_sid);
		$query = $this->db->get('coresearcher');
		return $query->row();
	}

	public function select_num_row_coresearcher($c_rid, $c_sid){
		$this->db->where('c_rid',$c_rid);
		$this->db->where('c_sid',$c_sid);
		$query = $this->db->get('coresearcher');
		return $query->num_rows();
	}

	public function select_num_row_author($au_pid, $au_sid){
		$this->db->where('au_pid',$au_pid);
		$this->db->where('au_sid',$au_sid);
		$query = $this->db->get('author');
		return $query->num_rows();
	}

	public function insert_author($data){
		return $this->db->insert('author',$data);
	}

	public function delete_author($data){
		return $this->db->delete('author',$data);
	}

	public function insert_coresearcher($data){
		return $this->db->insert('coresearcher',$data);
	}	

	public function delete_coresearcher($data){
		return $this->db->delete('coresearcher',$data);
	}	

	public function insert_publication($data){
		return $this->db->insert('publish',$data);
	}	

	public function delete_publication($data){
		return $this->db->delete('publish',$data);
	}	

	public function change_password($id,$pass,$s){
		if($s == 0){
			$this->db->where('s_id',$id);
			$this->db->update('student',$pass);
		}
		else{
			$this->db->where('t_id',$id);
			$this->db->update('lecturer',$pass);
		}
		
	}
	

	public function checkstatus($username,$password){
		$this->db->where('s_user',$username);
		$this->db->where('s_pass',$password);
		$stu = $this->db->count_all_results('student');

		$this->db->where('t_user',$username);
		$this->db->where('t_pass',$password);
		$lt = $this->db->count_all_results('lecturer');
		
		if($stu > 0){
			return 0;
		}
		if($lt > 0){
			return 1;
		}
		if($stu == 0 && $lt == 0){
			return 2;
		}
	}	

	public function load_student(){
		$query = $this->db->get('student');
		return $query->result();
	}
	public function load_lecturer(){
		$query = $this->db->get('lecturer');
		return $query->result();
	}
	public function load_research(){
		$query = $this->db->get('research');
		return $query->result();
	}
	public function load_publish(){
		$query = $this->db->get('publish');
		return $query->result();
	}
	public function load_ps(){
		$query = $this->db->get('published_standards');
		return $query->result();
	}
	public function load_publish_type(){
		$query = $this->db->get('publish_type');
		return $query->result();
	}

	public function load_prename(){
		$query = $this->db->get('prename');
		return $query->result();
	}

	public function load_academic_ranks(){
		$query = $this->db->get('academic_ranks');
		return $query->result();
	}

	public function load_university(){
		$query = $this->db->get('university');
		return $query->result();
	}

	public function load_education(){
		$query = $this->db->get('education');
		return $query->result();
	}

	public function load_province(){
		$query = $this->db->get('province');
		return $query->result();
	}

	public function load_activity(){
		$query = $this->db->get('activity');
		return $query->result();
	}
	

	public function load_amphur($id_p){
		$this->db->where('PROVINCE_ID',$id_p);
		$query = $this->db->get('amphur');
		return $query->result();
	}

	public function load_postcode($id_a){
		$this->db->where('AMPHUR_ID',$id_a);
		$query = $this->db->get('amphur');
		return $query->result();
	}

	public function load_district($id_a){
		$this->db->where('AMPHUR_ID',$id_a);
		$query = $this->db->get('district');
		return $query->result();
	}

	public function load_major(){
		$this->db->select('*');
		$this->db->from('major');
		$this->db->join('faculty', 'major.f_id = faculty.f_id');
		$this->db->join('university', 'university.u_id = faculty.u_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function load_faculty(){
		$this->db->select('*');
		$this->db->from('faculty');
		$this->db->join('university', 'university.u_id = faculty.u_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function load_published_standards(){
		$query = $this->db->get('published_standards');
		return $query->result();
	}

}