$(function(){

	$('#addstd-submit').click(function(){
		var id = $('#s_id').val();
      var user = $('#s_user').val();
      var pass = $('#s_pass').val();
		var nt = $('#s_prename').val();
      var fn = $('#s_fnameEN').val();
      var ln = $('#s_lnameEN').val();
      var phone = $('#s_phone').val();
      var email = $('#email').val();
      var url = $('#insert_student').attr('action');
      
      if(id != '' && user != '' && pass != '')
      {
         $.ajax({
            url: url,
            type: 'POST',
            data: {
               s_id: id,
               s_user: user,
               s_pass: pass,
               s_prename: nt,
               s_fnameEN: fn,
               s_lnameEN: ln
            },         
         }).done(function(){
            console.log('done');
         }).fail(function(){
            $('#admin-alert').slideDown('slow' , function(){});
            $('#al-label').text('Failing');
         }).always(function(){

         });
      }
      else
      {
         $('#admin-alert').slideDown('slow' , function(){});
         $('#al-label').text('Failing');
         
      }

	});

   $('.reseach-show-add').click(function(event) {
      $('.reseach-container').show('fast', function() {
         
      });
   });

   $('.reseach-close').click(function(event) {
      $('.reseach-container').hide('fast', function() {
         
      });
   });

   $(window).click(function(event) {
      if(event.target == $('.reseach-container')[0]){
         $('.reseach-container').hide('fast', function() {
         
         });
      }
   });


   $('.publish-show-add-admin').click(function(event) {
      $('.publish-container-admin').show('fast', function() {
         
      });
   });

   $('.publish-close-admin').click(function(event) {
      $('.publish-container-admin').hide('fast', function() {
         
      });
   });

   $(window).click(function(event) {
      if(event.target == $('.publish-container-admin')[0]){
         $('.publish-container-admin').hide('fast', function() {
         
         });
      }
   });


   $('.ad-edit-btn-close').click(function(event) {
      $('.ad-edit-st-container').fadeOut('fast', function() {

      });

   });

   $(window).click(function(event) {
      if(event.target == $('.ad-edit-st-container')[0]){
         $('.ad-edit-st-container').fadeOut('fast', function() {
         
         });
      }
   });

   $('.reseach-close-edit').click(function(event) {
      $('.reseach-container-edit').fadeOut('fast', function() {

      });
   });
   
   $(window).click(function(event) {
      if(event.target == $('.reseach-container-edit')[0]){
         $('.reseach-container-edit').fadeOut('fast', function() {

         });
      }
   });

});