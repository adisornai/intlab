;(function(window, QB, app, CONFIG, $, Backbone) {
    'use strict';

    $(function() {

        //INIT
        QB.init(
            CONFIG.CREDENTIALS.appId,
            CONFIG.CREDENTIALS.authKey,
            CONFIG.CREDENTIALS.authSecret,
            CONFIG.APP_CONFIG
        );
        
        var isAudio = false;
        var ui = {
            'income_call': '',
            'filterSelect': 'no',
            'videoSourceFilter': '',
            'audioSourceFilter': 'default',
            'bandwidthSelect': '',
            'insertOccupants': function() 
            {
                return new Promise(function(resolve, reject) {
                    app.helpers.renderUsers().then(function(res) {
                        resolve(res.users);
                        $('.list-users').empty();
                        res.users.forEach( function(element, index) {
                            if(app.caller.id != element.id)
                            {
                                $('.list-users').append("<button class='video-j-user j-user' type='' data-login='"+element.login+"' data-id='"+element.id+"' data-name='"+element.full_name+"'>"+element.full_name+"</button>");
                            }  
                        });
                    }, function(error) {
                        reject('Not found users by tag');
                    });
                });
            }
        };
        var call = {
            callTime : 0,
            callTimer: null,
            updTimer: function() {
                this.callTime += 1000;

                
              }
        };
        var remoteStreamCounter = 0;



        //LOGIN ROOM
        $(document).on('click','.j-join', function() {
            //START
            if (!QB.webrtc) {
                alert('Error: ' + CONFIG.MESSAGES.webrtc_not_avaible);
                return;
            }
            if(!window.navigator.onLine) {
                alert(CONFIG.MESSAGES['no_internet']);
                return false;
            }
            app.caller = {};
            app.callees = {};
            app.calleesAnwered = [];
            app.calleesRejected = [];
            app.users = [];

            //+--------------------+
            var uid = data_userPHP.uid;
            var full_name = data_userPHP.username;
            var password = data_userPHP.password;
            var data = {
                'uid' : uid,
                'username' : full_name,
                'password' : password,
                'room' : 'IntLab'
            };  

            app.helpers.join(data).then(function(user) {
                app.caller = user;
                $('#idvdo').append(app.caller.id);
                ui.insertOccupants().then(function(users){
                    console.log(users);
                    app.users = users;
                }, function(err) {
                    console.warn(err);
                });
                QB.chat.connect({
                    jid: uid,
                    password: password
                }, function(err, res) {
                    if(err)
                    {
                        
                    }
                    else
                    {
                      
                    }
                });
            }).catch(function(error) {
                console.error(error);
            });

            return false;
        });


        //CALL
        
        $(document).on('click', '.j-actions',function(){
            var $btn = $(this),
                mediaParams = {
                  audio: true,
                  video: false
                };

            isAudio = $btn.data('call') === 'audio';
            var additionalOptions = {"bandwith": 512};
            app.currentSession = QB.webrtc.createNewSession(
                Object.keys(app.callees), 
                QB.webrtc.CallType.AUDIO, 
                app.caller.id, 
                additionalOptions
            );

            console.log(app.currentSession);
            app.currentSession.getUserMedia(mediaParams, function(err, stream) {
                if (err){
                    console.log(err);
                }else{
                    app.currentSession.call({}, function(error) {
                        
                        if(error)
                        {
                            console.log(error);
                        }
                        else
                        {

                        }
                    }); 
                }
            });


        });


        //Accept
        $(document).on('click', '.j-accept', function() {
            var mediaParams = {
                    audio: true,
                    video: false
                };
            app.currentSession.getUserMedia(mediaParams, function(err, stream) {
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    app.currentSession.accept({});
                }
            });
            
        });


        //Refresh

        $(document).on('click', '.j-refresh',function(){
            console.log(app.users);
            ui.insertOccupants().then(function(users){
                app.users = users;
            }, function(err) {
                console.warn(err);
            });
        });




        //MARK USER
        $(document).on('click', '.j-user', function() {
            var $user = $(this),
                user = {
                    id : +$.trim($user.data('id')),
                    name : $.trim($user.data('name'))
                };
            if($user.hasClass('video-j-user-active'))
            {   
                delete app.callees[user.id];
                $user.removeClass('video-j-user-active');
            }
            else
            {
                app.callees[user.id] = user.name;
                $user.addClass('video-j-user-active');
            }
        });



        //LOGOUT

        $(document).on('click', '.j-logout', function() {
            QB.users.update(app.caller.id, {
                'full_name': app.caller.full_name,
                'tag_list': ''
            }, function(updateError, updateUser) {
                if(updateError) {
                    console.log('APP [update user] Error:', updateError);
                } 
            });
            app.caller = {};
            app.users = [];
            QB.chat.disconnect();
        });

        QB.chat.onDisconnectedListener = function() {
            console.log('onDisconnectedListener.');
        };

        QB.webrtc.onCallListener = function(session, extension) {
            console.group('onCallListener.');
                console.log('Session: ', session);
                console.log('Extension: ', extension);
            console.groupEnd();

            app.currentSession = session;
        };

        QB.webrtc.onUserNotAnswerListener = function(session, userId) {
            console.group('onUserNotAnswerListener.');
                console.log('UserId: ', userId);
                console.log('Session: ', session);
            console.groupEnd();

        };

        QB.webrtc.onAcceptCallListener = function(session, userId, extension) {
            console.group('onAcceptCallListener.');
                console.log('UserId: ', userId);
                console.log('Session: ', session);
                console.log('Extension: ', extension);
            console.groupEnd();
        };
        
        QB.webrtc.onRemoteStreamListener = function(session, userID, remoteStream) {
          // attach the remote stream to DOM element
            console.group('onRemoteStreamListener.');
                console.log('userId: ', userId);
                console.log('Session: ', session);
                console.log('Stream: ', remoteStream);
            console.groupEnd();
            app.currentSession.attachMediaStream('remoteVideo', remoteStream);
        };

    });
}(window, window.QB, window.app, window.CONFIG,  jQuery, Backbone));
