;(function(window, QB) {
    'use strict';

    /** GLOBAL */
    window.app = {};
    app.helpers = {};
    app.network = {};

    app.helpers.join = function(data) {
        var userRequiredParams = {
            'login': data.login,
            'password': data.password
        };
        return new Promise(function(resolve, reject) {
            QB.createSession(function(csErr, csRes){
                if(csErr)
                {
                    reject(csErr);
                }
                else
                {
                    QB.login(userRequiredParams, function(loginErr, loginUser){
                        if(loginErr)
                        {
                            QB.users.create({
                                'login': userRequiredParams.login,
                                'password': userRequiredParams.password,
                                'full_name': data.full_name,
                                'tag_list': data.room
                            }, function(createErr, createUser){
                                if(createErr)
                                {
                                    console.log('[create user] Error:', createErr);
                                    reject(createErr);
                                }
                                else
                                {
                                    QB.login(userRequiredParams, function(reloginErr, reloginUser) {
                                        if(reloginErr) {
                                            console.log('[relogin user] Error:', reloginErr);
                                        } else {
                                            resolve(reloginUser);
                                        }
                                    });
                                }
                            });
                        }
                        else
                        {
                            //Update ข้อมูลถ้าไม่ตรงกับปัจขจุบัน
                            if(loginUser.user_tags !== data.room || loginUser.full_name !== data.full_name)
                            {
                                QB.users.update(loginUser.id, {
                                    'full_name': data.full_name,
                                    'tag_list': data.room
                                }, function(updateError, updateUser) {
                                    if(updateError) {
                                        console.log('APP [update user] Error:', updateError);
                                        reject(updateError);
                                    } else {
                                        resolve(updateUser);
                                    }
                                });
                            }
                            else
                            {
                                resolve(loginUser);
                            }
                        }
                    });
                }
            });
        });
    };
    
    app.helpers.renderUsers = function() {
        return new Promise(function(resolve, reject) {
            var users = [];
            QB.users.get({'tags': [app.caller.user_tags], 'per_page': 100}, function(err, result){
                if(err)
                {
                    reject(err);
                }
                else
                {
                    _.each(result.items, function(item) {
                        users.push(item.user);
                    });
                    resolve({
                        'users': users
                    });
                }
            });
        });
    };



    function _getUui(identifyAppId) {
        var navigator_info = window.navigator;
        var screen_info = window.screen;
        var uid = navigator_info.mimeTypes.length;

        uid += navigator_info.userAgent.replace(/\D+/g, '');
        uid += navigator_info.plugins.length;
        uid += screen_info.height || '';
        uid += screen_info.width || '';
        uid += screen_info.pixelDepth || '';
        uid += identifyAppId;
        
        return uid;
    }

}(window, window.QB));
