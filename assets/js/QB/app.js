;(function(window, QB, app, CONFIG, $, Backbone) {
    'use strict';

    $(function() {
        var sounds = {
            'call': 'callingSignal',
            'end': 'endCallSignal',
            'rington': 'ringtoneSignal'
        };


        
        //INIT
        QB.init(
            CONFIG.CREDENTIALS.appId,
            CONFIG.CREDENTIALS.authKey,
            CONFIG.CREDENTIALS.authSecret,
            CONFIG.APP_CONFIG
        );
        //console.log(QB.webrtc.PeerConnectionState);
        
        var statesPeerConn = _.invert(QB.webrtc.PeerConnectionState);
        
        
        
        var isAudio = false;
        var ui = {
            'insertOccupants': function() 
            {
                return new Promise(function(resolve, reject) {
                    app.helpers.renderUsers().then(function(res) {
                        resolve(res.users);
                        $('.list-users').empty();
                        res.users.forEach( function(element, index) {
                            if(app.caller.id != element.id)
                            {
                                $('.list-users').append("<button class='video-j-user j-user' type='' data-login='"+element.login+"' data-id='"+element.id+"' data-name='"+element.full_name+"'>"+element.full_name+"</button>");
                            }  
                        });
                    }, function(error) {
                        reject('Not found users by tag');
                    });
                });
            }
        };
        




        //LOGIN ROOM
        $(document).on('click','.j-login', function() {
            //START
            
            $('.load-time').removeClass('video-hide');
            $('.j-login').addClass('video-hide');
            app.caller = {};
            app.callees = {};
            app.calleesAnwered = [];
            app.calleesRejected = [];
            app.users = [];
            app.usersid = [];
            app.chatid;


            //+--------------------+
            var login = data_userPHP.uid;
            var full_name = data_userPHP.username;
            var password = data_userPHP.password;
            var data = {
                'login' : login,
                'full_name' : full_name,
                'password' : password,
                'room' : 'IntLab'
            };  

            app.helpers.join(data).then(function(user) {
                app.caller = user;
                
                ui.insertOccupants().then(function(users){
                    app.users = users;

                    for (var i = 0; i < users.length; i++) {
                        app.usersid.push(users[i].id);
                    }
                    
                    
                    if(app.users.length < 2)
                    {
                        $('.list-users').empty();
                        $('.list-users').append('<p>No Users</p>');
                    }
                }, function(err) {
                    console.warn(err);
                });
                QB.chat.connect({
                    userId: app.caller.id,
                    password: data.password
                }, function(err, res) {
                    if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        var opponents = app.usersid;
                        var params = {
                            type: 2,
                            occupants_ids: opponents,
                            name: "IntLab"
                        };
                        var filters = null;
                        QB.chat.dialog.list(filters, function(err, resDialogs) {
                            if (err) {
                                console.log(err);
                            } else {
                                if(resDialogs.items.length == 0){
                                    QB.chat.dialog.create(params, function(cerr, createdDialog) {
                                        if(cerr){
                                            console.log(cerr);
                                        }
                                        else{
                                            console.log(createdDialog);
                                        }
                                    });
                                }
                                else
                                {
                                    app.chatid = resDialogs.items[0]._id;
                                    var chatupdata = [];
                                    var checkmat = true;
                                    for(var j = 0; j < app.users.length;j++){
                                        for (var i = 0; i < resDialogs.items[0].occupants_ids.length; i++) {
                                            if(app.users[j].id == resDialogs.items[0].occupants_ids[i]){
                                                checkmat = false;
                                            }
                                        }
                                        if(checkmat){
                                            chatupdata.push(app.users[j].id);
                                        }
                                        checkmat = true;
                                    }
                                    
                                    
                                    if(chatupdata.length>0)
                                    {
                                        var toUpdateParams = {
                                            push_all: {occupants_ids: chatupdata}
                                        };
                                        QB.chat.dialog.update(app.chatid, toUpdateParams, function(uerr, res) {
                                          if (err) {
                                              console.log(uerr);
                                          } else {
                                              var dialogId = app.chatid;
                                              var dialogJid = QB.chat.helpers.getRoomJidFromDialogId(dialogId);
                                              
                                              QB.chat.muc.join(dialogJid, function(resultStanza) {
                                                      var joined = true;
                                                      
                                                      for (var i = 0; i < resultStanza.childNodes.length; i++) {
                                                        var elItem = resultStanza.childNodes.item(i);
                                                        if (elItem.tagName === 'error'){
                                                          joined = false; 
                                                      }
                                                  }
                                              });
                                                load_history_Message();
                                                $('.load-time').addClass('video-hide');
                                                $('.chat').removeClass('video-hide');
                                                $('.vdo-container').removeClass('vdo-ht');
                                                $('.mainVideo').removeClass('video-hide');
                                                $('.j-logout').removeClass('video-hide');
                                          }
                                        });
                                    }
                                    else{
                                        var resD = resDialogs.items[0]
                                        /*notifyOccupants(resD.occupants_ids, resD._id);
                                        function notifyOccupants(dialogOccupants, newDialogId) {
                                          dialogOccupants.forEach(function(itemOccupanId, i, arr) {
                                            if (itemOccupanId != app.caller.id) {
                                              var msg = {
                                                type: 'chat',
                                                extension: {
                                                  notification_type: 1,
                                                  _id: newDialogId,
                                                }, 
                                              };
                                         
                                              QB.chat.send(itemOccupanId, msg);
                                              console.log(msg);
                                            }
                                            
                                          });
                                        }*/
                                        var dialogId = app.chatid;
                                        var dialogJid = QB.chat.helpers.getRoomJidFromDialogId(dialogId);

                                        QB.chat.muc.join(dialogJid, function(resultStanza) {
                                              var joined = true;

                                              for (var i = 0; i < resultStanza.childNodes.length; i++) {
                                                var elItem = resultStanza.childNodes.item(i);
                                                if (elItem.tagName === 'error'){
                                                  joined = false; 
                                              }
                                          }
                                      });
                                        load_history_Message();
                                        $('.load-time').addClass('video-hide');
                                        $('.vdo-container').removeClass('vdo-ht');
                                        $('.chat').removeClass('video-hide');
                                        $('.mainVideo').removeClass('video-hide');
                                        $('.j-logout').removeClass('video-hide');
                                    }
                                   

                                }
                            }
                        }); 
                       
                        /*QB.chat.dialog.create(params, function(err, createdDialog) {
                            if (err) {
                                console.log(err);
                            } else {
                                var user = createdDialog.occupants_ids;
                                for (var i = 0; i < user.length ; i++) {
                                    //console.log(app.users[i].full_name);
                                    if(user[i] == app.users[i].id){
                                        var name = app.users[i].full_name;
                                        var i = document.createElement("i");
                                        i.className = "fas fa-user-circle";
                                        var span = document.createElement("span");
                                        var t = document.createTextNode(name);
                                        span.appendChild(t);
                                        var div = document.createElement("div");
                                        var li = document.createElement("li");
                                        li.className = "chat-users-item";
                                        li.setAttribute('data-id', user[i]);
                                        div.append(i);
                                        div.append(span);
                                        li.append(div);
                                        $('#chat-users').append(li);
                                    }
                                    
                                }

                                
                                     
                                
                            }
                        });*/
                        
                    }
                });
            }).catch(function(error) {
                console.error(error);
            });

            return false;
        });
        
        //LOAD message
        function load_history_Message(){
            var dialogId = app.chatid;
            var params = {chat_dialog_id: dialogId, sort_desc: 'date_sent', limit: 50, skip: 0};
            QB.chat.message.list(params, function(err, messages) {
                  if (messages) {
                    var rnode = document.getElementById('chat-message');
                    while(rnode.firstChild){
                        rnode.removeChild(rnode.firstChild);
                    }
                    ui.insertOccupants().then(function(users){
                        for(var i=messages.items.length - 1 ;i>=0;i--){
                            var div = document.createElement("div");
                            var p_name = document.createElement("p");
                            var p_msg = document.createElement("p");
                            var userId = messages.items[i].sender_id;
                            var message = messages.items[i].message;
                            app.users = users;
                            var initiator = _.findWhere(app.users, {id: userId});
                            if(userId == app.caller.id){
                                div.className = "chat-message-user-me";
                                p_name.appendChild(document.createTextNode(initiator.full_name));
                            }else {
                                div.className = "chat-message-user-your";
                                p_name.appendChild(document.createTextNode(initiator.full_name));
                            }

                            p_msg.appendChild(document.createTextNode(message));
                            div.appendChild(p_name);
                            div.appendChild(p_msg);
                            rnode.appendChild(div);
                            $("#chat-message").each( function() 
                            {
                               var scrollHeight = Math.max(this.scrollHeight, this.clientHeight);
                               this.scrollTop = scrollHeight - this.clientHeight;
                           });

                        }
                    }); 


                }else{
                    console.log(err);
                }
            });
        }

        function show_Message(userId, message){
            ui.insertOccupants().then(function(users){
                var div = document.createElement("div");
                var p_name = document.createElement("p");
                var p_msg = document.createElement("p");
                app.users = users;
                var initiator = _.findWhere(app.users, {id: userId});
                if(userId == app.caller.id){
                    div.className = "chat-message-user-me";
                    p_name.appendChild(document.createTextNode(initiator.full_name));
                }else {
                    div.className = "chat-message-user-your";
                    p_name.appendChild(document.createTextNode(initiator.full_name));
                }

                p_msg.appendChild(document.createTextNode(message.body));
                div.appendChild(p_name);
                div.appendChild(p_msg);
                var rnode = document.getElementById('chat-message');
                rnode.appendChild(div);
                $("#chat-message").each( function() 
                {
                   var scrollHeight = Math.max(this.scrollHeight, this.clientHeight);
                   this.scrollTop = scrollHeight - this.clientHeight;
                });

            }); 
        }
        //CALL
        
        $(document).on('click', '.j-actions',function(){
            var $btn = $(this);
            var bandwidth = {"bandwidth": 0};
            var mediaParams = {
              audio: true,
              video: true,
              options: {
                muted: true,
                mirror: false
              },
              elemId: 'main_video'
            };

            isAudio = $btn.data('call') === 'audio';
            
            app.currentSession = QB.webrtc.createNewSession(
                Object.keys(app.callees), 
                isAudio ? QB.webrtc.CallType.AUDIO : QB.webrtc.CallType.VIDEO, 
                app.caller.id, 
                bandwidth
            );

            if(isAudio)
            {
                mediaParams = {
                    audio: true,
                    video: false
                };
            }
            
            app.currentSession.getUserMedia(mediaParams, function(err, stream) {
                if (err){
                    console.log(err);
                }else{
                    app.currentSession.call({}, function(error) { 
                        if(!window.navigator.onLine)
                        {
                            app.currentSession.stop({});
                            console.log(error);
                        }
                        else
                        {   
                            $('.jj-actions').addClass('video-hide');
                            $('.jj-end').removeClass('video-hide');
                            $('.vdo-local').empty();
                            Object.keys(app.callees).forEach(function(id , i , arr) {   
                                var userInfo = _.findWhere(app.users, {'id': +id});
                                
                                $('.vdo-local').append("<div class='col-xs-4 col-sm-4 col-md-4'>"+"<video id='localVideo_"+id+"' data-userID='"+id+"' class='localVideo'></video>"+"<span id='name_localVideo_"+id+"' class='local-name'>"+userInfo.full_name+"</span>"+"</div>");
                                
                            });

                            document.getElementById(sounds.call).play();
                        }
                    }); 
                }
            });


        });


        //END CALL
        $(document).on('click', '.j-end', function() {
            if(!_.isEmpty(app.currentSession)) {
                app.currentSession.stop({});
                app.currentSession = {};
                $('.jj-actions').removeClass('video-hide');
                $('.jj-end').addClass('video-hide');
                return false;
            }
        });

        //Accept
        $(document).on('click', '.j-accept', function() {
            isAudio = app.currentSession.callType === QB.webrtc.CallType.AUDIO;
            var mediaParams;
            

            if(isAudio)
            {
                mediaParams = {
                  audio: true,
                  video: false
                };
            }
            else
            {
                mediaParams = {
                  audio: true,
                  video: true,
                  options: {
                    muted: true,
                    mirror: false
                  },
                  elemId: 'main_video'
                };
            }
            document.getElementById(sounds.rington).pause();
            app.currentSession.getUserMedia(mediaParams, function(err, stream) {
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    var opponents = [app.currentSession.initiatorID];

                    app.currentSession.opponentsIDs.forEach(function(userID, i, arr) {
                        if(userID != app.currentSession.currentUserID){
                            opponents.push(userID);
                        }
                    });
                    $('.vdo-local').empty();
                    opponents.forEach(function(userID, i, arr) {
                        ui.insertOccupants().then(function(users){
                            app.users = users;
                        });
                        var initiator = _.findWhere(app.users, {id: userID});
                        
                        $('.vdo-local').append("<div class='col-xs-4 col-sm-4 col-md-4'>"+"<video id='localVideo_"+userID+"' data-userID='"+userID+"' class='localVideo'></video>"+"<span id='name_localVideo_"+userID+"' class='local-name'>"+initiator.full_name+"</span>"+"</div>");
                        
                    });
                    $('#call-user-div').addClass('video-hide');
                    $('.jj-actions').addClass('video-hide');
                    $('.jj-end').removeClass('video-hide');
                    app.currentSession.accept({});
                    
                }
            });
        });

        //Decline
        $(document).on('click', '.j-decline', function() {
            if (!_.isEmpty(app.currentSession)) {
                app.currentSession.reject({});
                $('#call-user-div').addClass('video-hide');
                $('.vdo-local').empty();
                $('.jj-actions').removeClass('video-hide');
                $('.jj-end').addClass('video-hide');
                document.getElementById(sounds.rington).pause();
            }
        });


        //Refresh

        $(document).on('click', '.j-refresh',function(){
            app.callees = {};
            ui.insertOccupants().then(function(users){
                app.users = users;
                if(app.users.length < 2)
                    {
                        $('.list-users').empty();
                        $('.list-users').append('<p>No Users</p>');
                    }
                   
            }, function(err) {
                console.warn(err);
            });
        });




        //MARK USER
        $(document).on('click', '.j-user', function() {
            var $user = $(this),
                user = {
                    id : +$.trim($user.data('id')),
                    name : $.trim($user.data('name'))
                };
            if($user.hasClass('video-j-user-active'))
            {   
                delete app.callees[user.id];
                $user.removeClass('video-j-user-active');
            }
            else
            {
                app.callees[user.id] = user.name;
                $user.addClass('video-j-user-active');
            }
        });



        //LOGOUT

        $(document).on('click', '.j-logout', function() {
            $('.mainVideo').addClass('video-hide');
            $('.chat').addClass('video-hide');
            $('.load-time').removeClass('video-hide');
            QB.users.update(app.caller.id, {
                'full_name': app.caller.full_name,
                'tag_list': ''
            }, function(updateError, updateUser) {
                if(updateError) {
                    console.log('APP [update user] Error:', updateError);
                } 
                else
                {
                    $('.load-time').addClass('video-hide');
                    $('.vdo-container').addClass('vdo-ht');
                    $('.j-logout').addClass('video-hide');
                    $('.j-login').removeClass('video-hide');

                }
            });
            if (!_.isEmpty(app.currentSession)) {
                app.currentSession.reject({});
            }
            app.caller = {};
            app.users = [];
            app.usersid = [];
            QB.chat.disconnect();
        });

        //SEND
        $(document).on('click', '#chat-btn-send', function() {
            var mess = document.getElementById('chat-msg-send').value;
            document.getElementById('chat-msg-send').value = "";
            var msg = {
                  type: 'groupchat',
                  body: mess,
                  extension: {
                    save_to_history: 1,
                }
            };

            /*var div = document.createElement("div");
            div.className = "chat-message-user-me";
            var p_name = document.createElement("p");
            var p_msg = document.createElement("p");
            p_name.appendChild(document.createTextNode(app.caller.id));
            p_msg.appendChild(document.createTextNode(mess));
            div.appendChild(p_name);
            div.appendChild(p_msg);
            document.getElementById('chat-message').appendChild(div);*/

            var dialogId = app.chatid;
            var dialogJid = QB.chat.helpers.getRoomJidFromDialogId(dialogId);
            QB.chat.send(dialogJid, msg);

        });
        QB.chat.onMessageListener = function onMessage(userId, message) {
          // This is a notification about dialog creation
          //
            
            if(message.markable){
                var params = {
                    messageId: message.id,
                    userId: userId,
                    dialogId: message.dialogId
                  };
                  QB.chat.sendReadStatus(params);
            }
            
            show_Message(userId, message)
           
        };

        
        
        QB.chat.onJoinOccupant = function onJoinOccupant(dialogId, userId){
            console.log(userId);
        };
        QB.chat.onDisconnectedListener = function() {
            console.log('onDisconnectedListener.');
        };

        QB.webrtc.onCallListener = function onCallListener(session, extension) {
            console.group('onCallListener.');
                console.log('Session: ', session);
                console.log('Extension: ', extension);
            console.groupEnd();

            app.currentSession = session;
            $('#call-user-div').removeClass('video-hide');
            ui.insertOccupants().then(function(users){
                app.users = users;
                var initiator = _.findWhere(app.users, {id: session.initiatorID});
                app.callees = {};
                $('#call-user').text(initiator.full_name);
                if (app.currentSession.state !== QB.webrtc.SessionConnectionState.CLOSED){
                    document.getElementById(sounds.rington).play();
                }
            });  
        };

        QB.webrtc.onRejectCallListener = function onRejectCallListener(session, userId, extension) {
            console.group('onRejectCallListener.');
                console.log('UserId: ' + userId);
                console.log('Session: ' + session);
                //console.log('Extension: ' + JSON.stringify(extension));
            console.groupEnd();

            var userInfo = _.findWhere(app.users, {'id': +userId});
            app.calleesRejected.push(userInfo);
        };

        QB.webrtc.onSessionCloseListener = function onSessionCloseListener(session){
            console.log('onSessionCloseListener: ', session);
            //app.currentSession.detachMediaStream('main_video');
            //app.currentSession.detachMediaStream('localVideo');
            $('.jj-actions').removeClass('video-hide');
            $('.jj-end').addClass('video-hide');
            document.getElementById(sounds.call).pause();
            document.getElementById(sounds.end).play();

        };
        QB.webrtc.onAcceptCallListener = function onAcceptCallListener(session, userId, extension) {
            console.group('onAcceptCallListener.');
                console.log('UserId: ', userId);
                console.log('Session: ', session);
                console.log('Extension: ', extension);
            console.groupEnd();
            
            var userInfo = _.findWhere(app.users, {'id': +userId});
            app.calleesAnwered.push(userInfo);

            document.getElementById(sounds.call).pause();

        };

        QB.webrtc.onUpdateCallListener = function onUpdateCallListener(session, userId, extension) {
            console.group('onUpdateCallListener.');
                console.log('UserId: ' + userId);
                console.log('Session: ' + session);
                console.log('Extension: ' + JSON.stringify(extension));
            console.groupEnd();
        };

        QB.webrtc.onUserNotAnswerListener = function onUserNotAnswerListener(session, userId) {
            console.group('onUserNotAnswerListener.');
                console.log('UserId: ', userId);
                console.log('Session: ', session);
            console.groupEnd();

            
        };

        QB.webrtc.onRemoteStreamListener = function onRemoteStreamListener(session, userId, stream) {
            console.group('onRemoteStreamListener.');
                console.log('userId: ', userId);
                console.log('Session: ', session);
                console.log('Stream: ', stream);
            console.groupEnd();

            app.currentSession.attachMediaStream('localVideo_'+userId, stream);
            
        };

        QB.webrtc.onStopCallListener = function onStopCallListener(session, userId, extension) {
            console.group('onStopCallListener.');
                console.log('UserId: ', userId);
                console.log('Session: ', session);
                console.log('Extension: ', extension);
            console.groupEnd();

        };

        QB.webrtc.onCallStatsReport = function onCallStatsReport(session, userId, stats, error) {
            console.group('onCallStatsReport');
                console.log('userId: ', userId);
                console.log('session: ', session);
                console.log('stats: ', stats);
            console.groupEnd();
        };

        QB.webrtc.onSessionConnectionStateChangedListener = function onSessionConnectionStateChangedListener(session, userId, connectionState) {
            console.group('onSessionConnectionStateChangedListener.');
                console.log('UserID:', userId);
                console.log('Session:', session);
                console.log('Сonnection state:', connectionState, statesPeerConn[connectionState]);
            console.groupEnd();

            var connectionStateName = _.invert(QB.webrtc.SessionConnectionState)[connectionState],
                isCallEnded = false;

            isCallEnded = _.every(app.currentSession.peerConnections, function(i) {
                    return i.iceConnectionState === 'closed';
                });

            if( isCallEnded ) 
            {    
                $('.vdo-local').empty();
                app.calleesAnwered = [];
                app.calleesRejected = [];
            }
        };
    });
}(window, window.QB, window.app, window.CONFIG,  jQuery, Backbone));
