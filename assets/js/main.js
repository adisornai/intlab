"use strict";

jQuery(document).ready(function ($) {

	$(window).load(function () {
		$(".loaded").fadeOut();
		$(".preloader").delay(1000).fadeOut("slow");
	});
    /*---------------------------------------------*
     * Mobile menu
     ---------------------------------------------*/
    $('#navbar-collapse').find('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 40)
                }, 1000);
                if ($('.navbar-toggle').css('display') != 'none') {
                    $(this).parents('.container').find(".navbar-toggle").trigger("click");
                }
                return false;
            }
        }
    });
	
	
	/*---------------------------------------------*
     * For Price Table
     ---------------------------------------------*/
	 
	checkScrolling($('.cd-pricing-body'));
	$(window).on('resize', function(){
		window.requestAnimationFrame(function(){checkScrolling($('.cd-pricing-body'))});
	});
	$('.cd-pricing-body').on('scroll', function(){ 
		var selected = $(this);
		window.requestAnimationFrame(function(){checkScrolling(selected)});
	});

	function checkScrolling(tables){
		tables.each(function(){
			var table= $(this),
				totalTableWidth = parseInt(table.children('.cd-pricing-features').width()),
		 		tableViewport = parseInt(table.width());
			if( table.scrollLeft() >= totalTableWidth - tableViewport -1 ) {
				table.parent('li').addClass('is-ended');
			} else {
				table.parent('li').removeClass('is-ended');
			}
		});
	}

	//switch from monthly to annual pricing tables
	bouncy_filter($('.cd-pricing-container'));

	function bouncy_filter(container) {
		container.each(function(){
			var pricing_table = $(this);
			var filter_list_container = pricing_table.children('.cd-pricing-switcher'),
				filter_radios = filter_list_container.find('input[type="radio"]'),
				pricing_table_wrapper = pricing_table.find('.cd-pricing-wrapper');

			//store pricing table items
			var table_elements = {};
			filter_radios.each(function(){
				var filter_type = $(this).val();
				table_elements[filter_type] = pricing_table_wrapper.find('li[data-type="'+filter_type+'"]');
			});

			//detect input change event
			filter_radios.on('change', function(event){
				event.preventDefault();
				//detect which radio input item was checked
				var selected_filter = $(event.target).val();

				//give higher z-index to the pricing table items selected by the radio input
				show_selected_items(table_elements[selected_filter]);

				//rotate each cd-pricing-wrapper 
				//at the end of the animation hide the not-selected pricing tables and rotate back the .cd-pricing-wrapper
				
				if( !Modernizr.cssanimations ) {
					hide_not_selected_items(table_elements, selected_filter);
					pricing_table_wrapper.removeClass('is-switched');
				} else {
					pricing_table_wrapper.addClass('is-switched').eq(0).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {		
						hide_not_selected_items(table_elements, selected_filter);
						pricing_table_wrapper.removeClass('is-switched');
						//change rotation direction if .cd-pricing-list has the .cd-bounce-invert class
						if(pricing_table.find('.cd-pricing-list').hasClass('cd-bounce-invert')) pricing_table_wrapper.toggleClass('reverse-animation');
					});
				}
			});
		});
	}
	function show_selected_items(selected_elements) {
		selected_elements.addClass('is-selected');
	}

	function hide_not_selected_items(table_containers, filter) {
		$.each(table_containers, function(key, value){
	  		if ( key != filter ) {	
				$(this).removeClass('is-visible is-selected').addClass('is-hidden');

			} else {
				$(this).addClass('is-visible').removeClass('is-hidden is-selected');
			}
		});
	}



    /*---------------------------------------------*
     * STICKY scroll
     ---------------------------------------------*/

    $.localScroll();



    /*---------------------------------------------*
     * Counter 
     ---------------------------------------------*/

//    $('.statistic-counter').counterUp({
//        delay: 10,
//        time: 2000
//    });




    /*---------------------------------------------*
     * WOW
     ---------------------------------------------*/

//        var wow = new WOW({
//            mobile: false // trigger animations on mobile devices (default is true)
//        });
//        wow.init();


    /* ---------------------------------------------------------------------
     Carousel
     ---------------------------------------------------------------------= */

//    $('.testimonials').owlCarousel({
//        responsiveClass: true,
//        autoplay: false,
//        items: 1,
//        loop: true,
//        dots: true,
//        autoplayHoverPause: true
//
//    });



// scroll Up
    $(window).scroll(function(){
        if ($(this).scrollTop() > 600) {
            $('.scrollup').fadeIn('slow');
        } else {
            $('.scrollup').fadeOut('slow');
        }
    });

    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;
    });	
    
    $('.bLecturer').click(function(){
    	var path = window.location.pathname;
    	var pathname = "/IntLab/";
    	if(path == pathname){
    		window.location = "../IntLab/#Lecturer";
    	}
    	else{
    		window.location = "../../IntLab/#Lecturer";
    	}
    	
    	
    });	

    $('.bStudents').click(function(){
    	var path = window.location.pathname;
    	var pathname = "/IntLab/";
    	if(path == pathname){
    		window.location = "../IntLab/#Students";
    	}
    	else{
    		window.location = "../../IntLab/#Students";
    	}
    	
    });	

    $('#next').click(function(){
    	$('.back').removeClass('hide');
    	$('.div-input-profile-d2').removeClass('hide');
    	$('.div-input-profile-d1').addClass('hide');
    	$(this).addClass('hide');
    });	

    $('#back').click(function(){
    	$('#next').removeClass('hide');
    	$('.div-input-profile-d1').removeClass('hide');
    	$('.div-input-profile-d2').addClass('hide');
    	$('.back').addClass('hide');

    });	
   

    // 
    //profile
    //
    
    $('#province').change(function() {
        alert('--'+$('#province option:selected').text()+'--');
    });

    $('.publish-show-add').click(function() {
        $('.publish-container').show('slow', function() {
            
        });
    });

    $(window).click(function(event) {
        if(event.target == $('.publish-container')[0]){
            $('.publish-container').hide('fast', function() {

            });
        }
    });
   
   $('.publish-close').click(function(event) {
       $('.publish-container').hide('fast', function() {

        });
   });


   $('.research-show-st').click(function(event) {
       $('.research-container-st').show('fast', function() {
           
       });
   });
   $('.research-close-st').click(function(event) {
       $('.research-container-st').hide('fast', function() {
           
       });
   });
   $(window).click(function(event) {
       if(event.target == $('.research-container-st')[0])
       {
            $('.research-container-st').hide('fast', function() {
           
            });
       }
   });

   //research

   $('.re-edit').click(function(event) {
        var reid = $(this).attr('id');
        var path = location.protocol + "//" + location.host + "/IntLab/research_select";
        $.ajax({
            url: path,
            type: 'POST',
            dataType: 'json',
            data: {'reid': reid},
            success: function(data){
                $('#r_id').val(data['r_id']);
                $('#r_nameTH').val(data['r_nameTH']);
                $('#r_nameEN').val(data['r_nameEN']);
                $('#r_head').val(data['r_head']);
                $('#r_contributor1').val(data['r_contributor1']);
                $('#r_contributor2').val(data['r_contributor2']);
                $('#r_contributor3').val(data['r_contributor3']);
                $('#r_budget').val(data['r_budget']);
                $('#r_begin').val(data['r_begin']);
                $('#r_end').val(data['r_end']);
                $('#r_status').val(data['r_status']);
                $('#r_file').val(data['r_file']);
            }
        });

       $('.re-show-edit-container').show('fast', function() {
       });

   });

   $('.re-close-edit').click(function(event) {
       $('.re-show-edit-container').hide('fast', function() {
           
       });
   });

   $(window).click(function(event) {
    if(event.target == $('.re-show-edit-container')[0])
       {
            $('.re-show-edit-container').hide('fast', function() {
           
            });
       }
   });

});
